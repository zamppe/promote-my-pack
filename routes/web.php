<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Getting under /manage requires the user to have default role
Route::group([
    'prefix' => 'manage',
    'as' => 'manage.',
    'namespace' => 'Manage',
    'middleware' => [
        'role:'.config('roles.roles.default')
    ]
],
function () {
    Route::get('/', 'DashboardController@Dashboard')->name('dashboard');
    Route::put('levels/{level}/update-times', 'LevelController@updateTimes')->name('levels.update-times');
    Route::put('levels/{level}/tags/{tag}', 'LevelController@toggleTag')->name('levels.tags.toggle-tag');
    Route::resource('levels', 'LevelController', ['only' => ['show']]);

    // Pack managing is allowed only if the user is a pack admin
    Route::group(['middleware' => 'isPackAdmin'], function()
    {
        Route::resource('packs', 'PackController', ['only' => ['store', 'edit', 'update', 'destroy']]);

        Route::post('packs/{pack}/update-times', 'PackController@updateTimes')->name('packs.update-times');
        Route::post('packs/{pack}/rebuild-history', 'PackController@rebuildHistory')->name('packs.rebuild-history');
        Route::delete('packs/{pack}/delete-times', 'PackController@deleteTimes')->name('packs.delete-times');
        Route::post('packs/{pack}/process-achievements', 'PackController@processAchievements')->name('packs.process-achievements');

        Route::get('packs/{pack}/levels/{level}', 'PackLevelsController@show')->name('packs.levels.show');
        Route::put('packs/{pack}/levels/{level}/detach', 'PackLevelsController@detach')->name('packs.levels.detach');
        Route::post('packs/{pack}/levels/add', 'PackLevelsController@add')->name('packs.levels.add');
        Route::post('packs/{pack}/levels/import', 'PackLevelsController@import')->name('packs.levels.import');
        Route::put('packs/{pack}/levels/sort', 'PackLevelsController@sort')->name('packs.levels.sort');
        Route::put('packs/{pack}/levels/sort-drag', 'PackLevelsController@sortDrag')->name('packs.levels.sort-drag');
        Route::get('packs/{pack}/level-search', 'PackLevelsController@search')->name('packs.levels.search');

        //@todo maby move to packlevelstimescont
        Route::post('packs/{pack}/levels/{level}/times/{time}/ban', 'PackLevelsController@banTime')->name('packs.levels.times.ban');
        Route::delete('packs/{pack}/levels/{level}/times/{time}/unban', 'PackLevelsController@unbanTime')->name('packs.levels.times.unban');

        Route::post('packs/{pack}/kuskis/{kuski}/ban', 'PackKuskisController@banKuski')->name('packs.kuskis.ban');
        Route::delete('packs/{pack}/kuskis/{kuski}/unban', 'PackKuskisController@unbanKuski')->name('packs.kuskis.unban');
        Route::resource('packs.news', 'PacknewsController', ['except' => ['index', 'show', 'create']]);
        Route::resource('packs.targetAchievements', 'TargetAchievementsController', ['except' => ['index', 'show']]);

        Route::resource('packs.customAchievements', 'CustomAchievementsController', ['except' => ['index', 'show']]);
        Route::post('packs/{pack}/customAchievements/{customAchievement}/grant-to-kuski', 'CustomAchievementsController@grantToKuski')->name('packs.customAchievements.grant-to-kuski');
        Route::delete('packs/{pack}/customAchievements/{customAchievement}/remove-from-kuski', 'CustomAchievementsController@removeFromKuski')->name('packs.customAchievements.remove-from-kuski');

        Route::get('profile/edit', 'UserController@edit')->name('users.edit');
        Route::put('profile/update', 'UserController@update')->name('users.update');

    });
});

// Getting under /moderator requires the user to have moderator role
Route::group([
    'prefix' => 'moderator',
    'as' => 'moderator.',
    'namespace' => 'Moderator',
    'middleware' => [
        'role:'.config('roles.roles.moderator')
    ]
], function () {
    Route::get('/', 'DashboardController@Dashboard')->name('dashboard');
    Route::resource('tags', 'TagsController', ['only' => ['store', 'update', 'destroy']]);
});

// Getting under /admin requires the user to have admin role
Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin',
    'middleware' => [
        'role:'.config('roles.roles.admin')
    ]
], function () {
    Route::get('/', 'DashboardController@Dashboard')->name('dashboard');
    Route::post('get-all-levels', 'DashboardController@getAllLevels')->name('get-all-levels');

    Route::resource('users', 'UsersController', ['only' => ['index', 'edit', 'update', 'destroy']]);
    Route::post('users/{user}/grant-role', 'UsersController@grantRole')->name('users.grant-role');
    Route::delete('users/{user}/remove-role', 'UsersController@removeRole')->name('users.remove-role');

    Route::resource('packs', 'PackController', ['only' => ['destroy']]);
});

Route::get('/', 'FrontPageController@frontPage')->name('frontpage');

Route::resource('news', 'NewsController', ['except' => ['index', 'show', 'create']]);

Route::resource('packs', 'PackController', ['only' => ['index']]);
Route::get('packs/{pack}/records', 'PackController@records')->name('packs.records');
Route::get('packs/{pack}/achievements', 'PackController@achievements')->name('packs.achievements');
Route::get('packs/{pack}/landing', 'PackController@landing')->name('packs.landing');

Route::get('packs/{pack}/statistics/show', 'PackStatisticsController@show')->name('packs.statistics.show');
Route::get('packs/{pack}/statistics/generate', 'PackStatisticsController@generate')->name('packs.statistics.generate');

Route::get('packs/{pack}/levels/{level}', 'PackLevelsController@show')->name('packs.levels.show');
Route::get('packs/{pack}/levels/{level}/extra', 'PackLevelsController@getExtraInfo')->name('packs.levels.get-extra-info');
Route::get('packs/{pack}/download-levels', 'PackController@download')->name('packs.download-levels');

Route::resource('levels', 'LevelController', ['only' => ['show']]);
Route::post('levels/{level}/times/{time}/ban', 'LevelController@banTime')->name('levels.times.ban');
Route::delete('levels/{level}/times/{time}/unban', 'LevelController@unbanTime')->name('levels.times.unban');

Route::resource('kuskis', 'KuskiController', ['only' => ['show']]);

Route::get('statistics/show', 'StatisticsController@show')->name('statistics.show');
Route::get('statistics/generate', 'StatisticsController@generate')->name('statistics.generate');
