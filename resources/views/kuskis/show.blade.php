@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge">
        <h2>{{ $kuski->name }}{{ $kuski->teamFormatted }}</h2>
        <span class="flag-icon flag-icon-{{ isset($kuski) && $kuski->nationality ? $kuski->nationality : '?'}} flag-icon--large"></span>

        @if ($recentActivity->count())
            <h3>Recent activity</h3>
            <ul class="list-reset m-0 p-0">
                @foreach($recentActivity as $activity)
                    <li>
                        {{ $activity->driven_at }}
                        @include('packs.partials.recentActivityTag', ['time' => $activity])
                        {{ $activity->time }}
                        {{ $activity->level->fullName }}
                    </li>
                @endforeach
            </ul>
        @endif

        <h3>Achievements</h3>
        @if($kuski->achieves->count())
            <ul>
                @foreach($kuski->achieves as $achieve)
                    @php
                        $achievement = $achieve->achievable;
                    @endphp

                    @if ($achievement !== null)
                        @php
                            $pack = $achievement->pack;
                        @endphp

                        <li>
                            {{ $achievement->name }}
                            @if($achieve->achievable_type == \App\Models\TargetAchievement::class)
                                <h4>Computed description</h4>
                                <p>{{ $achievement->computedDescription }}</p>
                            @endif
                        </li>
                    @endif
                @endforeach
            </ul>
        @else
            <p class="c-text--quiet">This kuski does not have any achievements yet.</p>
        @endif
    </div>
@endsection
