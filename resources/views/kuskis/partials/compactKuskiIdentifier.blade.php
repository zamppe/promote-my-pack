@php
    if (isset($kuski) && $kuski instanceof \App\Models\Kuski) {
        $nationality = $kuski->nationality;
        $kuskiName = $kuski->name;
        $team = $kuski->teamFormatted;
        $url = route('kuskis.show', [$kuski]);
    } else {
        if (! isset($nationality)) {
            $nationality = '';
        }

        if (! isset($kuskiName)) {
            $kuskiName = '';
        }

        if (! isset($team)) {
            $team = '';
        }
    }
@endphp

@include('components.flagIcon', ['nationality' => $nationality])&nbsp;<a class="c-link" href="{{ isset($url) ? $url : '#' }}">{{ $kuskiName }}{{ $team }}</a>
