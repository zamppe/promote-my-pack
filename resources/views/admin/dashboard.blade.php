@extends('layouts.app')

@section('content')

    <div class="o-container o-container--xlarge">
        <h1 class="u-centered c-heading">Admin panel</h1>

        <a href="{{route('admin.users.index')}}" class="c-link">Manage users</a>
    </div>
@endsection
