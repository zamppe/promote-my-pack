@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge">
        <h1 class="c-heading">Edit profile of {{ $user->nickname }}</h1>

        <div class="o-grid o-grid--no-gutter o-grid--wrap">
            <div class="o-grid__cell o-grid__cell--width-30@medium o-grid__cell--width-100">
                {!! Form::model($user, [
                    'route' => ['admin.users.update', $user],
                    'method' => 'PUT',
                ]) !!}
                    @include('blaze.forms.email', ['input' => 'email', 'label' => 'Email address'])

                    @include('blaze.forms.text', ['input' => 'nickname', 'label' => 'Nick'])

                    @include('blaze.forms.submit', [
                        'text' => 'Update'
                    ])
                {!! Form::close() !!}
            </div>

            <div class="o-grid__cell o-grid__cell--offset-10@medium o-grid__cell--width-30@medium o-grid__cell--width-100">
                {!! Form::model($user, [
                    'route' => ['admin.users.update', $user],
                    'method' => 'PUT'
                ]) !!}
                    @include('blaze.forms.password', ['input' => 'password', 'label' => 'New password'])

                    @include('blaze.forms.password', ['input' => 'password_confirmation', 'label' => 'Password confirmation'])

                    @include('blaze.forms.submit', [
                        'text' => 'Update'
                    ])
                {!! Form::close() !!}
            </div>
        </div>

        <h3 class="c-heading">Roles</h3>

        @foreach($roles as $role)
            <div class="u-letter-box--xsmall">
                @if($user->hasRole($role))
                    {!! Form::open([
                        'route' => ['admin.users.remove-role', $user],
                        'method' => 'DELETE'
                    ]) !!}
                        {!! Form::hidden('role', $role, []) !!}
                        {!! Form::submit($role, ['class' => 'c-button c-button--error u-small c-text--loud']) !!}
                    {!! Form::close() !!}
                @else
                    {!! Form::open([
                        'route' => ['admin.users.grant-role', $user],
                        'method' => 'POST'
                    ]) !!}
                        {!! Form::hidden('role', $role, []) !!}
                            @include('blaze.forms.submit', [
                                'text' => $role
                            ])
                    {!! Form::close() !!}
                @endif
            </div>
        @endforeach

        <h3 class="c-heading">Delete user</h3>

        {!! Form::open([
            'route' => ['admin.users.destroy', $user],
            'method' => 'DELETE',
        ]) !!}
            {!! Form::submit('Delete', ['class' =>'c-button c-button--error u-small c-text--loud']) !!}
        {!! Form::close() !!}
    </div>
@endsection
