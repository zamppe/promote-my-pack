@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge">
        <h1 class="c-heading">Users</h1>
        @foreach($users as $user)
            <a href="{{ route('admin.users.edit', $user) }}" class="c-link">{{ $user->nickname }}</a> <br />
        @endforeach
    </div>
@endsection
