@extends('layouts.app')

@section('content')
    <div class="o-container o-container--super">
        @include('packs.partials.childNav')

        <div class="u-pillar-box--medium">
            <h2 class="c-heading">Statistics</h2>

            {!! Form::open([
                'route' => ['packs.statistics.generate', $pack],
                'method' => 'GET',
                'class' => 'js-statistics-form'
            ]) !!}
                <div class="o-grid o-grid--no-edge-gutter o-grid--wrap">
                    <div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--width-100">
                        @include('blaze.forms.select', [
                            'input' => 'type',
                            'optionsArray' => $statisticsTypes,
                            'label' => 'Type'
                        ])
                    </div>
                </div>

                <div class="o-grid o-grid--no-edge-gutter o-grid--wrap">
                    <div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--width-100">
                        @include('blaze.forms.select', [
                            'input' => 'level_id',
                            'optionsArray' => [0 => 'All'] + $levelsForSelect->toArray(),
                            'label' => 'Level'
                        ])
                    </div>

                    <div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--width-100">
                        @include('blaze.forms.text', [
                            'input' => 'kuski',
                            'label' => 'Kuski'
                        ])
                    </div>
                </div>

                <div class="o-grid o-grid--no-edge-gutter o-grid--wrap">
                    <div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--width-100">
                        @include('blaze.forms.text', [
                            'input' => 'after',
                            'label' => 'After',
                            'classes' => ['js-flatpickr']
                        ])
                    </div>

                    <div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--width-100">
                        @include('blaze.forms.text', [
                            'input' => 'before',
                            'label' => 'Before',
                            'classes' => ['js-flatpickr']
                        ])
                    </div>
                    <div class="o-grid__cell o-grid__cell--width-100@medium o-grid__cell--width-100">
                        <div class="u-letter-box--medium">
                            @include('blaze.forms.submit', [
                                'text' => 'Go'
                            ])
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}

            <table class="table" @if(! empty($statisticsJson)) data-json="{{ $statisticsJson }}" @endif></table>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script>
        $(function() {
            var $table = $('.table');
            if ($table.attr('data-json')) {
                var statisticsJson = JSON.parse($table.attr('data-json'));
                var ft = FooTable.init('.table', {
                    columns: JSON.parse(statisticsJson.columns),
                    rows: JSON.parse(statisticsJson.rows),
                    filtering: {
                        enabled: true,
                        delay: 200
                    },
                    paging: {
                        enabled: true,
                        size: 50
                    },
                    sorting: {
                        enabled: true
                    }
                });
            }
        });

        $(document).on('submit', '.js-statistics-form', function(e) {
            e.preventDefault();

            // Little workaround hacks
            $('tr.footable-empty').remove();
            $('div.footable-loader').remove();

            var $form = $(this);
            $form.find('input[type="submit"]').prop('disabled', true);

            $ajax = $.ajax({
                method: 'get',
                data: $form.serialize(),
                url: $form.attr('action'),
                dataType: 'json'
            });

            $ajax.then(function() {
                $('.js-error').each(function() {
                    $(this).html('');
                });
            });

            var ft = FooTable.init('.table', {
                columns: $ajax.then(function(response) {
                    console.log(JSON.parse(response['columns']));
                    return JSON.parse(response['columns']);
                }),
                rows: $ajax.then(function(response) {
                    $form.find('input[type="submit"]').prop('disabled', false);
                    console.log(JSON.parse(response['rows']));
                    return JSON.parse(response['rows']);
                }),
                filtering: {
                    enabled: true,
                    delay: 200
                },
                paging: {
                    enabled: true,
                    size: 50
                },
                sorting: {
                    enabled: true
                }
            });

            $ajax.fail(function(response) {
                var errors = response.responseJSON;
                Object.keys(errors).forEach(function (key) {
                    var error = errors[key];

                    $('.js-error').each(function() {
                        $el = $(this);
                        if ($el.attr('data-input') === key) {
                            $el.html(error);
                        }
                    });
                });
                $form.find('input[type="submit"]').prop('disabled', false);
            });

            var url = window.location.href.split('?')[0] + '?' + $form.serialize();
            window.history.pushState(null, '', url);
        });
    </script>
@endsection
