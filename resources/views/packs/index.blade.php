@extends('layouts.app')

@section('content')
    <div class="o-container o-container--super u-pillar-box--medium">
        <h3>Levelpacks</h3>

        <div class="c-table c-table--condensed">
            <div class="c-table__row c-table__row--heading">
                <div class="c-table__cell">
                    Pack
                </div>
                <div class="c-table__cell">
                    TT leader
                </div>
                <div class="c-table__cell">
                    WRs leader
                </div>
            </div>
            @foreach($packs as $pack)
                @php
                    $totalTimeLeader = $pack->totalTimesLeader();
                    $worldRecordsLeader = $pack->worldRecordsLeader();
                @endphp

                <div class="c-table__row c-table__row--body">
                    <div class="c-table__cell">
                        <a href="{{ route('packs.landing', $pack) }}" class="c-link c-text--loud">{{ $pack->name }}</a>
                    </div>
                    <div class="c-table__cell">
                        @if($totalTimeLeader)
                            @include('kuskis.partials.compactKuskiIdentifier', ['kuski' => $totalTimeLeader->kuski])&nbsp;{{ mstostrtime($totalTimeLeader->total_time) }}
                        @else
                            <span class="c-text--quiet">No results</span>
                        @endif
                    </div>
                    <div class="c-table__cell">
                        @if($worldRecordsLeader)
                            @include('kuskis.partials.compactKuskiIdentifier', ['kuski' => $worldRecordsLeader])&nbsp;{{ $worldRecordsLeader->wrCount }} {{ $worldRecordsLeader->wrCount > 1 ? 'WRs' : 'WR' }}
                        @else
                            <span class="c-text--quiet">No results</span>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
