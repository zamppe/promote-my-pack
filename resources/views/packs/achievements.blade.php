@extends('layouts.app')

@section('content')
    <div class="o-container o-container--super">
        @include('packs.partials.childNav')

        <div class="u-pillar-box--medium">
            <h2 class="c-heading">Achievements</h2>

            <p class="c-paragraph c-text--quiet">This pack contains {{ $targetAchievements->count() + $customAchievements->count()}} achievements</p>

            @foreach($targetAchievements as $achievement)
                @include('packs.partials.achievementItem')
            @endforeach

            @foreach($customAchievements as $achievement)
                @include('packs.partials.achievementItem')
            @endforeach
        </div>
    </div>
@endsection
