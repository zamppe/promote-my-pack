@php
    if(!isset($class)) {
        $class = '';
    }
@endphp

@if($times->count())
    <ol class="c-positions-list {{$class}}">
        @foreach($times as $time)
            <li>
                <span>
                     @include('kuskis.partials.compactKuskiIdentifier', ['kuski' => $time->kuski])
                </span>
                <span class="float-right c-tooltip c-tooltip--top" aria-label="{{ $time->driven_at }}">
                    {{ $time->time }}
                </span>
            </li>
        @endforeach
    </ol>
@else
    <span class="c-text--quiet">No finishes</span>
@endif
