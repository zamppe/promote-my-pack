<div class="u-pillar-box--medium">
    <h1 class="c-heading">{{ $pack->name }}</h1>

    <a href="{{ route('packs.download-levels', [$pack]) }}" class="c-link"><i class="fa fa-download">&nbsp;</i>Download levels</a>
</div>

<div class="u-pillar-box--medium">
    <div class="c-tabs c-tabs--info">
        <div class="c-tabs__headings">            
            <a class="c-tab-heading c-link c-link--unstyled {{ Route::is('packs.landing') ? 'c-tab-heading--active' : ''}}" href="{{ route('packs.landing', [$pack]) }}">Main</a>          
            <a class="c-tab-heading c-link c-link--unstyled {{ Route::is('packs.records') ? 'c-tab-heading--active' : ''}}" href="{{ route('packs.records', [$pack]) }}">Records</a>

            <a class="c-tab-heading c-link c-link--unstyled {{ Route::is('packs.statistics.show') ? 'c-tab-heading--active' : ''}}" href="{{ route('packs.statistics.show', [$pack]) }}">Statistics</a>

            @if ($pack->targetAchievements->merge($pack->customAchievements)->count())
                <a class="c-tab-heading c-link c-link--unstyled {{ Route::is('packs.achievements') ? 'c-tab-heading--active' : ''}}" href="{{ route('packs.achievements', [$pack]) }}">Achievements</a>
            @endif
        </div>
    </div>
</div>
