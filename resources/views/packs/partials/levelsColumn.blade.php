@foreach($levels as $level)
    <div class="o-grid o-grid--wrap u-letter-box--medium">
        <div class="o-grid__cell o-grid__cell--width-100">
            <h2 class="c-heading">
                <a href="{{ route('packs.levels.show', [$pack, $level]) }}" class="c-link c-link--success">
                    {{ $level->filename }},&nbsp;{{ $level->name }}
                </a>
            </h2>
        </div>
        <div class="o-grid__cell o-grid__cell--width-40@small o-grid__cell--width-100">
            @include('levels.partials.image')
        </div>

        <div class="o-grid__cell o-grid__cell--width-60@small o-grid__cell--width-100">
            @php
                $top3 = $level->prs()->with('kuski')->where('prs.pack_id', $pack->id)->limit(3)->get();
                // $top3 = $level->top(3);
            @endphp
            @if($top3->count())
                <ol class="positions-list">
                    @foreach($top3 as $time)
                        <li>
                            <span>
                                 @include('kuskis.partials.compactKuskiIdentifier', ['kuski' => $time->kuski])
                            </span>
                            <span class="float-right">
                                {{ $time->time }}
                            </span>
                        </li>
                    @endforeach
                </ol>
            @else
                <span class="c-text--quiet">No finishes</span>
            @endif
        </div>
    </div>
@endforeach
