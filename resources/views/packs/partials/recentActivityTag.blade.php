@if($time->activity_type === 'wr')
    <span class="c-improvement-tag u-color-green">wr</span>
@elseif(!$time->old_time_ms || (isset($time->activity_type) && $time->activity_type === 'ff'))
    <span class="c-improvement-tag u-color-orange">ff</span>
@elseif($time->activity_type === 'pr')
    <span class="c-improvement-tag u-color-grey-darker">pr</span>
@endif
