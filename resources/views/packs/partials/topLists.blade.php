@php
    if(!isset($n)) {
        $n = 3;
    }

    $remember = config('cache.remember');

    if (is_dev()) {
        $topList = $level->top($n, $with = ['kuski'], $pack);
        $finishCounts = $level->finishCounts($n, $with = ['kuski'], $pack);
        $recentActivity = $level->recentActivity($n, $with = ['kuski'], $pack);
    } else {    
        $topList = \Cache::tags($pack->cache_tag)->remember("best_times{$level->id}{$n}", $remember, function() use ($pack, $level, $n) {
            return $level->top($n, $with = ['kuski'], $pack);
        });

        $finishCounts = \Cache::tags($pack->cache_tag)->remember("finish_counts{$level->id}{$n}", $remember, function() use ($pack, $level, $n) {
            return $level->finishCounts($n, $with = ['kuski'], $pack);
        });

        $recentActivity = \Cache::tags($pack->cache_tag)->remember("recent_activity{$level->id}{$n}", $remember, function() use ($pack, $level, $n) {
            return $level->recentActivity($n, $with = ['kuski'], $pack);
        });
    }
@endphp

@include('packs.partials.bestTimesList', [
    'times' => $topList,
    'class' => 'c-best-times--flex',
])
@include('packs.partials.finishCountsList', [
    'finishes' => $finishCounts,
    'class' => 'c-finish-counts--flex',
])
@include('packs.partials.recentActivityList', [
    'recent' => $recentActivity,
    'class' => 'c-recent-activity--flex',
])
