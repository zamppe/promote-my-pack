<section class="u-letter-box--super">
    <!-- GRID -->
    <div class="o-grid o-grid--no-gutter o-grid--xsmall-full o-grid--small-full o-grid--medium-full">

        <!-- CELL -->
        @php
            $media = $achievement->getMedia('achievement')->first();
        @endphp

        <!-- CELL -->
        <div class="o-grid__cell o-grid__cell--width-50">
            <div class="o-grid o-grid--no-gutter o-grid--wrap">
                <div class="o-grid__cell o-grid__cell--width-40@small o-grid__cell--width-100">
                    <h3 class="c-heading c-heading--medium">
                        {{ $achievement->name }}
                        <span class="c-heading__sub">{{ $achievement->created_at }}</span>
                    </h3>

                    <p class="c-paragraph">
                        <b>Description:</b><br> {{ $achievement->description }}
                    </p>

                    @if ($achievement instanceof \App\Models\TargetAchievement)
                        <p class="c-paragraph">
                            <b>Computed description:</b><br> {{ $achievement->computedDescription }}
                        </p>
                    @endif
                </div>

                <div class="o-grid__cell o-grid__cell--width-40@small o-grid__cell--width-100">
                    @if($media)
                        <img class="o-image achievement-image"
                            src="{{ GlideImage::load($media->getDiskPath(), [
                                'q' => 60,
                                'w' => 300,
                                'h' => 250,
                            ]) }}"
                            alt="{{ $achievement->name }}" />
                    @endif
                </div>
            </div>
        </div>

        @php
            $achievers = $achievement->achieves()->with('kuski')->orderBy('achieved_at', 'asc')->get();
        @endphp

        <div class="o-grid__cell o-grid__cell--width-50">
            @if ($achievers->count())
                <h3 class="c-heading">Achievers</h3>
                <div class="c-table c-table--condensed">
                    <div class="c-table__row c-table__row--heading">
                        <span class="c-table__cell">Kuski</span>
                        <span class="c-table__cell">Achieved at</span>
                    </div>

                    @foreach($achievers as $achieve)
                        <div class="c-table__row">
                            <span class="c-table__cell">@include('kuskis.partials.compactKuskiIdentifier', ['kuski' => $achieve->kuski])</span>
                            <span class="c-table__cell">{{ $achieve->achieved_at }}</span>
                        </div>
                    @endforeach
                </div>
            @else
                <p class="c-text--quiet">No achievers. Be the first.</p>
            @endif
        </div>
    </div>
</section>
