@php
    if(!isset($class)) {
        $class = '';
    }
@endphp

@if($recent->count())
    <ul class="c-list c-list--unstyled c-recent-activity {{$class}}">
        @foreach($recent as $time)
            <li style="display: flex;">
                @if($time->relationLoaded('level'))
                    <span>
                         {{ $time->level->filename }}
                    </span>
                @endif

                @include('packs.partials.recentActivityTag')
                <span>
                     @include('kuskis.partials.compactKuskiIdentifier', ['kuski' => $time->kuski])
                </span>

                <span class="float-right c-tooltip c-tooltip--top" aria-label="{{ $time->driven_at }}" style="margin-left: auto">
                    <span class="c-recent-activity__old-time">{{$time->old_time}}</span> <i class="fa fa-arrow-right u-color-green c-recent-activity__arrow"></i> <span class="c-recent-activity__new-time">{{$time->time}}</span><span class="c-recent-activity__driven-at">, {{$time->driven_at->diffForHumans(null, false, true)}}</span>
                </span>
            </li>
        @endforeach
    </ul>
@endif
