@php
    if(!isset($class)) {
        $class = '';
    }
@endphp

@if($finishes->count())
    <ol class="c-positions-list {{$class}}">
        @foreach($finishes as $time)
            <li>
                <span>
                     @include('kuskis.partials.compactKuskiIdentifier', ['kuski' => $time->kuski])
                </span>
                <span class="float-right">
                    <b>{{ $time->finish_count }}</b> finishes
                </span>
            </li>
        @endforeach
    </ol>
@endif
