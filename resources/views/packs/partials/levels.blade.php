@if($levelsLeftColumn->count())
    <div class="o-grid o-grid--no-gutter o-grid--wrap">
        <div class="o-grid__cell o-grid__cell--width-50@xlarge o-grid__cell--width-100">
            @include('packs.partials.levelsColumn', ['levels' => $levelsLeftColumn])
        </div>
        <div class="o-grid__cell o-grid__cell--width-50@xlarge o-grid__cell--width-100">
            @include('packs.partials.levelsColumn', ['levels' => $levelsRightColumn])
        </div>
    </div>
@else
    <p class="c-text--quiet">No levels</p>
@endif
