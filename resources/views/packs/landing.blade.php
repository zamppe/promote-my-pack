@extends('layouts.app')

@section('content')
    <div class="o-container o-container--super">
        @include('packs.partials.childNav')
        <div class="o-grid">   
            <div class="o-grid__cell o-grid__cell--width-66">         
                <h2 class="c-heading">News</h2>

                @unless($pack->news->count()) <p class="c-text--quiet">No news is good news oke</p> @endunless
                @foreach($pack->news as $newsItem)
                    <div class="u-letter-box--small">
                        <div class="c-card c-card--higher u-bg-beige">
                            <div class="c-card__item c-card__item--divider u-bg-green-light">{{ $newsItem->title }}</div>
                            <div class="c-card__body">
                                <p class="c-paragraph">{!! $newsItem->content !!}</p>
                            </div>
                            <footer class="c-card__footer">
                                 <h4 class="c-heading">
                                    <div class="c-heading__sub">{{ $newsItem->user->nickname }}, {{ $newsItem->createdAt->diffForHumans() }}</div>
                                </h4>
                            </footer>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="o-grid__cell o-grid__cell--width-33">
                <h2 class="c-heading">Recent activity</h2>
                @include('packs.partials.recentActivityList', [
                    'recent' => $recentActivity,
                    'class' => 'c-recent-activity--flex',
                ])           
            </div>
        </div>

    </div>
@endsection
