@php
    $top20 = $level->prs()->with('kuski')->where('prs.pack_id', $pack->id)->limit(20)->get();
    // $top3 = $level->top(3);
@endphp
@if($top20->count())
    <ol class="c-positions-list c-positions-list--striped">
        @foreach($top20 as $time)
            <li>
                <span>
                     @include('kuskis.partials.compactKuskiIdentifier', ['kuski' => $time->kuski])
                </span>
                <span class="float-right">
                    {{ $time->time }}
                </span>
            </li>
        @endforeach
    </ol>
@else
    <span class="c-text--quiet">No finishes</span>
@endif
