<h3>Times for {{ $level->name }} (EOL id: {{ $level->id }})</h3>

<a href="{{ $times->appends(request()->input())->previousPageUrl() }}" class="c-link">&lt;</a>
<a href="{{ $times->appends(request()->input())->nextPageUrl() }}" class="c-link">&gt;</a>
@if($missingKuskiCount)
    <p>Missing data for {{ $missingKuskiCount }} kuskis out of {{ $kuskiCount }}</p>
@endif

<div class="c-table c-table--condensed c-table--striped">
    <div class="c-table__row c-table__row--heading">
        <span class="c-table__cell">Kuski</span>
        <span class="c-table__cell">Nationality</span>
        <span class="c-table__cell">Team</span>
        <span class="c-table__cell">Time</span>
        <span class="c-table__cell">Date</span>
        <span class="c-table__cell">Eol Index</span>
        @if(user() && (user()->isAdmin() || user()->isModerator()))
            <span class="c-table__cell"></span>
        @endif
    </div>

    @foreach($times as $time)
        <div class="c-table__row">
            <span class="c-table__cell">{{ ++$loop->index + (($times->currentPage() - 1) * $times->perPage()) }}.&nbsp;<a href="{{ route('kuskis.show', $time->kuski) }}" class="c-link">{{ isset($time->kuski) ? $time->kuski->name : '?'}}</a></span>
            <span class="c-table__cell"><span class="flag-icon flag-icon-{{ isset($time->kuski) ? $time->kuski->nationality : '?'}}"></span></span>
            <span class="c-table__cell">{{ isset($time->kuski) ? $time->kuski->team : '?' }}</span>
            <span class="c-table__cell">{{ $time->time }}</span>
            <span class="c-table__cell">{{ $time->driven_at }}</span>
            <span class="c-table__cell">{{ $time->id }}</span>
            @if(user() && (user()->isAdmin() || user()->isModerator()))
                <span class="c-table__cell">
                    @unless($time->isGlobalBanned())
                        {!! Form::open([
                            'method' => 'POST',
                            'route' => ['levels.times.ban', $level, $time->id],
                        ]) !!}
                            @include('blaze.forms.submit', [
                                'text' => 'Ban'
                            ])
                        {!! Form::close() !!}
                    @endunless
                </span>
            @endif
        </div>
    @endforeach
</div>
