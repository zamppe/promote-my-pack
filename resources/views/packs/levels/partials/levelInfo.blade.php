
<div class="o-grid o-grid--wrap">
    <div class="o-grid__cell o-grid__cell--width-100">
        @include('levels.partials.image', [
            'w' => 500,
            'h' => 340
        ])
    </div>

    <div class="o-grid__cell o-grid__cell--width-100 u-letter-box--small">
        @foreach($allTags as $tag)
            @if(user())
                {!! Form::open([
                    'route' => ['manage.levels.tags.toggle-tag', $level, $tag],
                    'method' => 'PUT',
                    'class' => 'form--inline'
                ]) !!}
                <button class="c-button c-button--ghost-success u-xsmall c-text--loud
                    {{ $tags->contains($tag) ? 'c-button--active' : '' }}"
                >
                    {{ $tag->name }}
                </button>
                {!! Form::close() !!}
            @else
                @if($tags->contains($tag))
                    <span class="c-badge c-badge--success">{{ $tag->name }}</span>
                @endif
            @endif
        @endforeach
    </div>
</div>

