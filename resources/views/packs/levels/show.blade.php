@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge">
        <div class="o-grid o-grid--no-gutter o-grid--wrap">
            <div class="o-grid__cell o-grid__cell--width-100 o-grid__cell--width-50@medium">
                {!! Form::open([
                    'route' => ['packs.levels.show', $pack, $level],
                    'method' => 'GET',
                    'class' => 'c-form'
                ]) !!}

                @include('levels.partials.searchForm')
                {!! Form::hidden('pack', $pack->id, []) !!}
                {!! Form::hidden('level', $level->id, []) !!}

                <div class="u-window-box--medium">
                    @include('blaze.forms.submit', [
                        'text' => 'Go'
                    ])
                </div>

                {!! Form::close() !!}
            </div>

            <div class="o-grid__cell o-grid__cell--width-100 o-grid__cell--width-50@medium">
                @include('packs.levels.partials.levelInfo')
            </div>
        </div>

        <div class="u-pillar-box--medium">
            @include('packs.levels.partials.timesTable')
        </div>
    </div>
@endsection
