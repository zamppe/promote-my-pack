@extends('layouts.app')

@section('content')
    <div class="o-container o-container--super">
        @include('packs.partials.childNav')

        <div class="u-pillar-box--medium">
            <h2 class="c-heading">Records</h2>

            @include('components.pagination', ['rows' => $levels])

            <div class="c-picture-list">
                @foreach($levels as $level)
                    <div class="c-picture-row js-picture-row">
                        @php
                            $media = $level->getMedia('image')->first();
                        @endphp

                        @if ($media)
                            <div class="lightgallery c-picture-row__picture">
                                <a href="{{ $media ? GlideImage::load($media->getDiskPath(), ['q' => 60, 'w' => 640, 'h' => 480, 'fit' => 'crop']) : ''}}">
                                    @include('levels.partials.image', [
                                        'w' => 180,
                                        'h' => 100,
                                    ])
                                </a>
                            </div>
                        @endif

                        <div class="c-picture-row__body">
                            <div class="c-level">
                                <div class="c-level__header">
                                    <h3 class="c-heading m-0 p-0">
                                        <a href="{{ route('packs.levels.show', [$pack, $level]) }}" class="c-link c-link--success">
                                            {{ $level->full_name }}
                                        </a>
                                    </h3>

                                </div>

                                <div class="c-level__body js-packs-level-row-top-lists">
                                    @include('packs.partials.topLists', ['n' => 3])
                                </div>
                                <p class="c-link u-small mb-0 u-centered js-packs-expand-level-row" data-url="{{ route('packs.levels.get-extra-info', [$pack, $level]) }}">Show more/less results</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
