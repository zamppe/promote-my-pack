@extends('layouts.app')

@section('content')
<div class="o-container o-container--xlarge">
    <div class="o-grid o-grid--wrap">
        <div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--offset-33@medium o-grid__cell--width-100">
            <h1 class="c-heading">Register</h1>

            {!! Form::open([
                'url' => url('/register'),
                'method' => 'POST'
            ]) !!}
                @include('blaze.forms.text', [
                    'input' => 'nickname',
                    'label' => 'Name',
                    'attributes' => ['required', 'autofocus']
                ])

                @include('blaze.forms.email', [
                    'input' => 'email',
                    'label' => 'E-Mail Address',
                    'attributes' => ['required']
                ])

                @include('blaze.forms.password', [
                    'input' => 'password',
                    'label' => 'Password',
                    'attributes' => ['required']
                ])

                @include('blaze.forms.password', [
                    'input' => 'password_confirmation',
                    'label' => 'Confirm Password',
                    'attributes' => ['required']
                ])

                <div class="o-form-element">
                    {!! Form::submit('Register', ['class' => 'c-button c-button--brand c-button--block']) !!}
                </div>

                <a href="{{ route('login') }}" class="c-link">
                    Already registered? Login
                </a>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
