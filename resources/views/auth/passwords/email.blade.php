@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="o-container o-container--xlarge">
    <div class="o-grid o-grid--wrap">
        <div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--offset-33@medium o-grid__cell--width-100">
            <h1 class="c-heading">Reset Password</h1>

            @if (session('status'))
                {{ session('status') }}
            @endif

            {!! Form::open([
                'url' => url('/password/email'),
                'method' => 'POST'
            ]) !!}
                @include('blaze.forms.email', [
                    'input' => 'email',
                    'label' => 'E-Mail Address',
                    'attributes' => ['required']
                ])

                <div class="o-form-element">
                    {!! Form::submit('Send password reset link', ['class' => 'c-button c-button--brand c-button--block']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
