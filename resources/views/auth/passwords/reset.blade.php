@extends('layouts.app')

@section('content')
<div class="o-container o-container--xlarge">
    <div class="o-grid o-grid--wrap">
        <div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--offset-33@medium o-grid__cell--width-100">
            <h1>Reset Password</h1>

            @if (session('status'))
                {{ session('status') }}
            @endif

            {!! Form::open([
                'url' => url('/password/reset'),
                'method' => 'POST'
            ]) !!}
                {!! Form::hidden('token', $token, []) !!}

                @include('blaze.forms.email', [
                    'input' => 'email',
                    'label' => 'E-Mail Address',
                    'attributes' => ['required', 'autofocus']
                ])

                @include('blaze.forms.password', [
                    'input' => 'password',
                    'label' => 'Password',
                    'attributes' => ['required']
                ])

                @include('blaze.forms.password', [
                    'input' => 'password_confirmation',
                    'label' => 'Confirm Password',
                    'attributes' => ['required']
                ])

                <div class="o-form-element">
                    {!! Form::submit('Reset password', ['class' => 'c-button c-button--brand c-button--block']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
