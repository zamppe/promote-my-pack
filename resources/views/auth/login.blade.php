@extends('layouts.app')

@section('content')
<div class="o-container o-container--xlarge">
    <div class="o-grid">
        <div class="
            c-login-form
            o-grid__cell o-grid__cell--width-100
            o-grid__cell-width-66@xsmall
            o-grid__cell--width-50@small
            o-grid__cell--width-33@medium
            ">
            <h1 class="c-heading">Please login</h1>

            {!! Form::open([
                'url' => url('/login'),
                'method' => 'POST'
            ]) !!}
                <div class="o-form-element">
                    <div class="c-input-group c-input-group--stacked">
                        <div class="o-field">
                            {!! Form::email('email', null, [
                                'id' => 'email',
                                'class' => 'c-field',
                                'placeholder' => 'Email address',
                                'required',
                                'autofocus'
                            ]) !!}
                        </div>

                        <div class="o-field">
                            {!! Form::password('password', [
                                'id' => 'password',
                                'class' => 'c-field',
                                'placeholder' => 'Password',
                                'required'
                            ]) !!}
                        </div>
                    </div>

                    @include('blaze.forms.error', ['input' => 'email'])

                    @include('blaze.forms.error', ['input' => 'password'])
                </div>

                <label class="o-form-element c-label">
                    <input type="checkbox" name="remember">
                    Remember me
                </label>

                <div class="o-form-element">
                    {!! Form::submit('Login', ['class' => 'c-button c-button--brand c-button--block']) !!}
                </div>

                <a href="{{ url('/password/reset') }}" class="c-link">
                    Forgot Your Password?
                </a>

                <a href="{{ url('/register') }}" class="c-link">
                    Register
                </a>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
