@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge">
        <div class="u-absolute-center">
            <p class="c-paragraph">No rights to resource</p>
            <p style="font-size: 100px; text-align: center; margin: 0;">:(</p>
        </div>
    </div>
@endsection
