@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge u-pillar-box--medium">
        @if(user() && (user()->isModerator() || user()->isAdmin()))
            {!! Form::open([
                'route' => 'news.store',
                'method' => 'POST'
            ]) !!}
                <h3 class="c-heading">Write news</h3>
                <div class="o-grid o-grid--no-gutter o-grid--medium-full">
                    <div class="o-grid__cell o-grid__cell--width-50">
                        @include('news.partials.editForm')
                    </div>
                </div>

                <div class="u-letter-box--medium">
                    @include('blaze.forms.submit', [
                        'text' => 'Save'
                    ])
                </div>
            {!! Form::close() !!}
        @endif

        @if ($allNews->count())
            <h1 class="c-heading">News</h1>
        @endif

        @foreach($allNews as $newsItem)
            <div class="u-letter-box--small">
                <article class="c-card c-card--higher u-bg-beige">
                    <header class="c-card__item c-card__item--divider u-bg-green-light">{{ $newsItem->title }}</header>

                    <section class="c-card__body">
                        {!! $newsItem->content !!}
                    </section>

                    <footer class="c-card__footer">
                        <h4 class="c-heading">
                            <div class="c-heading__sub">{{ $newsItem->user->nickname }}, {{ $newsItem->createdAt->diffForHumans() }}</div>
                        </h4>

                        @if(user() && (user()->isAdmin() || user()->isModerator()))
                            <a href="{{ route('news.edit', [$newsItem]) }}" class="c-button c-button--success c-text--loud u-small">Edit</a>
                            {!! Form::open([
                                'route' => ['news.destroy', $newsItem],
                                'method' => 'DELETE',
                                'class' => 'form--inline'
                            ]) !!}
                                {!! Form::submit('Delete', ['class' => 'c-button c-button--error c-text--loud u-small']) !!}
                            {!! Form::close() !!}
                        @endif
                    </footer>
                </article>
            </div>
        @endforeach
    </div>
@endsection
