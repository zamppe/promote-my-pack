@if($rows->count())
    <ol class="c-positions-list">
        @foreach($rows as $row)
            <li>
                @include('kuskis.partials.compactKuskiIdentifier', ['kuski' => $row->kuski])
                <span class="float-right">{{ mstostrtime($row->total_time) }}</span>
            </li>
        @endforeach
    </ol>
@else
    <p class="c-text--quiet">No totaltimes</p>
@endif
