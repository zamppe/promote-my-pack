@if ($rows->lastPage() > 1)
    <div class="pagination">
        <?php for ($pageIndex = 1; $pageIndex <= $rows->lastPage(); $pageIndex++): ?>
            <a class="c-link pagination__item" href="{{ $rows->url($pageIndex) }}">{{ ($pageIndex - 1) * $rows->perPage() + 1}}&ndash;{{ $pageIndex * $rows->perPage() }}</a>
        <?php endfor; ?>
    </div>
@endif
