<span class="date">{{ $date->format('Y-m-d H:i:s') }} <span class="u-color-grey-darker">{{ $date->diffForHumans() }}</span></span>
