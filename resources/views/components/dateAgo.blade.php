<span class="c-tooltip c-tooltip--top" aria-label="{{ $date->format('Y-m-d H:i:s') }}">{{ $date->diffForHumans() }}</span>
