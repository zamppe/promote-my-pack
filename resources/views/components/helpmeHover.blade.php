@if(isset($text) && $text)
    <span aria-label="{{$text}}" class="c-tooltip c-tooltip--bottom" style="cursor: pointer"><span class="u-color-blue-dark u-large c-text--loud">?</span></span>
@endif
