<div class="o-grid o-grid--wrap o-grid--no-gutter">
    <div class="o-grid__cell o-grid__cell--width-50@medium o-grid__cell--width-100">
        @include('blaze.forms.text', ['input' => 'name', 'label' => 'Name'])

    </div>
</div>
<div class="o-grid o-grid--wrap o-grid--no-gutter">
    <div class="o-grid__cell o-grid__cell--width-50@medium o-grid__cell--width-100">
        @include('blaze.forms.textarea', ['input' => 'description', 'label' => 'Description'])
    </div>
</div>

<div class="o-grid o-grid--wrap o-grid--no-gutter">
    <div class="o-grid__cell o-grid__cell--width-50">
         <div class="o-form-element">
            <label class="c-label" for="image">Image:
                {!! Form::file('image', ['class' => 'c-field']) !!}
            </label>
        </div>
    </div>
</div>
<div class="o-grid o-grid--wrap">
    <div class="o-grid__cell o-grid__cell--width-50">
        @if(isset($achievement) && $achievement->getMedia('achievement')->first())
            <img class="o-image"
            src="{{ GlideImage::load($achievement->getMedia('achievement')->first()->getDiskPath(), ['q' => 60, 'w' => 200, 'h' => 200, 'fit' => 'max']) }}"
            alt="{{ $achievement->name }}" />
        @endif
    </div>
</div>
