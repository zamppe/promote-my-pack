<div class="o-grid__cell o-grid__cell--width-50">
<h4 class="c-heading">Grant achievement</h4>
    <div class="c-table">
        {{-- <div class="c-table__caption">Kuskis</div> --}}

        <div class="c-table__row c-table__row--heading">
            <span class="c-table__cell">Kuski</span>
            <span class="c-table__cell">&nbsp;</span>

        </div>
        @if($kuskis->count())
            @foreach($kuskis as $kuski)
                <div class="c-table__row">
                    <span class="c-table__cell">{{ $kuski->name }}</span>
                    <span class="c-table__cell">
                        {!! Form::open([
                            'route' => ['manage.packs.customAchievements.grant-to-kuski', $pack, $achievement],
                            'method' => 'POST',
                            'class' => 'js-custom-achievement-grant-to-kuski'
                        ]) !!}
                            <input type="hidden" name="k_id" value="{{ $kuski->id }}">
                            @include('blaze.forms.submit', [
                                'text' => 'Grant'
                            ])
                        {!! Form::close() !!}
                    </span>
                </div>
            @endforeach
        @else
            <p class="c-text--quiet">No results</p>
        @endif
    </div>
</div>
<div class="o-grid__cell o-grid__cell--width-50">
    <h4 class="c-heading">Achieves</h4>
    <div class="c-table">
        {{-- <div class="c-table__caption">Kuskis</div> --}}

        <div class="c-table__row c-table__row--heading">
            <span class="c-table__cell">Kuski</span>
            <span class="c-table__cell">&nbsp;</span>

        </div>

        @if($achieves->count())
            @foreach($achieves as $achieve)
                <div class="c-table__row">
                    <span class="c-table__cell">{{ $achieve->kuski ? $achieve->kuski->name : ''}}</span>
                    <span class="c-table__cell">
                        {!! Form::open([
                            'route' => ['manage.packs.customAchievements.remove-from-kuski', $pack, $achievement],
                            'method' => 'DELETE',
                            'class' => 'js-custom-achievement-remove-from-kuski'
                        ]) !!}
                            <input type="hidden" name="achieve_id" value="{{ $achieve->id }}">
                            @include('blaze.forms.submit', [
                                'text' => 'Remove'
                            ])
                        {!! Form::close() !!}
                    </span>
                </div>
            @endforeach
        @else
            <p class="c-text--quiet">No results</p>
        @endif
    </div>
</div>
