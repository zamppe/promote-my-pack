<div class="o-grid o-grid--wrap o-grid--no-edge-gutter">
    <div class="o-grid__cell o-grid__cell--width-50">
        <label class="c-label o-form-element">
            Name:
            @php
                $class = "c-field c-field--label";
                if ($errors->has('name')) {
                    $class .= " c-field--error";
                }
            @endphp
            {!! Form::text('name', null, ['class' => $class]) !!}
            @if($errors->has('name')) <div class="c-hint c-hint--static c-hint--error"> {{ $errors->first('name')}} </div> @endif
        </label>

        <label class="c-label o-form-element">
            Compare with:
            {!! Form::select('consider', [1 => 'Best finishes by kuskis', 2 => 'All finishes'], isset($achievement) ? $achievement->consider : 1, ['class' => 'c-field']) !!}
        </label>

        <label class="c-label o-form-element">
            Comparison type:
            {!! Form::select('comparator', [1 => 'Under', 2 => 'Exact'], isset($achievement) ? $achievement->comparator : 1, ['class' => 'c-field']) !!}
        </label>

        <label class="c-label o-form-element">
            Target type:
            {!! Form::select('target_type', [1 => 'Time', 2 => 'Total time'], isset($achievement) ? $achievement->target_type : 1, ['class' => 'c-field']) !!}
        </label>

        <label class="c-label o-form-element">
            Target value type:
            {!! Form::select('target_value_type', [1 => 'Time', 2 => 'Position'], isset($achievement) ? $achievement->target_value_type : 1, ['class' => 'c-field']) !!}
            <div class="c-hint c-hint--static"> Define how to interpret target value </div>
        </label>

        <label class="c-label o-form-element">
            Target value:
            @php
                $class = "c-field c-field--label";
                if ($errors->has('target_value')) {
                    $class .= " c-field--error";
                }
            @endphp
            {!! Form::text('target_value', isset($achievement) ? $achievement->targetFormatted : '', ['class' => $class]) !!}
            @if($errors->has('target_value')) <div class="c-hint c-hint--static c-hint--error"> {{ $errors->first('target_value')}} </div>
            @else <div class="c-hint c-hint--static"> Position integer or time string in 05:43,21 format </div>
            @endif
        </label>

        <label class="c-label o-form-element">
            Apply on:
            @php
                $class = "c-field c-field--label";
                if ($errors->has('apply_on')) {
                    $class .= " c-field--error";
                }
            @endphp
            {!! Form::text('apply_on', null, ['class' => $class]) !!}
            @if($errors->has('apply_on')) <div class="c-hint c-hint--static c-hint--error"> {{ $errors->first('apply_on')}} </div>
            @else <div class="c-hint c-hint--static"> Integer [0, number of levels]. Use 0 to apply on every selected level. </div>
            @endif
        </label>

        <div class="o-grid o-grid--wrap o-grid--no-gutter">
            <div class="o-grid__cell o-grid__cell--width-100">
                 <div class="o-form-element">
                    <label class="c-label" for="image">Image:
                        {!! Form::file('image', ['class' => 'c-field']) !!}
                    </label>
                </div>
            </div>
        </div>
        <div class="o-grid o-grid--wrap">
            <div class="o-grid__cell o-grid__cell--width-100">
                @if(isset($achievement) && $achievement->getMedia('achievement')->first())
                    <img class="o-image"
                    src="{{ GlideImage::load($achievement->getMedia('achievement')->first()->getDiskPath(), ['q' => 60, 'w' => 200, 'h' => 200, 'fit' => 'max']) }}"
                    alt="{{ $achievement->name }}" />
                @endif
            </div>
        </div>

    </div>
    <div class="o-grid__cell o-grid__cell--width-50">
        @include('blaze.forms.textarea', ['input' => 'description', 'label' => 'Description'])

        <div class="achievement-levels">
            <fieldset class="o-fieldset">
                <legend class="o-fieldset__legend">Select levels:</legend>
                <label class="c-field c-field--choice p-0">
                    {!! Form::checkbox('select_all', 1, false, ['class' => 'js-checkbox-select-all']) !!}
                    Select all
                </label>
                @foreach($levels as $key => $level)
                <label class="c-field c-field--choice p-0">
                    {!! Form::hidden('levels[' . $level->id . ']', 0, false, []) !!}
                    {!! Form::checkbox('levels[' . $level->id . ']', 1, isset($achievement) && $achievement->levels && in_array($level->id, $achievement->levels) ? true : false, ['class' => 'js-checkbox']) !!}
                    {{ $level->name }}
                </label>
                @endforeach
                @if($errors->has('levels')) <div class="c-hint c-hint--static c-hint--error"> {{ $errors->first('levels')}} </div> @endif
            </fieldset>
        </div>
    </div>
</div>
