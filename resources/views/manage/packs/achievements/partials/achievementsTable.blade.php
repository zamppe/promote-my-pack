<div class="o-grid o-grid--no-gutter">
    <div class="o-grid__cell o-grid__cell--width-100 u-centered">
        <div class="c-table">
            {{-- <div class="c-table__caption">Kuskis</div> --}}

            <div class="c-table__row c-table__row--heading">
                <span class="c-table__cell">Name</span>
                <span class="c-table__cell">Type</span>
                <span class="c-table__cell">Achieved by</span>
                <span class="c-table__cell" style="flex-grow: 3">Computed description</span>
                <span class="c-table__cell">&nbsp;</span>
            </div>

            @foreach($pack->targetAchievements as $achievement)
                <div class="c-table__row">
                    <span class="c-table__cell c-text--loud"><a href="{{ route('manage.packs.targetAchievements.edit', [$pack, $achievement->id]) }}" class="c-link">{{ $achievement->name }}</a></span>
                    <span class="c-table__cell">{{ className($achievement) }}</span>
                    <span class="c-table__cell c-text--loud">{{ $achievement->achieves->count() }} / {{ $pack->getKuskis()->count() }}</span>
                    <span class="c-table__cell" style="flex-grow: 3"><i>{{ rtrim($achievement->computedDescription, ':') }}...</i></span>
                    <div class="c-table__cell">
                        {!! Form::open([
                            'route' => ['manage.packs.targetAchievements.destroy', $pack, $achievement],
                            'method' => 'delete'
                        ]) !!}
                            {!! Form::submit('Destroy', ['class' => 'c-button c-button--error c-text--loud u-xsmall']) !!}
                        {!! Form::close() !!}

                    </div>
                </div>
            @endforeach
            @foreach($pack->customAchievements as $achievement)
                <div class="c-table__row">
                    <span class="c-table__cell c-text--loud"><a href="{{ route('manage.packs.customAchievements.edit', [$pack, $achievement->id]) }}" class="c-link">{{ $achievement->name }}</a></span>
                    <span class="c-table__cell">{{ className($achievement) }}</span>
                    <span class="c-table__cell c-text--loud">{{ $achievement->achieves->count() }} / {{ $pack->getKuskis()->count() }}</span>
                    <span class="c-table__cell" style="flex-grow: 3">N/A</span>
                    <div class="c-table__cell">
                        {!! Form::open([
                            'route' => ['manage.packs.customAchievements.destroy', $pack, $achievement],
                            'method' => 'delete'
                        ]) !!}
                            {!! Form::submit('Destroy', ['class' => 'c-button c-button--error c-text--loud u-xsmall']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
