@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge u-letter-box--super">
        @include('manage.packs.achievements.partials.returnLink')

        <h3>Create a new target achievement for pack {{ $pack->name }}</h3>

        @if($levels)
            {!! Form::open([
                'route' => ['manage.packs.targetAchievements.store', $pack],
                'method' => 'post',
                'files' => true
            ]) !!}
            @include('manage.packs.achievements.partials.targetAchievementForm')
            <div class="u-letter-box--medium">
                @include('blaze.forms.submit', [
                    'text' => 'Create'
                ])
            </div>
            {!! Form::close() !!}
        @else
            <h3>Pack has no levels! Add some levels first</h3>
        @endif

        @include('manage.packs.achievements.partials.returnLink')
    </div>
@endsection
