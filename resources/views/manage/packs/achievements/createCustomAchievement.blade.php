@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge u-letter-box--super">
        @include('manage.packs.achievements.partials.returnLink')

        <h3>Create a new custom achievement for pack {{ $pack->name }}</h3>

        @if($levels)
            {!! Form::open([
                'route' => ['manage.packs.customAchievements.store', $pack],
                'method' => 'post',
                'files' => true,
            ]) !!}
            @include('manage.packs.achievements.partials.customAchievementForm')
            <div class="u-letter-box--medium">
                {!! Form::submit('Create', ['class' => 'c-button c-button--success']) !!}
            </div>
            {!! Form::close() !!}
        @else
            <h3>Pack has no levels! Add some levels first</h3>
        @endif

        @include('manage.packs.achievements.partials.returnLink')
    </div>
@endsection
