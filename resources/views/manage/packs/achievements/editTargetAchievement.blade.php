@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge u-letter-box--super">
        @include('manage.packs.achievements.partials.returnLink')

        <h3>Editing target achievement {{ $achievement->name }} for pack {{ $pack->name }}</h3>

        @if($levels)
            {!! Form::model($achievement, [
                'route' => ['manage.packs.targetAchievements.update', $pack, $achievement],
                'method' => 'put',
                'files' => true,
            ]) !!}
                @include('manage.packs.achievements.partials.targetAchievementForm')

                <div class="u-letter-box--medium">
                    @include('blaze.forms.submit', [
                        'text' => 'Update'
                    ])
                </div>
            {!! Form::close() !!}
        @else
            <h3>Pack has no levels! Add some levels first</h3>
        @endif

        <h4>Computed description</h4>

        <p>{{ $achievement->computedDescription }}</p>

        @include('manage.packs.achievements.partials.returnLink')
    </div>
@endsection
