@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge u-letter-box--super">
        @include('manage.packs.achievements.partials.returnLink')

        <section class="u-letter-box--medium">
            <h2 class = "c-heading">Editing custom achievement {{ $achievement->name }} for pack {{ $pack->name }}</h2>

            {!! Form::model($achievement, [
                'route' => ['manage.packs.customAchievements.update', $pack, $achievement],
                'method' => 'put',
                'files' => true,
            ]) !!}
                @include('manage.packs.achievements.partials.customAchievementForm')

                <div class="u-letter-box--medium">
                    @include('blaze.forms.submit', [
                        'text' => 'Update'
                    ])
                </div>
            {!! Form::close() !!}
        </section>

        <section class="u-letter-box--medium">
            <h2 class="c-heading">Grant to kuskis</h2>

            <div class="o-grid o-grid--no-gutter">
                <div class="o-grid__cell o-grid__cell--width-50@medium o-grid__cell--width-100">
                    {!! Form::open([
                        'route' => ['manage.packs.customAchievements.edit', $pack, $achievement],
                        'method' => 'get',
                        'class' => 'js-edit-custom-achievement-kuski-search'
                    ]) !!}
                        @include('blaze.forms.inputGroup', [
                            'input' => 'kuski',
                            'label' => 'Filter results by kuski name',
                            'button' => 'Go',
                            'extraClasses' => 'js-kuski'
                        ])
                    {!! Form::close() !!}
                </div>
            </div>

            <div class="o-grid o-grid--no-edge-gutter js-edit-custom-achievement-kuski-results">
                @include('manage.packs.achievements.partials.customAchievementKuskis')
            </div>
        </section>

        @include('manage.packs.achievements.partials.returnLink')
    </div>
@endsection
