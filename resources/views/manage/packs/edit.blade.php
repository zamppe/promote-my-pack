@extends('layouts.app')

@section('content')
    @parent
    <div class="o-container o-container--xlarge u-letter-box--super">
        <h1 class="c-heading u-centered">{{ $pack->name }}</h1>
        @if(isset($timesError))
            <ul class="errors">
                <li class="errors__item">
                    {{ $timesError }}
                </li>
            </ul>
        @endif

        @include('manage.packs.partials.adminOperations')

        <div class="ionTabs c-tabs c-tabs--info" id="tabs_1" data-name="tabs--pack-edit">
            <div class="ionTabs__head c-tabs__headings">
                <div class="ionTabs__tab c-tab-heading" data-target="tab--general">General</div>
                <div class="ionTabs__tab c-tab-heading" data-target="tab--news">News</div>
                <div class="ionTabs__tab c-tab-heading" data-target="tab--levels">Levels</div>
                <div class="ionTabs__tab c-tab-heading" data-target="tab--bans">Bans</div>
                <div class="ionTabs__tab c-tab-heading" data-target="tab--achievements">Achievements</div>
            </div>
            <div class="ionTabs__body">
                <div class="ionTabs__item c-tabs__tab" data-name="tab--general">
                    @include('manage.packs.tabs.tabGeneral')
                </div>
                <div class="ionTabs__item c-tabs__tab" data-name="tab--news">
                    @include('manage.packs.tabs.tabNews')
                </div>
                <div class="ionTabs__item c-tabs__tab" data-name="tab--levels">
                    @include('manage.packs.tabs.tabLevels')
                </div>
                <div class="ionTabs__item c-tabs__tab js-pack-edit-tab-bans" data-name="tab--bans">
                    @include('manage.packs.tabs.tabBans')
                </div>
                <div class="ionTabs__item c-tabs__tab js-pack-edit-tab-achievements" data-name="tab--achievements">
                    @include('manage.packs.tabs.tabAchievements')
                </div>
                <div class="ionTabs__preloader"></div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>
        $.ionTabs("#tabs_1", {
            type: "storage"
        });
    </script>
@endsection
