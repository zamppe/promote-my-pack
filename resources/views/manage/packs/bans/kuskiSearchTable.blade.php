<div class="o-grid o-grid--no-gutter">
    <div class="o-grid__cell o-grid__cell--width-100 u-centered">
        <div class="c-table c-table--striped">
            {{-- <div class="c-table__caption">Kuskis</div> --}}

            <div class="c-table__row c-table__row--heading">
                <span class="c-table__cell">Kuski</span>
                <span class="c-table__cell">Nationality</span>
                <span class="c-table__cell">Team</span>
                <span class="c-table__cell"></span>
            </div>

            @foreach($kuskiSearchResults as $kuski)
                <div class="c-table__row">
                    <span class="c-table__cell c-text--loud"><a href="{{ route('kuskis.show', $kuski->id) }}" class="c-link">{{ $kuski->name }}</a></span>
                    <span class="c-table__cell"><span class="flag-icon flag-icon--table flag-icon-{{ $kuski->nationality }}"></span></span>
                    <span class="c-table__cell">{{ $kuski->team }}</span>
                    <span class="c-table__cell">
                        @if(!$pack->isKuskiBanned($kuski->id))
                            {!! Form::open([
                                'route' => ['manage.packs.kuskis.ban', $pack, $kuski->id],
                                'method' => 'POST'
                            ]) !!}
                                @include('blaze.forms.submit', [
                                    'text' => 'Ban'
                                ])
                            {!! Form::close() !!}
                        @endif
                    </span>
                </div>
            @endforeach
        </div>
    </div>
</div>
