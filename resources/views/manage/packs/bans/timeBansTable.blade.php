<div class="o-grid">
    <div class="o-grid__cell o-grid__cell--width-100 u-centered">
        <div class="c-table">
            {{-- <div class="c-table__caption">Kuskis</div> --}}

            <div class="c-table__row c-table__row--heading">
                <span class="c-table__cell">Kuski</span>
                <span class="c-table__cell">Nationality</span>
                <span class="c-table__cell">Team</span>
                <span class="c-table__cell">Time</span>
                <span class="c-table__cell">Level</span>
                <span class="c-table__cell"></span>
            </div>

            @foreach($pack->localTimeBans as $timeBan)
                <div class="c-table__row">
                    <span class="c-table__cell c-text--loud"><a href="{{ route('kuskis.show', $timeBan->kuski->id) }}">{{ $timeBan->kuski->name }}</a></span>
                    <span class="c-table__cell"><span class="flag-icon flag-icon--table flag-icon-{{ $timeBan->kuski->nationality }}"></span></span>
                    <span class="c-table__cell">{{ $timeBan->kuski->team }}</span>
                    <span class="c-table__cell">{{ $timeBan->time->time }}</span>
                    <span class="c-table__cell">{{ $timeBan->level->name }}</span>
                    <span class="c-table__cell">
                        @if($timeBan->time->isBannedToPack($pack))
                            <a href="{{ route('manage.packs.levels.times.unban', [$pack, $timeBan->level, $timeBan->time->id]) }}"
                                class="c-button c-button--success u-small c-text--loud u-color-white"
                                onclick="event.preventDefault();
                                         document.getElementById('js-packs-times-unban' + {{ $timeBan->time->id }}).submit();">
                                Remove
                            </a>
                            {!! Form::open([
                                'method' => 'DELETE',
                                'route' => ['manage.packs.levels.times.unban', $pack, $timeBan->level, $timeBan->time->id],
                                'id' => 'js-packs-times-unban' . $timeBan->time->id ,
                                'style' => 'display: none;'
                            ]) !!}
                            {!! Form::close() !!}
                        @endif
                    </span>
                </div>
            @endforeach
        </div>
    </div>
</div>
