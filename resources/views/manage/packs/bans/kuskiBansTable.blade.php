<div class="o-grid o-grid--no-gutter">
    <div class="o-grid__cell o-grid__cell--width-100 u-centered">
        <div class="c-table c-table--striped">
            {{-- <div class="c-table__caption">Kuskis</div> --}}

            <div class="c-table__row c-table__row--heading">
                <span class="c-table__cell">Kuski</span>
                <span class="c-table__cell">Nationality</span>
                <span class="c-table__cell">Team</span>
                <span class="c-table__cell"></span>
            </div>

            @foreach($pack->localKuskiBans as $kuskiBan)
                <div class="c-table__row">
                    <span class="c-table__cell c-text--loud"><a href="{{ route('kuskis.show', $kuskiBan->kuski->id) }}" class="c-link">{{ $kuskiBan->kuski->name }}</a></span>
                    <span class="c-table__cell"><span class="flag-icon flag-icon--table flag-icon-{{ $kuskiBan->kuski->nationality }}"></span></span>
                    <span class="c-table__cell">{{ $kuskiBan->kuski->team }}</span>
                    <span class="c-table__cell">
                        @if($pack->isKuskiBanned($kuskiBan->kuski->id))
                            {!! Form::open([
                                'route' => ['manage.packs.kuskis.unban', $pack, $kuskiBan->kuski->id],
                                'method' => 'DELETE'
                            ]) !!}
                                @include('blaze.forms.submit', [
                                    'text' => 'Unban'
                                ])
                            {!! Form::close() !!}
                        @endif
                    </span>
                </div>
            @endforeach
        </div>
    </div>
</div>
