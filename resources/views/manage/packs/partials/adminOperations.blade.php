@if(user()->isAdmin())
    <div class="u-letter-box--medium">
        {!! Form::open([
            'route' => ['manage.packs.update-times', $pack],
            'method' => 'POST',
            'style' => 'display: inline;'
        ]) !!}
            @include('blaze.forms.submit', [
                'text' => 'Update all times'
            ])
        {!! Form::close() !!}

        {!! Form::open([
            'route' => ['manage.packs.rebuild-history', $pack],
            'method' => 'POST',
            'style' => 'display: inline;'
        ]) !!}
            @include('blaze.forms.submit', [
                'text' => 'Rebuild history'
            ])
        {!! Form::close() !!}

        {!! Form::open([
            'route' => ['manage.packs.delete-times', $pack],
            'method' => 'DELETE',
            'style' => 'display: inline;'
        ]) !!}
            @include('blaze.forms.submit', [
                'text' => 'Delete times'
            ])
        {!! Form::close() !!}

        {!! Form::open([
            'route' => ['manage.packs.process-achievements', $pack],
            'method' => 'POST',
            'style' => 'display: inline;'
        ]) !!}
            @include('blaze.forms.submit', [
                'text' => 'Process achievements'
            ])
        {!! Form::close() !!}
    </div>
@endif
