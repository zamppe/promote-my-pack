<h2 class="c-heading">News</h2>
<div class="o-grid o-grid--no-gutter o-grid--wrap">
    <div class="o-grid__cell o-grid__cell--width-50@medium o-grid__cell--width-100">
        {!! Form::open([
            'route' => ['manage.packs.news.store', $pack],
            'method' => 'POST'
        ]) !!}
            <h3 class="c-heading">Create</h3>

            @include('news.partials.editForm')

            @include('blaze.forms.submit', [
                'text' => 'Save'
            ])
        {!! Form::close() !!}

        @foreach($pack->news as $newsItem)
            <div class="u-letter-box--small">
                <div class="c-card c-card--higher u-bg-beige">
                    <div class="c-card__item c-card__item--divider u-bg-green-light">{{ $newsItem->title }}</div>

                    <div class="c-card__body">
                        <p class="c-paragraph">{!! $newsItem->content !!}</p>
                    </div>

                    <footer class="c-card__footer">
                        <a href="{{ route('manage.packs.news.edit', [$pack, $newsItem->id]) }}" class="c-button c-button--success c-text--loud u-small">Edit</a>
                        {!! Form::open([
                            'route' => ['manage.packs.news.destroy', $pack, $newsItem->id],
                            'method' => 'DELETE',
                            'class' => 'form--inline'
                        ]) !!}
                            {!! Form::submit('Delete', ['class' => 'c-button c-button--error c-text--loud u-small']) !!}
                        {!! Form::close() !!}
                    </footer>
                </div>
            </div>
        @endforeach
    </div>
</div>
