@if($pack->levels->count())
    <h2 class="c-heading">Achievements</h2>

    <p>
        <a href="{{ route('manage.packs.targetAchievements.create', [$pack]) }}" class="c-link">Create a new target achievement</a>&nbsp;<span class=c-text--quiet>These are achievements that will be automatically computed during times update</span>
    </p>

    <p>
        <a href="{{ route('manage.packs.customAchievements.create', [$pack]) }}" class="c-link">Create a new custom achievement</a>&nbsp;<span class=c-text--quiet>These are achievements that you can manually assign to kuskis</span>
    </p>

    @unless($pack->targetAchievements->isEmpty() && $pack->customAchievements->isEmpty())
        <div class="u-letter-box--large">
            @include('manage.packs.achievements.partials.achievementsTable')
        </div>
    @endunless
@else
    <p class="c-text--quiet">Add some levels to the pack before you can create achievements.</p>
@endif
