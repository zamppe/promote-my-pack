<h2 class="c-heading">General</h2>
<div class="o-grid o-grid--no-gutter o-grid--wrap">
    <div class="o-grid__cell o-grid__cell--width-40@medium o-grid__cell--width-100">
        <h3 class="c-heading">Pack options</h3>
        {!! Form::model($pack, [
            'route' => ['manage.packs.update', $pack],
            'method' => 'PUT'
        ]) !!}
            @include('blaze.forms.text', ['input' => 'name', 'label' => 'Pack name'])

            @include('blaze.forms.select', [
                'input' => 'update_frequency',
                'label' => 'Deadline update frequency',
                'optionsArray' => $pack->getUpdateFrequencies(),
                'defaultKey' => $pack->update_frequency,
                'hint' => 'Nightly, every sunday or 1st day of month',
            ])

            <div class="u-letter-box--medium">
                Deadline: {{$pack->deadline}}
                @include('components.helpmeHover', ['text' => 'Only times that are driven before this timestamp are included in the pack'])
            </div>

            <div class="u-letter-box--medium">
                {!! Form::submit('Update', ['class' => 'c-button c-button--success u-small c-text--loud']) !!}
            </div>

        {!! Form::close() !!}
    </div>
</div>
