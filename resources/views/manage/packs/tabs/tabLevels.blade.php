<div class="js-pack-levels">
    @include('manage.packs.levels.partials.levelTable')
</div>

@if($pack->levels->count())
    <h3 class="c-heading">Operations</h3>
    {!! Form::open([
        'route' => ['manage.packs.levels.sort', $pack],
        'method' => 'PUT',
        'class' => 'js-pack-sort'
    ]) !!}
        <button class="text-button u-color-green">
                <i class="fa fa-lg fa-sort-alpha-asc">&nbsp;</i>Sort asc by filename
        </button>
    {!! Form::close() !!}
@endif

<h3 class="c-heading">Add levels</h3>
<div class="o-grid o-grid--wrap o-grid--no-gutter">
    <div class="o-grid__cell o-grid__cell--width-40@small o-grid__cell--width-100">
        <div class="o-grid o-grid--wrap o-grid--no-gutter">
            <div class="o-grid__cell o-grid__cell--width-100">
                {!! Form::open([
                    'route' => ['manage.packs.levels.search', $pack],
                    'method' => 'GET',
                    'class' => 'js-level-search'
                ]) !!}
                    @include('blaze.forms.inputGroup', ['input' => 'keyword', 'label' => 'Search for levels by keyword', 'button' => 'Search'])
                {!! Form::close() !!}
            </div>

            <div class="o-grid__cell o-grid__cell--width-100 o-grid__cell--width-80@small">
                <div class="js-level-search-results"></div>
            </div>
        </div>
    </div>

    <div class="o-grid__cell o-grid__cell--width-40@small o-grid__cell--offset-10@small o-grid__cell--width-100">
        {!! Form::open([
            'route' => ['manage.packs.levels.add', $pack],
            'method' => 'POST',
            'class' => 'js-pack-add-level'
        ]) !!}
            @include('blaze.forms.inputGroup', ['input' => 'level_index', 'label' => 'Add via EOL index', 'button' => 'Add'])
        {!! Form::close() !!}
    </div>

    <div class="o-grid__cell o-grid__cell--width-40@small o-grid__cell--width-100">
        {!! Form::open([
            'route' => ['manage.packs.levels.import', $pack],
            'method' => 'POST',
        ]) !!}
            @include('blaze.forms.select', ['input' => 'pack_name', 'optionsArray' => $EOLPacks, 'label' => 'Import levels from EOL pack'])

            @include('blaze.forms.submit', [
                'text' => 'Import'
            ])
        {!! Form::close() !!}
    </div>
</div>
