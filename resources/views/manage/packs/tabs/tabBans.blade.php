@if($pack->levels->count())
    <h2 class="c-heading">Bans</h2>
    <h3 class="c-heading">Time bans</h3> <span class="c-text--quiet"></span>
    @if(count($pack->localTimeBans))
        <h4>Banned times</h4>
        @include('manage.packs.bans.timeBansTable')
    @else
        <p class="c-text--quiet">No times banned. Ban a time of your choice by opening a level from the Levels tab.</p>
    @endif
    <h3 class="c-heading">Kuski bans</h3>

    @if(count($pack->localKuskiBans))
        <h4>Banned kuskis</h4>
         @include('manage.packs.bans.kuskiBansTable')
    @else
        <p class="c-text--quiet">No kuskis banned, yet.</p>
    @endif

    {!! Form::open([
        'route' => ['manage.packs.edit', $pack],
        'method' => 'GET'
    ]) !!}
        @include('blaze.forms.text', ['input' => 'kuski', 'label' => 'Kuski', 'hint' => 'Type the name of the kuski you wish to ban'])
        <div class="u-letter-box--medium">
            {!! Form::submit('Search', ['class' => 'c-button c-button--success c-text--loud']) !!}
        </div>
    {!! Form::close() !!}

    @if(isset($kuskiSearchResults) && count($kuskiSearchResults))
        <h4>Search results</h4>
        @include('manage.packs.bans.kuskiSearchTable')
    @endif
@else
    <p class="c-text--quiet">Wow, the pack doesn't even have levels yet and you already want to ban people.</p>
@endif

