@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge">
        <h3>Times for {{ $level->name }} (EOL id: {{ $level->id }})</h3>
        <a href="{{ $times->previousPageUrl() }}">&lt;</a>
        <a href="{{ $times->nextPageUrl() }}">&gt;</a>
        @if($missingKuskiCount)
            <p>Missing data for {{ $missingKuskiCount }} kuskis out of {{ $kuskiCount }}</p>
        @endif


        <div class="o-grid">
            <div class="o-grid__cell o-grid__cell--width-100">
                <div class="c-table c-table--condensed">

                    <div class="c-table__row c-table__row--heading">
                        <span class="c-table__cell">Kuski</span>
                        <span class="c-table__cell">Nationality</span>
                        <span class="c-table__cell">Team</span>
                        <span class="c-table__cell">Time</span>
                        <span class="c-table__cell">Date</span>
                        <span class="c-table__cell">Eol Index</span>
                        <span class="c-table__cell">Bans</span>
                    </div>

                    @foreach($times as $time)
                        <div class="c-table__row">
                            <div class="c-table__cell">
                                {{ ++$loop->index + (($times->currentPage() - 1) * $times->perPage()) }}.&nbsp;<a href="{{route('kuskis.show', $time->kuski)}}">{{ isset($time->kuski) ? $time->kuski->name : '?'}}</a>
                            </div>
                            <div class="c-table__cell">
                                <span class="flag-icon flag-icon-{{ isset($time->kuski) ? $time->kuski->nationality : '?'}}"></span>
                            </div>
                            <div class="c-table__cell">
                                {{ isset($time->kuski) ? $time->kuski->team : '?' }}
                            </div>
                            <div class="c-table__cell">
                                {{ $time->time }}
                            </div>
                            <div class="c-table__cell">
                                {{ $time->driven_at }}
                            </div>
                            <div class="c-table__cell">
                                {{ $time->id }}
                            </div>
                            <div class="c-table__cell">
                                @if($time->isBannedToPack($pack))
                                    {!! Form::open([
                                        'method' => 'DELETE',
                                        'route' => ['manage.packs.levels.times.unban', $pack, $level, $time->id],
                                    ]) !!}
                                        @include('blaze.forms.submit', [
                                            'text' => 'Unban'
                                        ])
                                    {!! Form::close() !!}
                                @else
                                    {!! Form::open([
                                        'method' => 'POST',
                                        'route' => ['manage.packs.levels.times.ban', $pack, $level, $time->id],
                                    ]) !!}
                                        @include('blaze.forms.submit', [
                                            'text' => 'Ban'
                                        ])
                                    {!! Form::close() !!}
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
