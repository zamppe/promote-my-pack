@if($pack->levels->count())
    <h2 class="c-heading">Levels</h2>
    <div class="c-table js-sortable" data-url="{{ route('manage.packs.levels.sort-drag', [$pack]) }}">
        <div class="c-table__row c-table__row--heading">
            <span class="c-table__cell">Name</span>
            <span class="c-table__cell">Filename</span>
            <span class="c-table__cell">Finished times</span>
            <span class="c-table__cell">Times updated</span>
            <span class="c-table__cell"></span>
        </div>

        @foreach($pack->levels as $key => $level)
            <div class="c-table__row js-sortable-row" data-id="{{ $level->id }}">
                <span class="c-table__cell c-tooltip c-tooltip--bottom" aria-label="Eol id {{ $level->id }}"><a href="{{ route('manage.packs.levels.show', [$pack, $level]) }}" class="c-link">{{ str_limit($level->name, 16) }}</a></span>
                <span class="c-table__cell"><a href="{{ route('manage.packs.levels.show', [$pack, $level]) }}" class="c-link">{{ $level->filename }}</a></span>
                <span class="c-table__cell c-text--loud">{{ $level->finishedTimes }}</span>
                <span class="c-table__cell">{{ $level->times_updated_at->diffForHumans() }}</span>
                <span class="c-table__cell c-table__cell--actions">
                    {!! Form::open([
                        'route' => ['manage.packs.levels.detach', $pack, $level],
                        'method' => 'PUT',
                        'class' => 'js-pack-remove-level'
                    ]) !!}
                        <button class="text-button u-color-red-darker"><i class="fa fa-lg fa-trash-o"></i></button>
                    {!! Form::close() !!}
                    &nbsp;
                    <span class="cursor-move js-sortable-handle"><i class="fa fa-arrows"></i></span>
                </span>
            </div>
        @endforeach
    </div>
@endif

