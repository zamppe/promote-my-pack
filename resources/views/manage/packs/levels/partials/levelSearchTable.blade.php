<h3 class="c-heading">Search results</h3>
@if($levels->count())
    <ul class="c-list c-list--unstyled">
        @foreach($levels as $key => $level)
            <li>
                <span class="c-tooltip c-tooltip--right" aria-label="Eol id {{ $level->id }}">{{ $level->filename }},&nbsp;{{ $level->name }}</span>
                {!! Form::open([
                    'route' => ['manage.packs.levels.add', $pack],
                    'method' => 'POST',
                    'class' => 'js-pack-add-level form--inline'
                ]) !!}
                {!! Form::hidden('level_index', $level->id, []) !!}
                <button class="text-button text-button--green float-right"><i class="fa fa-lg fa-plus-square"></i>&nbsp;Add</button>
                {!! Form::close() !!}
            </li>
        @endforeach
    </ul>
@else
    <p class="c-text--quiet">No search results.</p>
@endif
