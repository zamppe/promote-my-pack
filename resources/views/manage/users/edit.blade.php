@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge">
        <h1 class="c-heading">Profile</h1>

        <div class="o-grid o-grid--no-gutter o-grid--wrap">
            <div class="o-grid__cell o-grid__cell--width-30@medium o-grid__cell--width-100">
                {!! Form::model($user, [
                    'route' => 'manage.users.update',
                    'method' => 'PUT'
                ]) !!}
                    @include('blaze.forms.text', ['input' => 'nickname', 'label' => 'Nick'])

                    @include('blaze.forms.submit', [
                        'text' => 'Update'
                    ])
                {!! Form::close() !!}
            </div>

            <div class="o-grid__cell o-grid__cell--offset-10@medium o-grid__cell--width-30@medium o-grid__cell--width-100">
                {!! Form::model($user, [
                    'route' => 'manage.users.update',
                    'method' => 'PUT'
                ]) !!}
                    @include('blaze.forms.password', ['input' => 'password', 'label' => 'New password'])

                    @include('blaze.forms.password', ['input' => 'password_confirmation', 'label' => 'Password confirmation'])

                    @include('blaze.forms.submit', [
                        'text' => 'Update'
                    ])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
