@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge">
        <h1 class="u-centered c-heading">Dashboard</h1>

        @if(user()->isAdmin())
            <h2 class="c-heading">
                All packs
            </h2>

            @if($packs->count())
                <div class="c-table c-table--striped">
                    <div class="c-table__caption"></div>
                    <div class="c-table__row c-table__row--heading">
                        <span class="c-table__cell">Pack</span>
                        <span class="c-table__cell">User</span>
                        <span class="c-table__cell">&nbsp;</span>
                    </div>

                    @foreach($packs as $pack)
                        <div class="c-table__row">
                            <span class="c-table__cell"><a href="{{ route('manage.packs.edit', $pack) }}" class="c-link c-text--loud">{{ $pack->name }}</a></span>
                            <span class="c-table__cell">{{ $pack->user->nickname }}</span>
                            <span class="c-table__cell">
                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['manage.packs.destroy', $pack],
                                ]) !!}
                                    {!! Form::submit('Destroy', ['class' => 'c-button c-button--error c-text--loud u-xsmall']) !!}
                                {!! Form::close() !!}
                            </span>
                        </div>
                    @endforeach
                </div>
            @else
                <p class="c-text--quiet">No packs created yet.</p>
            @endif
        @else
            <h2 class="c-heading">
                My packs
            </h2>

            @if($packs->count())
                <div class="c-table c-table--striped">
                    <div class="c-table__caption"></div>
                    <div class="c-table__row c-table__row--heading">
                        <span class="c-table__cell">Pack</span>
                        <span class="c-table__cell">&nbsp;</span>
                    </div>

                    @foreach($packs as $pack)
                        <div class="c-table__row">
                            <span class="c-table__cell"><a href="{{ route('manage.packs.edit', $pack) }}" class="c-link c-text--loud">{{ $pack->name }}</a></span>

                            <span class="c-table__cell">
                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['manage.packs.destroy', $pack],
                                ]) !!}
                                    {!! Form::submit('Destroy', ['class' => 'c-button c-button--error c-text--loud u-xsmall']) !!}
                                {!! Form::close() !!}
                            </span>
                        </div>
                    @endforeach
                </div>
            @else
                <p class="c-text--quiet">No packs created yet.</p>
            @endif
        @endif

        <h3 class="c-heading">New pack</h3>
            {!! Form::open([
                'route' => ['manage.packs.store'],
                'method' => 'POST'
            ]) !!}

            <div class="o-grid o-grid--no-gutter o-grid--small-full o-grid--wrap">
                <div class="o-grid__cell o-grid__cell--width-100">
                    @include('blaze.forms.text', ['input' => 'name', 'label' => 'Pack name'])
                </div>
            </div>

            <div class="o-grid o-grid--no-gutter o-grid--small-full o-grid--wrap">
                <div class="o-grid__cell o-grid__cell--width-15">
                    @include('blaze.forms.submit', [
                        'text' => 'Create'
                    ])
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
