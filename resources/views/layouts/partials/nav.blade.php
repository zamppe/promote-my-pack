<ul class="c-nav c-nav--fixed c-nav--inline">
    <li class="c-nav__item">
        <a href="{{ url('/') }}">
            <i class="fa fa-home"></i>
            <span class="hide-on-small">
                {{ config('app.name', 'Laravel') }}
            </span>
        </a>
    </li>

    <li class="c-nav__item">
        <a href="{{ route('statistics.show') }}">
            Stats
        </a>
    </li>

    @if (Auth::guest())
        <li class="c-nav__item c-nav__item--right">
            <a href="{{ url('/login') }}">
                <i class="fa fa-sign-in u-color-green"></i>
                Login
            </a>
        </li>

        <li class="c-nav__item c-nav__item--right">
            <a href="{{ url('/register') }}">
                Register
            </a>
        </li>
    @else
        <li class="c-nav__item c-nav__item--right">
            <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout <span class="hide-on-small">{{ Auth::user()->email }}</span>
            </a>
        </li>

        {!! Form::open([
            'url' => '/logout',
            'method' => 'POST',
            'style' => 'display: none;',
            'id' => 'logout-form'
        ]) !!}
        {!! Form::close() !!}
    @endif

    <li class="c-nav__item c-nav__item--right">
        <a href="{{ route('manage.dashboard') }}" class="nav__item">
            Manage <span class="hide-on-small">levelpacks</span>
        </a>
    </li>

    @if (user() && user()->isModerator())
        <li class="c-nav__item c-nav__item--right">
            <a href="{{ route('moderator.dashboard') }}" class="nav__item">
                Moderator
            </a>
        </li>
    @endif

    @if (user() && user()->isAdmin())
        <li class="c-nav__item c-nav__item--right">
            <a href="{{ route('admin.dashboard') }}" class="nav__item">
                Admin
            </a>
        </li>
    @endif
</ul>
