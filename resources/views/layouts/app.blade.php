<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Packs') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="/css/lightgallery.min.css" rel="stylesheet">
</head>
<body class="c-text">
    @include('layouts.partials.nav')

    @yield('content')

    @section('scripts')
    <script src="/js/app.js"></script>
    <script src="/js/tinymce/tinymce.min.js"></script>
    @show

    <div class="c-nav c-nav--inline c-nav--bottom c-nav--fixed u-bg-black">
        {{-- <p class="c-paragraph u-centered"> Powered by gofe inc.</p> --}}
        <p class="c-paragraph u-centered">{{ config('app.name') }}<span class="hide-on-small">, Elasto Mania level packs and statistics. </span></p>
    </div>

</body>
</html>
