@if($errors->has($input))
    <div class="c-hint c-hint--static c-hint--error"> {{ $errors->first($input)}} </div>
@endif
