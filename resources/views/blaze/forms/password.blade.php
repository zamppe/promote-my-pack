@php
    if (!isset($attributes)) {
        $attributes = [];
    }

    $class = "c-field c-field--label";
    if ($errors->has($input)) {
        $class .= " c-field--error";
    }

    $attributes = array_merge(['class' => $class], $attributes);
@endphp

<label class="c-label o-form-element">
    {{ $label }}

    {!! Form::password($input, $attributes) !!}

    @include('blaze.forms.hint')
    @include('blaze.forms.error')
</label>
