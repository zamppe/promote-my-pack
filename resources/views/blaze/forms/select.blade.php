@php
    $optionsArray = $optionsArray ?? [0 => 'no data'];
    $optionsArray = $optionsArray instanceof \Illuminate\Support\Collection ? $optionsArray->toArray() : $optionsArray;
    $defaultKey = $defaultKey ?? array_keys($optionsArray)[0];
    $classes = $classes ?? collect();

    if (is_array($classes)) {
        $classes = collect($classes);
    }

    if (is_string($classes)) {
        $classes = collect([$classes]);
    }

    $classes = $classes->merge([
        'c-field',
        'c-field--label',
    ]);

    if ($errors->has($input)) {
        $classes->push("c-field--error");
    }
@endphp

<label class="c-label o-form-element">
    {{ $label }}

    {!! Form::select($input, $optionsArray, $defaultKey, ['class' => $classes->implode(' ')]) !!}

    @include('blaze.forms.hint')
    @include('blaze.forms.error')
</label>
