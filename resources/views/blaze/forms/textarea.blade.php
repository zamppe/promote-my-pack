@php
    $tinymce = isset($tinymce) ? $tinymce : false;

    $class = "c-field c-field--label";
    if ($errors->has($input)) {
        $class .= " c-field--error";
    }
    if ($tinymce) {
        $class .= " js-tinymce";
    }
@endphp

<label class="c-label o-form-element">
    {{ $label }}

    {!! Form::textarea($input, null, ['class' => $class]) !!}

    @include('blaze.forms.hint')
    @include('blaze.forms.error')
</label>
