@php
    $attributes = $attributes ?? [];
    $classes = $classes ?? [];
    $value = $value ?? null;
    $label = $label ?? 'No label defined!';

    if (is_array($attributes)) {
        $attributes = collect($attributes);
    }

    if (is_array($classes)) {
        $classes = collect($classes);
    }

    $classes = $classes->merge([
        'c-field',
        'c-field--label',
    ]);
    if ($errors->has($input)) {
        $classes->push("c-field--error");
    }

    $attributes = $attributes->merge(['class' => $classes->implode(' ')]);

    $attributes = $attributes->toArray();
@endphp

<label class="c-label o-form-element">
    {{ $label }}

    {!! Form::text($input, $value, $attributes) !!}

    @include('blaze.forms.hint')
    @include('blaze.forms.error')
    @include('blaze.forms.js-error')
</label>
