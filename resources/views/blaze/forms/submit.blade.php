@php
    if (!isset($text)) {
        $text = 'Button';
    }
@endphp

{!! Form::submit($text, ['class' => 'c-button c-button--success u-small c-text--loud']) !!}
