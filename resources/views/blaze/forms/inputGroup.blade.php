@php
    if (! isset($value)) {
        $value = null;
    }

    $class = "c-field";

    if ($errors->has($input)) {
        $class .= " c-field--error";
    }

    if (isset($extraClasses)) {
        $class .= " " . $extraClasses;
    }
@endphp

<div class="o-form-element">
    <label for="{{ $input }}" class="c-label"> {{ $label }} </label>

    <div class="c-input-group">
        <div class="o-field">
            {!! Form::text($input, $value, ['class' => $class, 'id' => $input]) !!}
        </div>
        <button class="c-button c-button--brand">{{ $button }}</button>
    </div>

    @include('blaze.forms.hint')
    @include('blaze.forms.error')
</div>




