<table class="c-table c-table--condensed">
    <caption class="c-table__caption">Small table and text</caption>
    <thead class="c-table__head">
        <tr class="c-table__row c-table__row--heading">
         {{-- {{dd($data)}} --}}
            @foreach($headers as $header)
                <th class="c-table__cell">{{ $header }}</th>
            @endforeach
        </tr>
    </thead>
    <tbody class="c-table__body">
        @foreach($data as $row)
            <tr class="c-table__row">
                @foreach($row as $key => $value)
                    <td class="c-table__cell">{{ $value }}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>
