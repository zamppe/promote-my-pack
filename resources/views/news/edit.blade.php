@extends('layouts.app')

@section('content')
    @parent
    <div class="o-container o-container--xlarge u-letter-box--super">

        {!! Form::model($newsItem, [
            'route' => ['news.update', $newsItem],
            'method' => 'PUT'
        ]) !!}
            <h1 class="c-heading">Edit news</h1>

            @include('news.partials.editForm')

            @include('blaze.forms.submit', [
                'text' => 'Update'
            ])
        {!! Form::close() !!}

        <div class="u-letter-box--medium">
            <a href="{{ route('frontpage') }}" class="c-link">Return back</a>
        </div>
    </div>
@endsection
@section('scripts')
    @parent
    <script>

    </script>
@endsection
