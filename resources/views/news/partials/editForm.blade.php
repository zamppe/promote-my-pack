<div class="o-grid o-grid--no-gutter o-grid--small-full">
    <div class="o-grid__cell o-grid__cell--width-100">
        @include('blaze.forms.text', ['input' => 'title', 'label' => 'Title'])
    </div>
</div>
<div class="o-grid o-grid--no-gutter o-grid--small-full">
    <div class="o-grid__cell o-grid__cell--width-100">
        @include('blaze.forms.textarea', ['input' => 'content', 'label' => 'Content', 'tinymce' => true])
    </div>
</div>
