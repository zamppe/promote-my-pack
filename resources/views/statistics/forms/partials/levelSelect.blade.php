<div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--width-100">
    @include('blaze.forms.select', [
        'input' => 'level',
        'optionsArray' => $levels,
        'label' => 'Level',
        'classes' => 'js-statistics-select',
    ])
</div>
