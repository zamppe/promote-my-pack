<div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--width-100">
    @include('blaze.forms.text', [
        'input' => 'after',
        'label' => 'After',
        'classes' => ['js-flatpickr']
    ])
</div>

<div class="o-grid__cell o-grid__cell--width-33@medium o-grid__cell--width-100">
    @include('blaze.forms.text', [
        'input' => 'before',
        'label' => 'Before',
        'classes' => ['js-flatpickr']
    ])
</div>
