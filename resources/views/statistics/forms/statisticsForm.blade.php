{!! Form::open([
    'route' => ['statistics.generate'],
    'method' => 'GET',
    'class' => 'js-statistics-form'
]) !!}
    @section('statistics_form_fields')
        <div class="o-grid o-grid--no-edge-gutter o-grid--wrap">
            {!! $fields !!}
        </div>
    @show
{!! Form::close() !!}
