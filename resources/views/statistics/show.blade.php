@extends('layouts.app')

@section('content')
    <div class="o-container o-container--super">
        <div class="u-pillar-box--medium">
            <h2 class="c-heading">Statistics</h2>

            <div class="js-statistics-form-container"
                data-category={{ request()->has('category') ? request()->input('category') : 0 }}
                data-rerender-url={{ route('statistics.show') }}
            >
                {!! $form !!}
            </div>

            <div class="js-statistics-tables-container"  @if(! empty($tables)) data-json="{{ $tables }}" @endif>

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script id="js-footable-template" type="text/template">
        <table class="table js-footable"></table>
    </script>

    <script id="js-footable-title-template" type="text/template">
        <h2 class="c-heading js-footable-title"></h2>
    </script>

    <script>
        var renderForm = function(url) {
            var url = $('.js-statistics-form-container').attr('data-rerender-url');
            var $form = $('.js-statistics-form');

            $.get({
                url: url,
                data: $form.serialize(),
                dataType: 'json'
            }).then(function(response) {
                $('.js-statistics-form-container').html(response['view']);

                initFlatpickr();
                // $('.js-statistics-form select').last().select2({
                    // adaptContainerCssClass : "c-field c-field--label",
                    // adaptDropdownCssClass : "c-field c-field--label",
                    // dropdownCss : "c-field c-field--label",
                    // dropdownCssClass : "c-field c-field--label",
                    // containerCss : "c-field c-field--label",
                    // containerCssClass : "c-field c-field--label"
                // });

                url = window.location.href.split('?')[0] + '?' + $form.serialize();
                window.history.pushState(null, '', url);
            });
        }

        var renderTables = function(tables) {
            var $tablesContainer = $('.js-statistics-tables-container');
            tables.forEach(function(table, key) {
                var $tableTemplate = $($.parseHTML($('#js-footable-template').html()));
                var $tableTitleTemplate = $($.parseHTML($('#js-footable-title-template').html()));
                var id = 'js-footable' + key;
                $tableTemplate.attr('id', id);
                $tableTitleTemplate.html(table.title);
                $tablesContainer.append($tableTitleTemplate);
                $tablesContainer.append($tableTemplate);
                var ft = FooTable.init('#' + id, table);
            });
        }

        $(function() {
            // $('.js-statistics-category').trigger('change');
            var $tablesContainer = $('.js-statistics-tables-container');
            if ($tablesContainer.attr('data-json')) {
                var tables = JSON.parse($tablesContainer.attr('data-json'));

                renderTables(tables);
            }
        });

        $(document).on('change', '.js-statistics-select', function(e) {
            renderForm();
        });

        $(document).on('submit', '.js-statistics-form', function(e) {
            e.preventDefault();

            $('.js-statistics-tables-container').html('');

            var $form = $(this);
            $form.find('input[type="submit"]').prop('disabled', true);

            $tableRenderAjax = $.ajax({
                method: 'get',
                data: $form.serialize(),
                url: $form.attr('action'),
                dataType: 'json'
            });

            $tableRenderAjax.then(function(response) {
                $('.js-error').each(function() {
                    $(this).html('');
                });

                renderTables(response);
                $form.find('input[type="submit"]').prop('disabled', false);
            });

            $tableRenderAjax.fail(function(response) {
                var errors = response.responseJSON;
                Object.keys(errors).forEach(function (key) {
                    var error = errors[key];

                    $('.js-error').each(function() {
                        $el = $(this);
                        if ($el.attr('data-input') === key) {
                            $el.html(error);
                        }
                    });
                });
                $form.find('input[type="submit"]').prop('disabled', false);
            });

            var url = window.location.href.split('?')[0] + '?' + $form.serialize();
            window.history.pushState(null, '', url);

        });
    </script>
@endsection
