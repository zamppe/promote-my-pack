<div class="o-grid o-grid--wrap o-grid--no-gutter">
    <div class="o-grid__cell o-grid__cell--width-30@medium o-grid__cell--width-100">
        <h2 class="c-heading">Tags</h2>
        <ul class="c-list c-list--unstyled">
            @foreach($tags as $tag)
                <li class="c-list__item">
                    <span class="c-badge c-badge--success">{{ $tag->name }}</span>
                    {!! Form::open([
                        'route' => ['moderator.tags.destroy', $tag],
                        'method' => 'DELETE',
                        'class' => 'form--inline'
                    ]) !!}
                        <button class="text-button u-color-red-darker" type="submit"><i class="fa fa-lg fa-trash-o"></i></button>
                    {!! Form::close() !!}
                </li>
            @endforeach
            <p class="c-text--quiet">Note: deleting a tag is unrecoverable</p>
        </ul>
    </div>
    <div class="o-grid__cell o-grid__cell--width-50@medium o-grid__cell--width-100">
        <h2 class="c-heading">Create a new tag</h2>
        {!! Form::open([
            'route' => ['moderator.tags.store'],
            'method' => 'POST',
        ]) !!}
        <div class="o-grid o-grid--no-gutter o-grid--wrap">
            <div class="o-grid__cell o-grid__cell--width-70@medium o-grid__cell--width-50">
                @include('blaze.forms.inputGroup', [
                    'input' => 'name',
                    'label' => 'Name',
                    'button' => 'Save',
                ])
            </div>
        </div>
    </div>
</div>
