<h2 class="c-heading">Globally banned times</h2>
@if($timeBans->count())
    <div class="c-table c-table--striped">
        <div class="c-table__caption"></div>
        <div class="c-table__row c-table__row--heading">
            <span class="c-table__cell">Level</span>
            <span class="c-table__cell">Kuski</span>
            <span class="c-table__cell">Time</span>
            <span class="c-table__cell"></span>
        </div>
        @foreach($timeBans as $timeBan)
            <div class="c-table__row">
                <span class="c-table__cell"> {{ $timeBan->level->name }} </span>
                <span class="c-table__cell"> {{ $timeBan->kuski->name }} </span>
                <span class="c-table__cell"> {{ $timeBan->time->time }} </span>
                <span class="c-table__cell">
                    {!! Form::open([
                        'method' => 'DELETE',
                        'route' => ['levels.times.unban', $timeBan->level, $timeBan->time],
                    ]) !!}
                    @include('blaze.forms.submit', [
                        'text' => 'Unban'
                    ])
                    {!! Form::close() !!}
                </span>
            </div>
        @endforeach
    </div>
@else
    <p class="c-text--quiet">No globally banned times yet.</p>
@endif
<p class="c-text--quiet">Global bans apply to all packs.</p>
