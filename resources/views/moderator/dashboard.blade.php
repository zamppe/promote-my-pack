@extends('layouts.app')

@section('content')

    <div class="o-container o-container--xlarge">
        <h1 class="u-centered c-heading">Moderator panel</h1>

        @include('moderator.partials.globalTimeBans')

        @include('moderator.partials.tags')

    </div>
@endsection
