@extends('layouts.app')

@section('content')
    <div class="o-container o-container--xlarge">
        <h3>Times for {{ $level->name }} (EOL id: {{ $level->id }})</h3>
        <a href="{{ $times->previousPageUrl() }}">&lt;</a>
        <a href="{{ $times->nextPageUrl() }}">&gt;</a>
        @if($missingKuskiCount)
            <p>Missing data for {{ $missingKuskiCount }} kuskis out of {{ $kuskiCount }}</p>
        @endif
        <div class="table">
            <div class="table__row table__row--head">
                <div class="table__cell">
                    Kuski
                </div>
                <div class="table__cell">
                    Team
                </div>
                <div class="table__cell">
                    Nationality
                </div>
                <div class="table__cell">
                    Time
                </div>
                <div class="table__cell">
                    Date
                </div>
                <div class="table__cell">
                    Eol index
                </div>
            </div>
            @foreach($times as $time)
                <div class="table__row table__row--body">
                    <div class="table__cell">
                        {{ ++$loop->index + (($times->currentPage() - 1) * $times->perPage()) }}. <a href="{{routes('kuskis.show', $time->kuski)}}">{{ isset($time->kuski) ? $time->kuski->name : '?'}}</a>
                    </div>
                    <div class="table__cell">
                        <span class="flag-icon flag-icon-{{ isset($time->kuski) ? $time->kuski->nationality : '?'}}"></span>
                    </div>
                    <div class="table__cell">
                        {{ isset($time->kuski) ? $time->kuski->team : '?' }}
                    </div>
                    <div class="table__cell">
                        {{ $time->time }}
                    </div>
                    <div class="table__cell">
                        {{ $time->driven_at }}
                    </div>
                    <div class="table__cell">
                        {{ $time->id }}
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
