@php
    if (!isset($w)) {
        $w = 140;
    }

    if (!isset($h)) {
        $h = 80;
    }
@endphp

@if($level->getMedia('image')->first())
    <img class="o-image level-image--large"
    src="{{ GlideImage::load($level->getMedia('image')->first()->getDiskPath(), ['q' => 60, 'w' => $w, 'h' => $h, 'fit' => 'crop']) }}"
    alt="{{ $level->filename }}" />
@else
    @php
        $src = "http://via.placeholder.com/{$w}x{$h}"
    @endphp
    <img class="o-image" src={{ $src }} alt="">
@endif
