<div class="o-grid o-grid--wrap">
    <div class="o-grid__cell o-grid__cell--width-100 o-grid__cell--width-100@medium">
        @include('blaze.forms.text', [
            'input' => 'kuski',
            'label' => 'Kuski',
            'value' => $input->get('kuski')
        ])
    </div>

    <div class="o-grid__cell o-grid__cell--width-100 o-grid__cell--width-50@medium">
        @include('blaze.forms.select', [
            'input' => 'nationality',
            'label' => 'Nationality',
            'optionsArray' => $nationalities,
            'defaultKey' => $input->get('nationality')
        ])
    </div>

    <div class="o-grid__cell o-grid__cell--width-100 o-grid__cell--width-50@medium">
        @include('blaze.forms.select', [
            'input' => 'team',
            'label' => 'Team',
            'optionsArray' => $teams,
            'defaultKey' => $input->get('team')
        ])
    </div>

    <div class="o-grid__cell o-grid__cell--width-100 o-grid__cell--width-50@medium">
        @include('blaze.forms.text', [
            'input' => 'after',
            'label' => 'After',
            'attributes' => ['placeholder' => '2000'],
            'classes' => ['js-flatpickr']
        ])
    </div>

    <div class="o-grid__cell o-grid__cell--width-100 o-grid__cell--width-50@medium">
        @include('blaze.forms.text', [
            'input' => 'before',
            'label' => 'Before',
            'attributes' => ['placeholder' => \Carbon\Carbon::now()->addYear()->year],
            'classes' => ['js-flatpickr']
        ])
    </div>

    <div class="o-grid__cell o-grid__cell--width-50">
        <label class="c-field c-field--choice">
            {!! Form::checkbox('best_only', 1, $input->get('best_only'), []) !!}
            Best only
        </label>
    </div>

    <div class="o-grid__cell o-grid__cell--width-50">
        <label class="c-field c-field--choice">
            {!! Form::checkbox('include_banned', 1, $input->get('include_banned'), []) !!}
            Include banned
        </label>
    </div>
</div>

