var initSortable = function() {
    var el = document.querySelector('.js-sortable');

    if (el) {
        var sortable = Sortable.create(el, {
            handle: '.js-sortable-handle',
            scroll: true,
            animation: 150,
            ghostClass: 'sortable-ghost-class',
            draggable: '.js-sortable-row',
            onUpdate: function (evt) {
                var ids = $.map($(evt.to).children(), function(child) {
                    return $(child).attr('data-id');
                });

                $.ajax({
                    method: 'PUT',
                    url: $('.js-sortable').attr('data-url'),
                    data: {array: ids},
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }
        });
    }
}

var initFlatpickr = function() {
    $(".js-flatpickr").flatpickr({
        dateFormat: "Y-m-d",
        allowInput: true
    });
}

$(function() {
    tinymce.init({
        selector:'.js-tinymce',
        plugins: "code",
        toolbar: "code",
    });
    $(".lightgallery").lightGallery();

    initSortable();
    initFlatpickr();

    // $('.js-statistics-form select').last().select2({
        // adaptContainerCssClass : "c-field c-field--label"
        // adaptDropdownCssClass : "c-field c-field--label"
        // dropdownCss : "padding: 10rem",
        // dropdownCssClass : "c-field "
        // containerCssClass : "c-field c-field--label"
    // });
});

$(document).on('submit', '.js-edit-custom-achievement-kuski-search', function(e) {
    e.preventDefault();
    $form = $(this);

    $.ajax({
        method: 'GET',
        url: $form.attr('action'),
        data: $form.serialize(),
        dataType: 'json',
        success: function(response) {
            $('.js-edit-custom-achievement-kuski-results').html(response['kuskis']);
        }
    });
});

$(document).on('submit', '.js-custom-achievement-grant-to-kuski', function(e) {
    e.preventDefault();
    $form = $(this);

    $.ajax({
        method: 'POST',
        url: $form.attr('action'),
        data: $form.serialize() + '&kuski=' + $('.js-kuski').val(),
        dataType: 'json',
        success: function(response) {
            $('.js-edit-custom-achievement-kuski-results').html(response['kuskis']);
        }
    });
});

$(document).on('submit', '.js-custom-achievement-remove-from-kuski', function(e) {
    e.preventDefault();
    $form = $(this);

    $.ajax({
        method: 'DELETE',
        url: $form.attr('action'),
        data: $form.serialize() + '&kuski=' + $('.js-kuski').val(),
        dataType: 'json',
        success: function(response) {
            $('.js-edit-custom-achievement-kuski-results').html(response['kuskis']);
        }
    });
});

$(document).on('submit', '.js-level-search', function(e) {
    e.preventDefault();
    $form = $(this);

    $.ajax({
        method: 'GET',
        url: $form.attr('action'),
        data: $form.serialize(),
        dataType: 'json',
        success: function(response) {
            $('.js-level-search-results').html(response);
        }
    });
});

$(document).on('submit', '.js-pack-add-level, .js-pack-remove-level', function(e) {
    e.preventDefault();
    $form = $(this);

    $.ajax({
        method: 'POST',
        url: $form.attr('action'),
        data: $form.serialize(),
        dataType: 'json',
        success: function(response) {
            $('.js-pack-levels').html(response['levels_table']);
            $('.js-pack-edit-tab-bans').html(response['bans_tab']);
            $('.js-pack-edit-tab-achievements').html(response['achievements_tab']);
            initSortable();
        }
    });
});

$(document).on('submit', '.js-pack-sort', function(e) {
    e.preventDefault();
    $form = $(this);

    $.ajax({
        method: 'POST',
        url: $form.attr('action'),
        data: $form.serialize(),
        dataType: 'json',
        success: function(response) {
            $('.js-pack-levels').html(response['levels_table']);
            initSortable();
        }
    });
});

$(document).on('click', '.js-packs-show-level-row img, .js-packs-show-level-row a', function(e) {
    e.stopPropagation();
});

$(document).on('click', '.js-packs-expand-level-row', function() {
    var $self = $(this);
    var data = $self.hasClass('js-open') ? {n: 3} : {n: 20};
    $self.toggleClass('js-open');
    var url = $self.attr('data-url');
    $.ajax({
        method: 'GET',
        url: url,
        data: data,
        dataType: 'json',
        success: function(response) {
            console.log(response['top_lists']);
            $self.closest('.js-picture-row').find('.js-packs-level-row-top-lists').html(response['top_lists']);
        }
    });
});

$(document).on('click', '.js-checkbox-select-all', function() {
    var state = $(this).prop('checked');
    $('.js-checkbox').prop('checked', state);
});

