<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalTimeImprovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_time_improvements', function (Blueprint $table) {
            $table->integer('old_time_id')->unsigned()->nullable();
            $table->integer('time_id')->unsigned();
            $table->integer('pack_id')->unsigned();
            $table->integer('total_time')->unsigned();
            $table->foreign('old_time_id')->references('id')->on('times')
                ->onDelete('cascade');
            $table->foreign('time_id')->references('id')->on('times')
                ->onDelete('cascade');
            $table->foreign('pack_id')->references('id')->on('packs')
                ->onDelete('cascade');
            $table->index('old_time_id');
            $table->index('time_id');
            $table->index('pack_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_time_improvements');
    }
}
