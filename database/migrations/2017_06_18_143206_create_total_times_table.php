<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('total_times', function (Blueprint $table) {
            $table->integer('kuski_id')->unsigned();
            $table->integer('pack_id')->unsigned();
            $table->integer('total_time')->unsigned();
            $table->foreign('kuski_id')->references('id')->on('kuskis')
                ->onDelete('cascade');
            $table->foreign('pack_id')->references('id')->on('packs')
                ->onDelete('cascade');
            $table->index('kuski_id');
            $table->index('pack_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('total_times');
    }
}
