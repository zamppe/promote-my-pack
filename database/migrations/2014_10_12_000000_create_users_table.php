<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $config = config('roles.table_names');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nickname', 16);
            $table->string('email', 64)->unique();
            $table->string('password', 64);
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create($config['roles'], function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 16);
            $table->timestamps();
        });

        Schema::create($config['user_has_roles'], function (Blueprint $table) use ($config) {
            $table->integer('role_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('role_id')
                ->references('id')
                ->on($config['roles'])
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on($config['users'])
                ->onDelete('cascade');

            $table->primary(['role_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $config = config('roles.table_names');
        Schema::dropIfExists($config['user_has_roles']);
        Schema::dropIfExists($config['roles']);
        Schema::dropIfExists('users');
    }
}
