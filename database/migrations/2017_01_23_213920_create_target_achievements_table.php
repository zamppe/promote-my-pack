<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTargetAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_achievements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pack_id')->unsigned();
            $table->text('description');
            $table->text('levels');
            $table->tinyInteger('apply_on')->unsigned();
            $table->tinyInteger('comparator')->unsigned();
            $table->tinyInteger('consider')->unsigned();
            $table->tinyInteger('target_type')->unsigned();
            $table->tinyInteger('target_value_type')->unsigned();
            $table->mediumInteger('target_value')->unsigned();
            $table->string('name');
            $table->timestamps();

            $table->foreign('pack_id')->references('id')->on('packs')
                ->onDelete('cascade');

            $table->index('pack_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_achievements');
    }
}
