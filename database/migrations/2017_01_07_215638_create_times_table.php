<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('times', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('time')->unsigned();
            $table->string('team', 32)->nullable();
            $table->integer('kuski_id')->unsigned()->nullable();
            $table->integer('level_id')->unsigned()->nullable();
            $table->dateTime('driven_at')->nullable();

            $table->foreign('kuski_id')->references('id')->on('kuskis')
                ->onDelete('cascade');

            $table->foreign('level_id')->references('id')->on('levels')
                ->onDelete('cascade');

            $table->index('kuski_id');
            $table->index('level_id');
            $table->index(['level_id', 'kuski_id']);
            $table->index('driven_at');
            $table->index(['level_id', 'driven_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('times');
    }
}
