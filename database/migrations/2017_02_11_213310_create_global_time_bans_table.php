<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGlobalTimeBansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_time_bans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('time_id')->unsigned();
            $table->integer('kuski_id')->unsigned();
            $table->integer('level_id')->unsigned();
            $table->timestamps();

            $table->foreign('time_id')->references('id')->on('times')
                ->onDelete('cascade');
            $table->foreign('kuski_id')->references('id')->on('kuskis')
                ->onDelete('cascade');
            $table->foreign('level_id')->references('id')->on('levels')
                ->onDelete('cascade');

            $table->index('time_id');
            $table->index('kuski_id');
            $table->index('level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_time_bans');
    }
}
