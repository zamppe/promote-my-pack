<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalKuskiBansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_kuski_bans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pack_id')->unsigned();
            $table->integer('kuski_id')->unsigned();
            $table->timestamps();

            $table->foreign('pack_id')->references('id')->on('packs')
                ->onDelete('cascade');
            $table->foreign('kuski_id')->references('id')->on('kuskis')
                ->onDelete('cascade');

            $table->index(['kuski_id', 'pack_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_kuski_bans');
    }
}
