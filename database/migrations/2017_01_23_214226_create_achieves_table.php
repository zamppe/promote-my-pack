<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achieves', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kuski_id')->unsigned()->nullable();
            $table->integer('time_id')->unsigned()->nullable();
            $table->mediumInteger('achievable_id')->unsigned();
            $table->string('achievable_type');
            $table->dateTime('achieved_at')->nullable();
            $table->timestamps();

            $table->foreign('kuski_id')->references('id')->on('kuskis')
                ->onDelete('cascade');
            $table->foreign('time_id')->references('id')->on('times')
                ->onDelete('cascade');

            $table->index('kuski_id');
            $table->index('time_id');
            $table->index(['achievable_id', 'achievable_type']);
            $table->index('achieved_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achieves');
    }
}
