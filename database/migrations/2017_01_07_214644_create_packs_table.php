<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32);
            $table->integer('user_id')->unsigned();
            $table->unsignedTinyInteger('update_frequency')->default(1);
            $table->dateTime('deadline')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->index('user_id');
            $table->index('deadline');
        });

        Schema::create('levels', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32);
            $table->string('filename', 32);
            $table->unsignedSmallInteger('killers')->nullable();
            $table->unsignedSmallInteger('apples')->nullable();
            $table->unsignedSmallInteger('flowers')->nullable();
            $table->dateTime('times_updated_at')->nullable();
            $table->timestamps();
        });

        Schema::create('level_pack', function (Blueprint $table) {
            $table->integer('level_id')->unsigned();
            $table->integer('pack_id')->unsigned();
            $table->smallInteger('position')->unsigned()->nullable()->default(1);

            $table->foreign('level_id')
                ->references('id')
                ->on('levels')
                ->onDelete('cascade');

            $table->foreign('pack_id')
                ->references('id')
                ->on('packs')
                ->onDelete('cascade');

            $table->primary(['level_id', 'pack_id']);
            $table->index('position');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('level_pack');
        Schema::dropIfExists('levels');
        Schema::dropIfExists('packs');
    }
}
