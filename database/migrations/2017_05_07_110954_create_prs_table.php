<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prs', function (Blueprint $table) {
            $table->integer('time_id')->unsigned();
            $table->integer('pack_id')->unsigned();
            $table->foreign('time_id')->references('id')->on('times')
                ->onDelete('cascade');
            $table->foreign('pack_id')->references('id')->on('packs')
                ->onDelete('cascade');
            $table->index('time_id');
            $table->index('pack_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prs');
    }
}
