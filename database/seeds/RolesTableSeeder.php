<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        foreach(config('roles.roles') as $role)
        {
            Role::create(['name' => $role]);
        }
    }
}
