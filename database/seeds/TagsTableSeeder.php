<?php

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        // todo config file?
        $tags = collect([
            'Pipe',
            'Apple harvest',
            'Uphill',
            'Rollercoaster',
            'Linear',
            'Multiroute',
            'Speedloops',
            'Multiflower',
            'Rocket start',
            'Gravity',
        ]);

        $tags = $tags->sort();

        foreach($tags as $tag) {
            Tag::create([
                'name' => $tag,
            ]);
        }
    }
}
