<?php

use App\Models\Pack;
use App\Models\TargetAchievement;
use App\Services\PackService;
use Illuminate\Database\Seeder;

class PackTestSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        $levels = [
            199,
            200,
            201,
            202,
        ];

        $pack = Pack::create([
            'name' => 'Testpack',
            'user_id' => 1,
            'deadline' => \Carbon\Carbon::now(),
        ]);

        foreach($levels as $levelId) {
            PackService::addLevelByEolId($pack, $levelId);
        }

        $pack->targetAchievements()->create([
            'name' => 'Master of VSK',
            'description' => 'Finish every level of the pack',
            'apply_on' => TargetAchievement::APPLY_ON_ALL_LEVELS,
            'levels' => $levels,
            'consider' => TargetAchievement::CONSIDER_BEST,
            'target_type' => TargetAchievement::TARGET_TYPE_TIME,
            'target_value_type' => TargetAchievement::TARGET_VALUE_TYPE_TIME,
            'comparator' => TargetAchievement::COMPARATOR_EQ,
            'target_value' => TargetAchievement::TARGET_VALUE_ANY_FINISH
        ]);

        $pack->targetAchievements()->create([
            'name' => 'so good vov..',
            'description' => 'Get 2 world records',
            'apply_on' => 2, // there must be 2 matches ...
            'levels' => $levels, // ... for these levels
            'consider' => TargetAchievement::CONSIDER_BEST, // taking only best time per kuski into account, not all times
            'target_type' => TargetAchievement::TARGET_TYPE_TIME, // comparing to times, not total times
            'target_value_type' => TargetAchievement::TARGET_VALUE_TYPE_POSITION, // target value is a position value, not time value
            'comparator' => TargetAchievement::COMPARATOR_EQ, // get exactly...
            'target_value' => 1,  //... position 1 (WR)
        ]);
    }

}
