<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        $zamppe = User::create([
            'nickname' => 'zamppe',
            'email' => 'zamppe@gmail.com',
            'password' => bcrypt('qwerty'),
        ]);

        $zamppe->assignRole(config('roles.roles.default'));
        $zamppe->assignRole(config('roles.roles.moderator'));
        $zamppe->assignRole(config('roles.roles.admin'));
    }
}
