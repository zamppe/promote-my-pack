<?php

use App\Services\LevelService;
use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{
    /**
     *
     */
    public function run()
    {
        LevelService::getAllLevels();
    }
}
