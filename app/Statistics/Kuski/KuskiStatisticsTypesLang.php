<?php

namespace App\Statistics\Kuski;

use App\Utils\TypesBase;
use App\Utils\GetsClassConstants;

/**
 * Class KuskiStatisticsTypesLang
 * @package App\Statistics
 */
class KuskiStatisticsTypesLang extends TypesBase
{
    use GetsClassConstants;

    const TOP_10_LANG = "Top 10s";
    const TOP_100_LANG = "Top 100s";
    const PRS_LANG = "PRs";
}
