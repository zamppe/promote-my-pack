<?php

namespace App\Statistics\Kuski;

use App\Utils\TypesBase;
use App\Utils\GetsClassConstants;

/**
 * Class KuskiStatisticsTypes
 * @package App\Statistics
 */
class KuskiStatisticsTypes extends TypesBase
{
    use GetsClassConstants;

    const TOP_10 = 1;
    const TOP_100 = 2;
    const PRS = 3;

    public static function getAllWithLang()
    {
        $constants = self::getAll();
        $constantsLang = KuskiStatisticsTypesLang::getAll();
        $constantsWithLang = collect();

        return $constants->combine($constantsLang);
    }
}
