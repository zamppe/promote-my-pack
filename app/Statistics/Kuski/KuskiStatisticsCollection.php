<?php

namespace App\Statistics\Kuski;

use App\Models\Kuski;
use App\Models\Level;
use App\Models\Pack;
use App\Utils\DateRange;
use App\Utils\CarbonHelper;
use App\Footable\FootablesCollection;
use App\Footable\DefaultTable;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class KuskiStatisticsCollection
 * @package App\Statistics
 */
class KuskiStatisticsCollection extends FootablesCollection
{
    protected $pack;
    protected $level;
    protected $kuski;

    /**
     * Constructor
     *
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $required = [
            KuskiStatisticsTypes::TOP_10 => [
                'kuski',
                'pack',
                'level',
                'type',
            ],
            KuskiStatisticsTypes::TOP_100 => [
                'kuski',
                'pack',
                'level',
                'type',
            ],
            KuskiStatisticsTypes::PRS => [
                'kuski',
                'pack',
                'type',
            ]
        ];

        parent::__construct($request, $required);

        if ($this->hasRequired() && ($this->request->input('type') == KuskiStatisticsTypes::TOP_10 || $this->request->input('type') == KuskiStatisticsTypes::TOP_100)) {
            $this->pack = Pack::find($request->input('pack'));
            $this->level = Level::find($request->input('level'));
            $this->kuski = Kuski::find($request->input('kuski'));
        }

        if ($this->hasRequired() && ($this->request->input('type') == KuskiStatisticsTypes::PRS)) {
            $this->pack = Pack::find($request->input('pack'));
            $this->kuski = Kuski::find($request->input('kuski'));
        }

        if ($this->hasRequired() && $this->pack && $this->kuski) {
            $this->generateTables();
        }
    }

    /**
     * Generate tables
     *
     * @return KuskiStatisticsCollection
     */
    private function generateTables()
    {
        $type = $this->request->input('type');

        switch ($type) {
            case KuskiStatisticsTypes::TOP_10:
                $this->generateTopTables(10); break;
            case KuskiStatisticsTypes::TOP_100:
                $this->generateTopTables(100); break;
            case KuskiStatisticsTypes::PRS:
                $this->generatePRTable(); break;
        }

        return $this;
    }

    /**
     * Generate top n tables
     *
     * @param int $n
     * @param int $perPage
     * @return void
     */
    private function generateTopTables($n = 10, $perPage = 100)
    {
        $columns = [
            [
                'name' => 'index',
                'title' => '#',
            ],
            [
                'name' => 'kuski',
                'title' => 'Kuski',
            ],
            [
                'name' => 'time',
                'title' => 'Time',
            ],

            [
                'name' => 'driven_at',
                'title' => 'Driven at'
            ]
        ];

        $before = CarbonHelper::maxCarbonInput($this->request->input('before'));
        $after = CarbonHelper::minCarbonInput($this->request->input('after'));

        $dateRange = new DateRange($after, $before);

        $title = sprintf("%s, %s", $this->level->name, $this->level->filename);

        $table = (new DefaultTable())
            ->setCols($columns)
            // ->hideHeader()
            ->disableFiltering()
            ->perPage($perPage)
            ->setTitle($title);

        foreach ($this->level->topForKuski($this->kuski, $this->pack, $n, $dateRange) as $key => $time) {
            $kuski = view()->make('kuskis.partials.compactKuskiIdentifier', [
                    'kuski' => $this->kuski,
                ])
                ->render();

            $table->pushRow([
                'index' => ++$key,
                'kuski' => $kuski,
                'time' => $time->time,
                'driven_at' => (string) $time->driven_at
            ]);
        }

        $this->pushTable($table);

    }

    /**
     * Generate personal records table
     *
     * @return void
     */
    private function generatePRTable()
    {
        $columns = [
            [
                'name' => 'index',
                'title' => '#',
            ],
            [
                'name' => 'level',
                'title' => 'Level',
            ],
            [
                'name' => 'kuski',
                'title' => 'Kuski',
            ],
            [
                'name' => 'time',
                'title' => 'Time',
            ],
            [
                'name' => 'diff',
                'title' => 'WR diff',
            ],
            [
                'name' => 'driven_at',
                'title' => 'Driven at'
            ]
        ];

        $input = $this->request->has('before') ? $this->request->input('before') : '';
        $before = CarbonHelper::maxCarbonInput($input);
        $input = $this->request->has('after') ? $this->request->input('after') : '';
        $after = CarbonHelper::minCarbonInput($input);

        $dateRange = new DateRange($after, $before);

        $table = (new DefaultTable())
            ->setCols($columns)
            ->disableFiltering()
            ->perPage(100)
            ->setTitle("Personal records");

        $prs = $this->pack->getPersonalRecordsForKuski($this->kuski, $dateRange);

        foreach($prs as $key => $pr) {
            $kuski = view()->make('kuskis.partials.compactKuskiIdentifier', [
                    'kuski' => $this->kuski,
                ])
                ->render();

            $drivenAt = $pr->driven_at ?? '-';

            $wr = $pr->level->worldRecordForPack($this->pack);

            if ($pr->time && $wr->time) {
                $diff = sprintf('+%s', mstostrtime($pr->timeMs - $wr->timeMs));
            } else {
                $diff = '-';
            }

            $table->pushRow([
                'index' => ++$key,
                'level' => sprintf("%s, %s", $pr->level->name, $pr->level->filename),
                'kuski' => $kuski,
                'time' => $pr->time ?? '-',
                'diff' => $diff,
                'driven_at' => (string) $drivenAt,
            ]);
        }

        $this->pushTable($table);

        $columns = [
            [
                'name' => 'type',
                'title' => 'Type',
            ],
            [
                'name' => 'value',
                'title' => 'Value',
            ],

        ];

        $table = (new DefaultTable())
            ->setCols($columns)
            ->disableFiltering()
            ->perPage(100)
            ->setTitle("Meta stats");

        $wrtt = $this->pack->getWorldRecordsTotalTime(false);

        $tt = $prs->reduce(function($carry, $pr) {
            $time = $pr->time === null ? config('promo.unfinished_time_penalty') : $pr->timeMs;
            $carry += $time;

            return $carry;
        }, 0);

        $table->pushRow([
            'type' => 'Total time',
            'value' => mstostrtime($tt),
        ]);

        $table->pushRow([
            'type' => 'WR TT diff',
            'value' => sprintf("+%s", mstostrtime($tt - $wrtt)),
        ]);

        $this->pushTable($table);
    }
}
