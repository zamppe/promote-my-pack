<?php

namespace App\Statistics;

use App\Utils\TypesBase;
use App\Utils\GetsClassConstants;

/**
 * Class ActivityTypes
 * @package App\Statistics
 */
class ActivityTypes extends TypesBase
{
    use GetsClassConstants;

    const FF = 'ff';
    const PR = 'pr';
    const WR = 'wr';

    public static function getAllWithLang()
    {
        $constants = self::getAll();
        $constantsLang = ActivityTypesLang::getAll();
        $constantsWithLang = collect();

        return $constants->combine($constantsLang);
    }
}
