<?php

namespace App\Statistics;

use App\Utils\TypesBase;
use App\Utils\GetsClassConstants;

/**
 * Class StatisticsFormFields
 * @package App\Statistics
 */
class StatisticsFormFields extends TypesBase
{
    use GetsClassConstants;

    const CATEGORY = 'category';
    const TYPE = 'type';
    const PACK = 'pack';
    const PACK_ALL = 'pack_all';
    const KUSKI = 'kuski';
    const KUSKI_ALL = 'kuski_all';
    const LEVEL = 'level';
    const LEVEL_ALL = 'level_all';
    const DATES = 'dates';
    const ACTIVITY_TYPES = 'activity_types';
}
