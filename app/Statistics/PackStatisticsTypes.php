<?php

namespace App\Statistics;

use App\Utils\TypesBase;
use App\Utils\GetsClassConstants;

/**
 * Class PackStatisticsTypes
 * @package App\Statistics
 */
class PackStatisticsTypes extends TypesBase
{
    use GetsClassConstants;

    const TOTALTIME = 1;
    const RECENT_ACTIVITY = 2;
    const FINISH_COUNTS = 3;

    public static function getAllWithLang()
    {
        $constants = self::getAll();
        $constantsLang = PackStatisticsTypesLang::getAll();
        $constantsWithLang = collect();

        return $constants->combine($constantsLang);
    }
}
