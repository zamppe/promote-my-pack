<?php

namespace App\Statistics;

use App\Utils\TypesBase;
use App\Utils\GetsClassConstants;

/**
 * Class ActivityTypesLang
 * @package App\Statistics
 */
class ActivityTypesLang extends TypesBase
{
    use GetsClassConstants;

    const FF = 'FF';
    const PR = 'PR';
    const WR = 'WR';
}
