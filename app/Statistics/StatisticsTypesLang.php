<?php

namespace App\Statistics;

use App\Utils\TypesBase;
use App\Utils\GetsClassConstants;

/**
 * Class StatisticsTypesLang
 * @package App\Statistics
 */
class StatisticsTypesLang extends TypesBase
{
    use GetsClassConstants;

    const RECENT_ACTIVITY = 'Recent activity';
    const GENERAL = 'Pack - General';
    const WR_AGES_CURRENT = 'Pack - Current WR ages';
    const WR_DEVELOPMENT = 'Pack - WR development';
    const WR_DEVELOPMENT_PER_LEVEL = 'Pack - WR development per level';
    const WR_KUSKI_STATISTICS = 'Pack - Kuski WR statistics';
    const WR_AMOUNT_PER_LEVEL = 'Pack - WR improvements per level';
    const TOP_10 = "Kuski - Top 10s";
    const TOP_100 = "Kuski - Top 100s";
    const PRS = "Kuski - PRs";
}
