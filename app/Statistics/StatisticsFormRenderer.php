<?php

namespace App\Statistics;

use App\Models\Kuski;
use App\Models\Pack;
use App\Statistics\StatisticsFormFields;
use App\Statistics\StatisticsTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Class StatisticsFormRenderer
 * @package App\Http\Controllers
 */
class StatisticsFormRenderer
{
    protected $request;

    /**
     * Handle
     *
     * @param Request $request
     * @return string
     */
    public function handle(Request $request)
    {
        $this->request = $request;

        $type = (int) $this->request->input(StatisticsFormFields::TYPE);

        $statisticsTypeConstName = StatisticsTypes::getConstNameForInput($type);

        if ($statisticsTypeConstName && $this->hasStatisticsTypeHandler($statisticsTypeConstName)) {
            return $this->{'handle'.Str::studly($statisticsTypeConstName)}();
        }

        /* Default form */
        return $this->handleRecentActivity();
    }

    /**
     * Is there a handle method for the given statistics type
     *
     * @param string $type
     * @return bool
     */
    private function hasStatisticsTypeHandler($type)
    {
        return method_exists($this, 'handle'.Str::studly($type));
    }

    /**
     * Handle recent activity statistics type
     *
     * @return string
     */
    private function handleRecentActivity()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK_ALL,
            StatisticsFormFields::KUSKI_ALL,
            StatisticsFormFields::LEVEL_ALL,
            StatisticsFormFields::ACTIVITY_TYPES,
        ]);
    }

    /**
     * Handle general statistics type
     *
     * @return string
     */
    private function handleGeneral()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK,
        ]);
    }

    /**
     * Handle wr ages statistics type
     *
     * @return string
     */
    private function handleWrAgesCurrent()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK,
        ]);
    }

    /**
     * Handle wr development statistics type
     *
     * @return string
     */
    private function handleWrDevelopment()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK,
        ]);
    }

    /**
     * Handle wr development per level statistics type
     *
     * @return string
     */
    private function handleWrDevelopmentPerLevel()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK,
            StatisticsFormFields::LEVEL,
        ]);
    }

    /**
     * Handle wr kuski statistics type
     *
     * @return string
     */
    private function handleWrKuskiStatistics()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK,
        ]);
    }

    /**
     * Handle wr amount per level statistics type
     *
     * @return string
     */
    private function handleWrAmountPerLevel()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK,
            StatisticsFormFields::LEVEL,
        ]);
    }

    /**
     * Handle top10 statistics type
     *
     * @return string
     */
    private function handleTop10()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK,
            StatisticsFormFields::KUSKI,
            StatisticsFormFields::LEVEL,
            StatisticsFormFields::DATES,
        ]);
    }

    /**
     * Handle top100 statistics type
     *
     * @return string
     */
    private function handleTop100()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK,
            StatisticsFormFields::KUSKI,
            StatisticsFormFields::LEVEL,
            StatisticsFormFields::DATES,
        ]);
    }

    /**
     * Handle prs statistics type
     *
     * @return string
     */
    private function handlePrs()
    {
        return $this->renderForm([
            StatisticsFormFields::TYPE,
            StatisticsFormFields::PACK,
            StatisticsFormFields::KUSKI,
            StatisticsFormFields::DATES,
        ]);
    }

    /**
     * Render the form
     *
     * @return string
     */
    private function renderForm(array $formFields = [])
    {
        $fields = '';

        foreach ($formFields as $field) {
            $fields .= $this->{'render'.Str::studly($field)}();
        }

        $fields .= $this->renderSubmit();

        return view()->make('statistics.forms.statisticsForm', compact('fields'))->render();
    }

    /**
     * Render type field
     *
     * @return string
     */
    private function renderType()
    {
        $types = StatisticsTypes::getAllWithLang();

        return view()->make('statistics.forms.partials.typeSelect', compact('types'))->render();
    }

    /**
     * Render pack field
     *
     * @return string
     */
    private function renderPack($all = false)
    {
        $packs = Pack::all()->mapWithKeys(function ($pack) {
            return [$pack->id => $pack->name];
        });

        if ($all) {
            $packs->prepend('All', 'all');
        }

        return view()->make('statistics.forms.partials.packSelect', compact('packs'))->render();
    }

    /**
     * Render pack field with all packs option
     *
     * @return string
     */
    private function renderPackAll()
    {
        return $this->renderPack(true);
    }

    /**
     * Render level field
     *
     * @return string
     */
    private function renderLevel($all = false)
    {
        $pack = Pack::find($this->request->input(StatisticsFormFields::PACK));

        if (! $pack) {
            $pack = Pack::first();
        }

        $levels = $pack->levels->mapWithKeys(function ($level) {
            return [$level->id => $level->name];
        });

        if ($all) {
            $levels->prepend('All', 'all');
        }

        return view()->make('statistics.forms.partials.levelSelect', compact('levels'))->render();
    }

    /**
     * Render level field with all levels option
     *
     * @return string
     */
    private function renderLevelAll()
    {
        return $this->renderLevel(true);
    }

    /**
     * Render kuski field
     *
     * @return string
     */
    private function renderKuski($all = false)
    {
        $pack = Pack::find($this->request->input(StatisticsFormFields::PACK));

        if (! $pack) {
            $kuskis = Kuski::all();
        } else {
            $kuskis = $pack->getKuskis()->get();
        }

        $kuskis = $kuskis->mapWithKeys(function ($kuski) {
            return [$kuski->id => $kuski->name];
        })->sort();

        if ($all) {
            $kuskis->prepend('All', 'all');
        }

        return view()->make('statistics.forms.partials.kuskiSelect', compact('kuskis'))->render();
    }

    /**
     * Render kuski field with all kuskis option
     *
     * @return string
     */
    private function renderKuskiAll()
    {
        return $this->renderKuski(true);
    }

    /**
     * Render date filter fields
     *
     * @return string
     */
    private function renderDates()
    {
        return view()->make('statistics.forms.partials.dateFilters')->render();
    }

    /**
     * Render activity types field
     *
     * @return string
     */
    private function renderActivityTypes()
    {
        $types = ActivityTypes::getAllWithLang()
            ->prepend('All', 'all')
            ->toArray();

        return view()->make('statistics.forms.partials.activityTypes', compact('types'))->render();
    }

    /**
     * Render submit button
     *
     * @return string
     */
    private function renderSubmit()
    {
        return view()->make('statistics.forms.partials.submit')->render();
    }
}
