<?php

namespace App\Statistics;

use App\Utils\TypesBase;
use App\Utils\GetsClassConstants;

/**
 * Class StatisticsTypes
 * @package App\Statistics
 */
class StatisticsTypes extends TypesBase
{
    use GetsClassConstants;

    const RECENT_ACTIVITY = 1;
    const GENERAL = 2;
    const WR_AGES_CURRENT = 3;
    const WR_DEVELOPMENT = 4;
    const WR_DEVELOPMENT_PER_LEVEL = 5;
    const WR_KUSKI_STATISTICS = 6;
    const WR_AMOUNT_PER_LEVEL = 7;
    const TOP_10 = 8;
    const TOP_100 = 9;
    const PRS = 10;

    /**
     * Get all constants as [const name => human readable name] pairs
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getAllWithLang()
    {
        $constants = self::getAll();
        $constantsLang = StatisticsTypesLang::getAll();
        $constantsWithLang = collect();

        return $constants->combine($constantsLang);
    }

    /**
     * Get the name of a constant based on an int value
     * For example if $typeValue === 1, return 'RECENT_ACTIVITY'
     *
     * @param int|string $typeValue
     * @return string
     */
    public static function getConstNameForInput($typeValue)
    {
        return self::getAll()->flip()->get($typeValue) ?? '';
    }
}
