<?php

namespace App\Statistics;

use App\Utils\TypesBase;
use App\Utils\GetsClassConstants;

/**
 * Class PackStatisticsTypesLang
 * @package App\Statistics
 */
class PackStatisticsTypesLang extends TypesBase
{
    use GetsClassConstants;

    const TOTALTIME_LANG = 'Totaltimes';
    const RECENT_ACTIVITY_LANG = 'Recent activity';
    const FINISH_COUNTS_LANG = 'Finish counts';
}
