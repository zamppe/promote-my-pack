<?php

namespace App\Statistics;

use App\Footable\FootablesCollection;
use App\Footable\DefaultTable;
use App\Models\Pack;
use App\Models\Level;
use App\Models\Kuski;
use App\Services\RecentActivityBuilder;
use App\Utils\CarbonHelper;
use App\Utils\DateRange;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

/**
 * Class StatisticsCollection
 * @package App\Statistics
 */
class StatisticsCollection extends FootablesCollection
{
    public function __construct(Request $request)
    {
        $required = [
            StatisticsTypes::RECENT_ACTIVITY => [
                'type',
            ],
            StatisticsTypes::GENERAL => [
                'type',
                'pack',
            ],
            StatisticsTypes::WR_AGES_CURRENT => [
                'type',
                'pack',
            ],
            StatisticsTypes::WR_DEVELOPMENT => [
                'type',
                'pack',
            ],
            StatisticsTypes::WR_DEVELOPMENT_PER_LEVEL => [
                'type',
                'pack',
                'level',
            ],
            StatisticsTypes::WR_KUSKI_STATISTICS => [
                'type',
                'pack',
            ],
            StatisticsTypes::WR_AMOUNT_PER_LEVEL => [
                'type',
                'pack',
                'level',
            ],
            StatisticsTypes::TOP_10 => [
                'kuski',
                'pack',
                'level',
                'type',
            ],
            StatisticsTypes::TOP_100 => [
                'kuski',
                'pack',
                'level',
                'type',
            ],
            StatisticsTypes::PRS => [
                'kuski',
                'pack',
                'type',
            ]
        ];

        parent::__construct($request, $required);

        if ($this->hasRequired()) {
            $this->handle();
        }
    }

    /**
     * Handle
     *
     * @return StatisticsCollection
     */
    private function handle()
    {
        $type = (int) $this->request->input(StatisticsFormFields::TYPE);

        $statisticsTypeConstName = StatisticsTypes::getConstNameForInput($type);

        if ($this->hasStatisticsTypeHandler($statisticsTypeConstName)) {
            return $this->{'handle'.Str::studly($statisticsTypeConstName)}();
        }

        /* Return no results */
        return $this;
    }

    /**
     * Is there a handle method for the given statistics type
     *
     * @param string $type
     * @return bool
     */
    private function hasStatisticsTypeHandler($type)
    {
        return method_exists($this, 'handle'.Str::studly($type));
    }

    private function resolveModel($requestInput)
    {
        if ($this->request->has($requestInput)) {
            $input = $this->request->input($requestInput);

            if ($input != 'all') {
                switch ($input) {
                    case $requestInput === 'pack':
                        return Pack::find($input);
                    case $requestInput === 'kuski':
                        return Kuski::find($input);
                    case $requestInput === 'level':
                        return Level::find($input);
                }
            }
        }

        return null;
    }

    /**
     * 1. Recent activity
     *
     * @return StatisticsCollection
     */
    private function handleRecentActivity()
    {
        $pack = $this->resolveModel('pack');
        $kuski = $this->resolveModel('kuski');
        $level = $this->resolveModel('level');
        $builder = RecentActivityBuilder::start()
            ->limit(200);

        if ($pack) {
            $builder->pack($pack);
        }

        if ($kuski) {
            $builder->kuski($kuski);
        }

        if ($level) {
            $builder->level($level);
        }

        if ($this->request->has('activity_type') && $input = $this->request->input('activity_type')) {
            if ($input === 'pr') {
                $builder->pr();
            } else if ($input === 'wr') {
                $builder->wr();
            } else if ($input === 'ff') {
                $builder->ff();
            } else {
                $builder->all();
            }
        } else {
            $builder->all();
        }

        $recentActivity = $builder->get();

        $columns = [
            [
                'name' => 'level',
                'title' => 'Level',
            ],
            [
                'name' => 'activity',
                'title' => 'Activity',
            ],
            [
                'name' => 'kuski',
                'title' => 'Kuski',
            ],
            [
                'name' => 'time',
                'title' => 'Time',
            ],
            [
                'name' => 'date',
                'title' => 'Date',
            ],
        ];

        $table = (new DefaultTable())
            ->setCols($columns);

        foreach ($recentActivity as $time) {
            $kuskiRendered = view()->make('kuskis.partials.compactKuskiIdentifier', [
                'kuski' => $time->kuski,
            ])->render();

            $activity = view()->make('packs.partials.recentActivityTag', [
                'time' => $time,
            ])->render();

            $date = view()->make('components.date', [
                'date' => $time->driven_at,
            ])->render();

            $table->pushRow([
                'level' => $time->level->full_name,
                'activity' => $activity,
                'kuski' => $kuskiRendered,
                'time' => $time->time,
                'date' => $date,
            ]);
        }

        $this->pushTable($table);

        return $this;
    }

    /**
     * 2. General
     *
     * @return StatisticsCollection
     */
    private function handleGeneral()
    {
        $pack = Pack::find($this->request->input('pack'));

        if (! $pack) {
            return $this;
        }

        $columns = [
            [
                'name' => 'type',
                'title' => 'Type',
            ],
            [
                'name' => 'value',
                'title' => 'Value',
            ],
        ];

        $table = (new DefaultTable())
            ->setCols($columns);

        $wrTT = $pack->getWorldRecordsTotalTime();

        $wrsAvg = mstostrtime($pack->bestTimesAverage);

        $totalTimesLeader = $pack->totalTimesLeader();
        $worldRecordsLeader = $pack->worldRecordsLeader();

        $totalTimesLeaderRendered = $totalTimesLeader ? view()->make('statistics.kuski.kuskiCell', [
            'kuski' => $totalTimesLeader->kuski,
            'data' => mstostrtime($totalTimesLeader->total_time),
        ])
        ->render() : '';

        $worldRecordsLeaderRendered = view()->make('statistics.kuski.kuskiCell', [
            'kuski' => $worldRecordsLeader[0],
            'data' => $worldRecordsLeader[1],
            'unit' => 'WR(s)',
        ])
        ->render();

        $table->pushRow([
            'type' => 'Number of levels',
            'value' => $pack->levels->count(),
        ]);

        $table->pushRow([
            'type' => 'WR TT',
            'value' => $wrTT,
        ]);

        $table->pushRow([
            'type' => 'WR average',
            'value' => $wrsAvg,
        ]);

        $table->pushRow([
            'type' => 'TT leader',
            'value' => $totalTimesLeaderRendered,
        ]);

        $table->pushRow([
            'type' => 'WR leader',
            'value' => $worldRecordsLeaderRendered,
        ]);

        $table->disableFiltering();

        $this->pushTable($table);

        return $this;
    }

    /**
     * 3. WR ages
     *
     * @return StatisticsCollection
     */
    private function handleWrAgesCurrent()
    {
        $pack = Pack::find($this->request->input('pack'));

        if (! $pack) {
            return $this;
        }

        $columns = [
            [
                'name' => 'level',
                'title' => 'Level',
            ],
            [
                'name' => 'kuski',
                'title' => 'Kuski',
            ],
            [
                'name' => 'time',
                'title' => 'Time',
            ],
            [
                'name' => 'date',
                'title' => 'Date',
            ],
        ];

        $table = (new DefaultTable())
            ->setCols($columns);

        $wrs = $pack
            ->getWorldRecords()
            ->filter(function($time) {
                return $time->time;
            })
            ->sortBy('driven_at');

        foreach ($wrs as $wr) {
            $kuski = view()->make('kuskis.partials.compactKuskiIdentifier', [
                'kuski' => $wr->kuski,
            ])->render();

            $date = view()->make('components.date', [
                'date' => $wr->driven_at,
            ])->render();

            $table->pushRow([
                'level' => $wr->level->fullName,
                'kuski' => $kuski,
                'time' => $wr->time,
                'date' => $date,
            ]);
        }

        $this->pushTable($table);

        return $this;
    }

    /**
     * 4. WR development
     *
     * @return StatisticsCollection
     */
    private function handleWrDevelopment()
    {
        $pack = Pack::find($this->request->input('pack'));

        if (! $pack) {
            return $this;
        }

        $wrImprovements = $pack
            ->wrImprovementsWithTotalTime(['times.level_id', 'times.time']);

        $this->generateWrDevelopment($wrImprovements);

        return $this;
    }

    /**
     * 5. WR development per level
     *
     * @return StatisticsCollection
     */
    private function handleWrDevelopmentPerLevel()
    {
        $pack = Pack::find($this->request->input('pack'));
        $level = Level::find($this->request->input('level'));

        if (! $pack || ! $level) {
            return $this;
        }

        $wrImprovements = $pack
            ->wrImprovements($level);

        $this->generateWrDevelopment($wrImprovements, false);

        return $this;
    }

    /**
     * 6. WR kuski statistics
     *
     * @return StatisticsCollection
     */
    private function handleWrKuskiStatistics()
    {
        $pack = Pack::find($this->request->input('pack'));

        if (! $pack) {
            return $this;
        }

        $columns = [
            [
                'name' => 'kuski',
                'title' => 'Kuski',
            ],
            [
                'name' => 'total_improvement',
                'title' => 'Total improvement',
            ],
            [
                'name' => 'improvements',
                'title' => 'Number of improvements',
            ],
            [
                'name' => 'wr_count',
                'title' => 'Current WR count',
            ],
        ];

        $table = (new DefaultTable())
            ->setCols($columns);

        $wrImprovements = $pack
            ->wrImprovements()
            ->groupBy('kuski_id')
            ->sortByDesc(function($item) {
                return $item->count();
            });

        $wrs = $pack
            ->getWorldRecords()
            ->groupBy('kuski_id')
            ->sortByDesc(function($wrsByKuski) {
                return $wrsByKuski->count();
            });

        foreach ($wrImprovements as $wrImprovementsByKuski) {
            $kuski = $wrImprovementsByKuski->first()->kuski;
            $kuskiRendered = view()->make('kuskis.partials.compactKuskiIdentifier', [
                    'kuski' => $kuski,
                ])
                ->render();
            $totalImprovement = $wrImprovementsByKuski->reduce(function($carry, $wrImprovement) {
                if ($wrImprovement->old_time_ms) {
                    $carry += $wrImprovement->old_time_ms - $wrImprovement->time_ms;
                }

                return $carry;
            }, 0);
            $currentWrsByKuski = $wrs->get($kuski->id);
            $currentWrCountByKuski = $currentWrsByKuski ? $currentWrsByKuski->count() : 0;

            $table->pushRow([
                'kuski' => $kuskiRendered,
                'improvements' => $wrImprovementsByKuski->count(),
                'total_improvement' => $totalImprovement ? mstostrtime($totalImprovement) : '-',
                'wr_count' => $currentWrCountByKuski,
            ]);
        }

        $this->pushTable($table);

        return $this;
    }

    /**
     * 7. WR improvements per level
     *
     * @return StatisticsCollection
     */
    private function handleWrAmountPerLevel()
    {
        $pack = Pack::find($this->request->input('pack'));
        $level = Level::find($this->request->input('level'));

        if (! $pack || ! $level) {
            return $this;
        }

        $columns = [
            [
                'name' => 'level',
                'title' => 'Level',
            ],
            [
                'name' => 'kuski',
                'title' => 'Kuski',
            ],
            [
                'name' => 'improvements',
                'title' => 'Number of improvements',
            ],
            [
                'name' => 'total_improvement',
                'title' => 'Total improvement',
            ],
        ];

        $table = (new DefaultTable())
            ->setCols($columns);

        $wrImprovements = $pack
            ->wrImprovements($level)
            ->groupBy('kuski_id')
            ->sortByDesc(function($item) {
                return $item->count();
            });

        foreach ($wrImprovements as $wrImprovementsByKuski) {
            $kuski = $wrImprovementsByKuski->first()->kuski;
            $level = $wrImprovementsByKuski->first()->level;
            $kuskiRendered = view()->make('kuskis.partials.compactKuskiIdentifier', [
                    'kuski' => $kuski,
                ])
                ->render();
            $totalImprovement = $wrImprovementsByKuski->reduce(function($carry, $wrImprovement) {
                if ($wrImprovement->old_time_ms) {
                    $carry += $wrImprovement->old_time_ms - $wrImprovement->time_ms;
                }

                return $carry;
            }, 0);

            $table->pushRow([
                'level' => $level->fullName,
                'kuski' => $kuskiRendered,
                'improvements' => $wrImprovementsByKuski->count(),
                'total_improvement' => $totalImprovement ? mstostrtime($totalImprovement) : '-'
            ]);
        }

        $this->pushTable($table);

        return $this;
    }

    /**
     * 8. Top 10
     *
     * @return StatisticsCollection
     */
    private function handleTop10()
    {
        $this->generateTopTables(10);
    }

    /**
     * 9. Top 100
     *
     * @return StatisticsCollection
     */
    private function handleTop100()
    {
        $this->generateTopTables(100);
    }

    /**
     * 10. PR
     *
     * @return StatisticsCollection
     */
    private function handlePrs()
    {
        $pack = Pack::find($this->request->input('pack'));
        $kuski = Kuski::find($this->request->input('kuski'));

        if (! $pack || ! $kuski) {
            return $this;
        }

        $columns = [
            [
                'name' => 'index',
                'title' => '#',
            ],
            [
                'name' => 'level',
                'title' => 'Level',
            ],
            [
                'name' => 'kuski',
                'title' => 'Kuski',
            ],
            [
                'name' => 'time',
                'title' => 'Time',
            ],
            [
                'name' => 'diff',
                'title' => 'WR diff',
            ],
            [
                'name' => 'date',
                'title' => 'Date'
            ]
        ];

        $before = CarbonHelper::maxCarbonInput($this->request->input('before'));
        $after = CarbonHelper::minCarbonInput($this->request->input('after'));

        $dateRange = new DateRange($after, $before);

        $table = (new DefaultTable())
            ->setCols($columns)
            ->disableFiltering()
            ->perPage(100)
            ->setTitle("Personal records");

        $prs = $pack->getPersonalRecordsForKuski($kuski, $dateRange);

        foreach($prs as $key => $pr) {
            $kuskiRendered = view()->make('kuskis.partials.compactKuskiIdentifier', [
                    'kuski' => $kuski,
                ])
                ->render();

            $drivenAt = $pr->driven_at ?? '-';

            $wr = $pr->level->worldRecordForPack($pack);

            if ($pr->time && $wr->time) {
                $diff = sprintf('+%s', mstostrtime($pr->timeMs - $wr->timeMs));
            } else {
                $diff = '-';
            }

            $table->pushRow([
                'index' => ++$key,
                'level' => sprintf("%s, %s", $pr->level->name, $pr->level->filename),
                'kuski' => $kuskiRendered,
                'time' => $pr->time ?? '-',
                'diff' => $diff,
                'date' => (string) $drivenAt,
            ]);
        }

        $this->pushTable($table);

        $columns = [
            [
                'name' => 'type',
                'title' => 'Type',
            ],
            [
                'name' => 'value',
                'title' => 'Value',
            ],

        ];

        $table = (new DefaultTable())
            ->setCols($columns)
            ->disableFiltering()
            ->perPage(100)
            ->setTitle("Meta stats");

        $wrtt = $pack->getWorldRecordsTotalTime(false);

        $tt = $prs->reduce(function($carry, $pr) {
            $time = $pr->time === null ? config('promo.unfinished_time_penalty') : $pr->timeMs;
            $carry += $time;

            return $carry;
        }, 0);

        $table->pushRow([
            'type' => 'Total time',
            'value' => mstostrtime($tt),
        ]);

        $table->pushRow([
            'type' => 'WR TT diff',
            'value' => sprintf("+%s", mstostrtime($tt - $wrtt)),
        ]);

        $this->pushTable($table);
    }

    /**
     * Generate a WR development table
     *
     * @param \Illuminate\Support\Collection $wrImprovements
     * @param bool $includeWrTT
     *
     * @return StatisticsCollection
     */
    private function generateWrDevelopment($wrImprovements, $includeWrTT = true)
    {
        $columns = collect([
            [
                'name' => 'level',
                'title' => 'Level',
            ],
            [
                'name' => 'kuski',
                'title' => 'Kuski',
            ],
            [
                'name' => 'time',
                'title' => 'Time',
            ],
            [
                'name' => 'improvement',
                'title' => 'Improvement',
            ]
        ]);

        if ($includeWrTT) {
            $columns = $columns->merge([
                [
                    'name' => 'wrtt',
                    'title' => 'WR TT',
                ],
            ]);
        }

        $columns = $columns->merge([
            [
                'name' => 'span',
                'title' => 'Span',
            ],
            [
                'name' => 'age',
                'title' => 'Age',
            ],
        ]);


        $table = (new DefaultTable())
            ->setCols($columns->toArray());

        $beatenByMap = $wrImprovements->keyBy('old_time_id');

        foreach ($wrImprovements as $wr) {
            $kuski = view()->make('kuskis.partials.compactKuskiIdentifier', [
                    'kuski' => $wr->kuski,
                ])
                ->render();
            $beatenBy = $beatenByMap->get($wr->id);
            $beatenByDrivenAt = $beatenBy ? $beatenBy->driven_at : null;
            $improvement = $wr->old_time_ms ? mstostrtime($wr->old_time_ms - $wr->timeMs) : '';
            $age = $wr->driven_at->diffForHumans($beatenByDrivenAt);
            $span = (string) $wr->driven_at . ' &ndash; '.($beatenBy ? (string) $beatenBy->driven_at : '');
            $row = [
                'level' => $wr->level->fullName,
                'kuski' => $kuski,
                'time' => $wr->time,
                'improvement' => $improvement,
                'span' => $span,
                'age' => $age,
            ];

            if ($includeWrTT) {
                $row['wrtt'] = mstostrtime($wr->wrtt, true);
            }

            $table->pushRow($row);
        }

        $this->pushTable($table);
    }

    /**
     * Generate top n tables
     *
     * @param int $n
     * @param int $perPage
     * @return void
     */
    private function generateTopTables($n = 10, $perPage = 100)
    {
        $pack = Pack::find($this->request->input('pack'));
        $level = Level::find($this->request->input('level'));
        $kuski = Kuski::find($this->request->input('kuski'));

        if (! $pack || ! $level || ! $kuski) {
            return $this;
        }

        $columns = [
            [
                'name' => 'index',
                'title' => '#',
            ],
            [
                'name' => 'kuski',
                'title' => 'Kuski',
            ],
            [
                'name' => 'time',
                'title' => 'Time',
            ],

            [
                'name' => 'date',
                'title' => 'Date'
            ]
        ];

        $before = CarbonHelper::maxCarbonInput($this->request->input('before'));
        $after = CarbonHelper::minCarbonInput($this->request->input('after'));

        $dateRange = new DateRange($after, $before);

        $title = sprintf("%s, %s", $level->name, $level->filename);

        $table = (new DefaultTable())
            ->setCols($columns)
            // ->hideHeader()
            ->disableFiltering()
            ->perPage($perPage)
            ->setTitle($title);

        foreach ($level->topForKuski($kuski, $pack, $n, $dateRange) as $key => $time) {
            $kuskiRendered = view()->make('kuskis.partials.compactKuskiIdentifier', [
                'kuski' => $kuski,
            ])->render();

            $date = view()->make('components.date', [
                'date' => $time->driven_at,
            ])->render();

            $table->pushRow([
                'index' => ++$key,
                'kuski' => $kuskiRendered,
                'time' => $time->time,
                'date' => $date,
            ]);
        }

        $this->pushTable($table);
    }
}
