<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Sortable;
use App\Models\Traits\HandlesLevelsZips;
use App\Utils\DateRange;
use App\Models\Traits\HasActivity;

/**
 * Class Pack
 * @package App\Models
 */
class Pack extends Model
{
    use Sortable, HandlesLevelsZips, HasActivity {
        wrImprovements as baseWrImprovements;
    }

    const UPDATE_FREQUENCY_DAILY = 1;
    const UPDATE_FREQUENCY_WEEKLY = 2;
    const UPDATE_FREQUENCY_MONTHLY = 3;

    protected $fillable = [
        'user_id',
        'name',
        'update_frequency',
        'deadline',
    ];

    protected $dates = [
        'deadline'
    ];

    /**
     * All levels of this pack
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function levels()
    {
        return $this->belongsToMany(Level::class)
            ->withPivot('position')
            ->orderBy('position');
    }

    /**
     * All total times of this pack
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function totalTimes()
    {
        return $this->hasMany(TotalTime::class);
    }

    /**
     * All local time bans of this pack
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function localTimeBans()
    {
        return $this->hasMany(LocalTimeBan::class);
    }

    /**
     * All local kuski bans of this pack
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function localKuskiBans()
    {
        return $this->hasMany(LocalKuskiBan::class);
    }

    /**
     * All target achievements of this pack
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function targetAchievements()
    {
        return $this->hasMany(TargetAchievement::class);
    }

    /**
     * All custom achievements of this pack
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customAchievements()
    {
        return $this->hasMany(CustomAchievement::class);
    }

    /**
     * The user that created this pack
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * All news of this pack
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function news()
    {
        return $this->morphMany('App\Models\News', 'newsable');
    }

    /**
     * Get the achieves to this pack
     *
     * @return static
     */
    public function achieves()
    {
        return Achieve::with('achievable')->get()->filter(function($achieve) {
            if ($achieve->achievable) {
                return $achieve->achievable->pack_id == $this->id;
            }

            return false;
        });
    }

    /**
     * Accessor for getting the cache tag of this pack
     *
     * @return mixed
     */
    public function getCacheTagAttribute()
    {
        return "pack{$this->id}";
    }

    /**
     * Accessor for getting the number of levels in this pack
     *
     * @return mixed
     */
    public function getLevelAmountAttribute()
    {
        return $this->levels()->count();
    }

    /**
     * Accessor for getting world records for this pack
     *
     * @return mixed
     * @todo investigate if this is redundant
     */
    public function getBestTimesAttribute()
    {
        return Time::query()
            ->selectRaw('min(time) as time, level_id')
            ->whereIn('level_id', $this->levels()->pluck('id'))
            ->groupBy('level_id')
            ->filterByPack($this)
            ->get();
    }

    /**
     * Accessor for getting the average of world records for this pack
     *
     * @return float|int
     */
    public function getBestTimesAverageAttribute()
    {
        $times = $this->getWorldRecords();

        return $times->reduce(function($sum, $time) {
            return $sum + $time->timeMs;
        }) / $times->count();
    }

    /**
     * Get kuskis involved in this pack as a collection of kuski_ids
     *
     * @return \Illuminate\Support\Collection
     */
    public function getKuskiIds()
    {
        /* @todo make protected if pos */
        return $this->prImprovementsQuery()
            ->groupBy('times.kuski_id')
            ->pluck('times.kuski_id');
    }

    /**
     * Get kuskis involved in this pack as a query builder object
     * Optionally filter the results by kuski name
     * Optionally limit the number of results
     *
     * @param string $whereLike
     * @param int $limit
     * @return mixed
     */
    public function getKuskis($whereLike = '', $limit = 0)
    {
        $kuskiIds = $this->getKuskiIds();

        $query = Kuski::whereIn('id', $kuskiIds)
            ->where('name', 'like', "%{$whereLike}%")
            ->orWhereNull('name');

        if ($limit) {
            $query->limit($limit);
        }

        return $query;
    }

    /**
     * Get total times list for this pack
     *
     * @param int $kuskiId
     * @param int $limit
     * @param DateRange|null $dateRange
     * @param string $kuskiLike
     * @todo check if possible to use something else than mergebindings
     * @todo check if it's ok to embed $levels->count() and $unfinishedTimePenalty into the query like that, or is it possible to use addbindings or something
     * @return \Illuminate\Support\Collection
     */
    public function getTotalTimes($kuskiId = 0, $limit = 0, DateRange $dateRange = null, $kuskiLike = '')
    {
        $unfinishedTimePenalty = config('promo.unfinished_time_penalty');
        $levels = $this->levels()->pluck('id');

        if($kuskiId) {
            $subQuery = $this->getBestTimesForKuskiPerLevelQuery($kuskiId, $dateRange);
        } else {
            $subQuery = $this->getBestTimesPerKuskiPerLevelQuery($dateRange, $kuskiLike);
        }

        $subQuery2 = \DB::table('')->selectRaw('kuski_id, sum(time) as total_time, count(distinct t1.level_id) as finished_levels')
            ->from(\DB::raw('(' . $subQuery->toSql() . ') as t1'))
            ->mergeBindings($subQuery)
            ->groupBy('kuski_id')
            ->orderBy('total_time', 'asc');

        $totals = Kuski::selectRaw('kuski_id, name, team, nationality, SUM(t2.total_time + ('. $levels->count() .'- t2.finished_levels) *' . $unfinishedTimePenalty . ') as total_time')
            ->from(\DB::raw('(' . $subQuery2->toSql() . ') as t2'))
            ->mergeBindings($subQuery2)
            ->join('kuskis', 'kuskis.id', 't2.kuski_id')
            ->groupBy('kuski_id')
            ->orderBy('total_time', 'asc');

        if ($limit) {
            $totals->limit($limit);
        }

        return $totals->get();
    }

    /**
     * Get world record list for this pack
     *
     * @return \Illuminate\Support\Collection
     */
    public function getWorldRecords()
    {
        $wrs = $this->levels()->with(['worldRecord' => function($q) {
            $q->where('wrs.pack_id', $this->id);
        }])->get()->map(function($level) {
            $wr = $level->worldRecord;

            if ($wr) {
                return $wr;
            }
            // There are no finishes for the level, create a "null" Time and set the level relation manually
            $wr = new Time;
            $wr->setRelation('level', $level);

            return $wr;
        });

        return $wrs;
    }

    /**
     * Get world record total time for this pack
     *
     * @param bool $formatted
     * @return \Illuminate\Support\Collection
     */
    public function getWorldRecordsTotalTime($formatted = true)
    {
        $wrtt = $this->getWorldRecords()->reduce(function($carry, $time) {
            $time = $time->time === null ? config('promo.unfinished_time_penalty') : $time->timeMs;

            return $carry + $time;
        });

        return $formatted ? mstostrtime($wrtt) : $wrtt;
    }

    /**
     * Get personal record list for a kuski for this pack
     *
     * @param Kuski $kuski
     * @param DateRange $drivenDuring
     * @return \Illuminate\Support\Collection
     */
    public function getPersonalRecordsForKuski(Kuski $kuski, DateRange $drivenDuring = null)
    {
        $prs = $this->levels->map(function($level) use($kuski, $drivenDuring) {
            $pr = $level->personalRecordForKuski($kuski, $this, $drivenDuring);

            if ($pr) {
                return $pr;
            }
            // There are no finishes for the level, create a "null" Time and set the relations manually
            $pr = new Time;
            $pr->setRelation('level', $level);
            $pr->setRelation('kuski', $kuski);

            return $pr;
        });

        return $prs;
    }

    /**
     * Get finish counts for this pack
     *
     * @param int $n
     * @param array $with
     * @param string $kuskiLike
     * @todo change to flatten collection and calculated on sql side?
     * @return mixed
     */
    public function finishCounts($n = 0, $with = ['kuski'], $dateRange = null, $kuskiLike = '')
    {
        $finishCountsForPack = $this->levels
            ->map(function($level) use($n, $with, $dateRange, $kuskiLike) {
                return $level->finishCounts($n, $with, $this, $dateRange, $kuskiLike);
            })
            ->map(function($finishCounts) {
                return $finishCounts->map(function($time) {
                    return collect([
                        'kuski_id' => $time->kuski->id,
                        'kuski' => $time->kuski->name,
                        'team' => $time->kuski->teamFormatted,
                        'nationality' => $time->kuski->nationality,
                        'finish_count' => $time->finish_count,
                    ]);
                });
            });

        return $finishCountsForPack
            ->cumulativeCount('kuski_id', 'finish_count')
            ->sortByDesc('finish_count');
    }

    /**
     * Get recent activity for this pack
     *
     * @param int $n
     * @param array $with
     * @param dateRange $dateRange
     * @param string $kuskiLike
     * @return mixed
     * @todo get rid of pr improvements that are wr improvements
     */
    public function recentActivity($n = 0, $with = ['kuski', 'level'], $dateRange = null, $kuskiLike = '')
    {
        $prImprovements = $this->prImprovementsQuery()
            ->selectRaw('times.id as time_id, times.kuski_id, times.level_id, times.time, t.time as old_time, times.driven_at, \'pr\' as activity_type')
            ->leftJoin('times as t', 'pr_improvements.old_time_id', '=', 't.id');

        if ($dateRange) {
            $prImprovements->drivenDuring($dateRange);
        }

        if ($kuskiLike) {
            $prImprovements->whereHas('kuski', function($query) use ($kuskiLike) {
                $query->where('name', 'like', "%{$kuskiLike}%");
            });
        }

        $wrImprovements = $this->wrImprovementsQuery()
            ->selectRaw('times.id as time_id, times.kuski_id, times.level_id, times.time, t2.time as old_time, times.driven_at, \'wr\' as activity_type')
            ->leftJoin('times as t2', 'wr_improvements.old_time_id', '=', 't2.id')
            ->unionAll($prImprovements)
            ->withoutGlobalScopes()
            ->orderBy('time_id', 'desc')
            ->with($with);

        if ($dateRange) {
            $wrImprovements
                ->where('times.driven_at', '<', $dateRange->end())
                ->where('times.driven_at', '>', $dateRange->start());
        }

        if ($kuskiLike) {
            $wrImprovements->whereHas('kuski', function($query) use ($kuskiLike) {
                $query->where('name', 'like', "%{$kuskiLike}%");
            });
        }

        if ($n) {
            $wrImprovements->limit($n);
        }

        return $wrImprovements->get();
    }

    /**
     * Get the total time leader for this pack
     *
     * @return mixed
     */
    public function totalTimesLeader()
    {
        return $this->totalTimes()->orderBy('total_time', 'asc')->first();
    }

    /**
     * Get an array of kuski_id => wr_count pairs
     *
     * @return type
     */
    public function wrCounts()
    {
        $wrs = $this->getWorldRecords()->filter(function($time) {
            return $time->id;
        });

        $wrCounts = $wrs->reduce(function($carry, $time) {
            if ($carry->has($time->kuski_id)) {
                return $carry->put($time->kuski_id, $carry->get($time->kuski_id) + 1);
            }

            return $carry->put($time->kuski_id, 1);
        }, collect());

        $wrCounts = $wrCounts->sort();

        return $wrCounts;
    }

    /**
     * Get the kuski that has most wrs for this pack as an array [$kuski, $wrCount]
     *
     * @return array
     */
    public function worldRecordsLeader()
    {
        $wrCounts = $this->wrCounts();
        $kuskiId = $wrCounts->keys()->last();
        $wrCount = $wrCounts->last();
        $kuski = Kuski::find($kuskiId);
        $kuski->wrCount = $wrCount;

        return $kuski;
    }

    /**
     * Is given kuski banned to this pack
     *
     * @param $kuskiId
     * @return bool
     */
    public function isKuskiBanned($kuskiId)
    {
        return $this->localKuskiBans->filter(function($ban) use($kuskiId) {
            return $ban->kuski_id == $kuskiId;
        })->count() > 0;
    }

    /**
     * Get an array of valid update frequencies
     *
     * @return array
     */
    public function getUpdateFrequencies()
    {
        return [
            self::UPDATE_FREQUENCY_DAILY => 'Daily',
            self::UPDATE_FREQUENCY_WEEKLY => 'Weekly',
            self::UPDATE_FREQUENCY_MONTHLY => 'Monthly',
        ];
    }

    /**
     * Add a level to the pack that already exists in the database
     *
     * @todo rename
     * @param Level $level
     */
    public function addLevel(Level $level)
    {
        $highestPosition = LevelPack::where('pack_id', $this->id)->max('position');
        $contains = $this->fresh()->levels->contains(function($levelInPack, $key) use($level) {
            return $levelInPack->id == $level->id;
        });
        if (! $contains) {
            $this->levels()->attach($level, ['position' => $highestPosition + 1]);

            return true;
        }

        return false;
    }

    /**
     * Get the pr for a kuski in a level of this pack
     *
     * @param Kuski $kuski
     * @param Level $level
     * @return mixed
     */
    public function prForKuskiForLevel(Kuski $kuski, Level $level)
    {
        return Time::where('times.level_id', $level->id)
            ->where('times.kuski_id', $kuski->id)
            ->filterByPack($this)
            ->orderBy('times.time', 'asc')
            ->limit(1)
            ->first(['times.*']);
    }

    /**
     * Get the wr for a level of this pack
     *
     * @param Level $level
     * @return mixed
     */
    public function wrForLevel(Level $level)
    {
        return Time::where('times.level_id', $level->id)
            ->filterByPack($this)
            ->orderBy('times.time', 'asc')
            ->limit(1)
            ->first(['times.*']);
    }

    /**
     * Get wr improvements for this pack
     *
     * @param Level $level
     * @return Collection
     */
    public function wrImprovements(Level $level = null, $orderBy = [])
    {
        $wrImprovements = $this->baseWrImprovements(false);
        $wrImprovements->addSelect('old_time_id');

        if ($level) {
            $wrImprovements->where('times.level_id', $level->id);
        }

        if (count($orderBy)) {
            $wrImprovements->withoutGlobalScopes();
        }

        foreach ($orderBy as $col) {
            $wrImprovements->orderBy($col);
        }

        return $wrImprovements->get();
    }

    /**
     * Get wr improvements for this pack with wr tt included
     *
     * @return Collection
     */
    public function wrImprovementsWithTotalTime($orderBy = [])
    {
        $wrImprovements = $this->wrImprovements(null, $orderBy);

        $best = collect();
        foreach ($this->levels->pluck('id') as $levId) {
            $best->put($levId, config('promo.unfinished_time_penalty'));
        }

        foreach ($wrImprovements->sortBy('id') as $wrImprovement) {
            $best->put($wrImprovement->level_id, $wrImprovement->timeMs);
            $wrImprovement->wrtt = $best->reduce(function($carry, $time) {
                return $carry + $time;
            });
        }

        return $wrImprovements;
    }

    /**
     * Get a subquery for every pr in this pack
     *
     * @param DateRange $dateRange
     * @param string $kuskiLike
     * @return mixed
     */
    protected function getBestTimesPerKuskiPerLevelQuery(DateRange $dateRange = null, $kuskiLike = '')
    {
        if ($dateRange) {
            $query = $this->prImprovementsQuery($dateRange, $kuskiLike);

            return $query
                ->select(\DB::raw('MIN(time) as time, times.level_id, times.kuski_id'))
                ->groupBy('level_id')
                ->groupBy('kuski_id')
                ->getQuery();
        } else {
            $query = $this->prsQuery($dateRange, $kuskiLike);

            return $query
                ->select('times.kuski_id', 'times.level_id', 'times.time')
                ->getQuery();
        }
    }

    /**
     * Get a subquery for every kuski's pr in this pack
     *
     * @param int $kuskiId
     * @param DateRange $dateRange
     * @return mixed
     */
    protected function getBestTimesForKuskiPerLevelQuery($kuskiId, DateRange $dateRange = null)
    {
        if ($dateRange) {
            $query = $this->prImprovementsQuery($dateRange);

            return $query
                ->where('kuski_id', $kuskiId)
                ->select(\DB::raw('MIN(time) as time, times.level_id, times.kuski_id'))
                ->groupBy('level_id')
                ->getQuery();
        } else {
            $query = $this->prsQuery($dateRange);

            return $query
                ->where('kuski_id', $kuskiId)
                ->select('times.kuski_id', 'times.level_id', 'times.time')
                ->getQuery();
        }
    }

    /**
     * Get a subquery for best time of every level in this pack
     *
     * @param array $levels
     * @return mixed
     */
    protected function getBestTimePerLevelQuery($levels = [])
    {
        if (empty($levels)) {
            $levels = $this->levels()->orderBy('position')->pluck('id');
        }

        return Time::selectRaw('times.level_id, min(time) as wr')
            ->whereIn('times.level_id', $levels)
            ->filterByPack($this)
            ->groupBy('times.level_id')
            ->getQuery();
    }

    /**
     * Skeleton query for pr improvements
     *
     * @param DateRange $dateRange
     * @param string $kuskiLike
     * @return mixed
     */
    protected function prImprovementsQuery(DateRange $dateRange = null, $kuskiLike = '')
    {
        $query = Time::leftJoin('pr_improvements', 'pr_improvements.time_id', '=', 'times.id')
            ->whereNotNull('pr_improvements.time_id')
            ->where('pr_improvements.pack_id', $this->id)
            ->when($kuskiLike, function ($query) use ($kuskiLike) {
                return $query->whereHas('kuski', function($query) use ($kuskiLike) {
                    $query->where('name', 'like', "%{$kuskiLike}%");
                });
            });

        if ($dateRange) {
            $query->drivenDuring($dateRange);
        }

        return $query;
    }

    /**
     * Skeleton query for prs
     *
     * @param DateRange $dateRange
     * @param string $kuskiLike
     * @return mixed
     */
    protected function prsQuery(DateRange $dateRange = null, $kuskiLike = '')
    {
        $query = Time::leftJoin('prs', 'prs.time_id', '=', 'times.id')
            ->whereNotNull('prs.time_id')
            ->where('prs.pack_id', $this->id)
            ->when($kuskiLike, function ($query) use ($kuskiLike) {
                return $query->whereHas('kuski', function($query) use ($kuskiLike) {
                    $query->where('name', 'like', "%{$kuskiLike}%");
                });
            });

        if ($dateRange) {
            $query->drivenDuring($dateRange);
        }

        return $query;
    }

    /**
     * Skeleton query for wr improvements
     *
     * @param DateRange $dateRange
     * @return mixed
     */
    protected function wrImprovementsQuery(DateRange $dateRange = null)
    {
        $query = Time::leftJoin('wr_improvements', 'wr_improvements.time_id', '=', 'times.id')
            ->whereNotNull('wr_improvements.time_id')
            ->where('wr_improvements.pack_id', $this->id);

        if ($dateRange) {
            $query->drivenDuring($dateRange);
        }

        return $query;
    }

    /**
     * Skeleton query for wrs
     *
     * @param DateRange $dateRange
     * @return mixed
     */
    protected function wrsQuery(DateRange $dateRange = null)
    {
        $query = Time::leftJoin('wrs', 'wrs.time_id', '=', 'times.id')
            ->whereNotNull('wrs.time_id')
            ->where('wrs.pack_id', $this->id);

        if ($dateRange) {
            $query->drivenDuring($dateRange);
        }

        return $query;
    }
}
