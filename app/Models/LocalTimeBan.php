<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LocalTimeBan
 * @package App\Models
 */
class LocalTimeBan extends Model
{
    protected $fillable = ['pack_id', 'time_id', 'kuski_id', 'level_id'];

    /**
     * The pack where time is locally banned
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pack()
    {
        return $this->belongsTo(Pack::class);
    }

    /**
     * The time that is locally banned
     *
     * @return mixed
     */
    public function time()
    {
        return $this->belongsTo(Time::class)->withoutGlobalScope('notBanned');
    }

    /**
     * The kuski whose time is locally banned
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @todo Probably redundant
     */
    public function kuski()
    {
        return $this->belongsTo(Kuski::class);
    }

    /**
     * The level where time is locally banned
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @todo Probably redundant
     */
    public function level()
    {
        return $this->belongsTo(Level::class);
    }
}
