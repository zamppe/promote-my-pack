<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package App\Models
 */
class Role extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    public $guarded = ['id'];

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable(config('roles.table_names.roles'));
    }

    /**
     * This role can belong to many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(config('auth.providers.users.model'), config('roles.table_names.user_has_roles'));
    }

    /**
     * Find a role by its name.
     *
     * @param string $name
     * @return Role
     */
    public static function findByName($name)
    {
        $role = static::where('name', $name)->first();

        return $role;
    }
}
