<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Pr
 * @package App\Models
 */
class Pr extends Model
{
    public $timestamps = false;

    protected $fillable = ['pack_id', 'time_id'];
}
