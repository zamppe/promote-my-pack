<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nickname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * All the news written by this user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function news()
    {
        return $this->hasMany(News::class);
    }

    /**
     * All the packs this user created
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function packs()
    {
        return $this->hasMany(Pack::class);
    }

    /**
     * All the roles this user has
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(config('roles.models.role'), config('roles.table_names.user_has_roles'));
    }

    /**
     * Is this user the currently logged in user
     *
     * @return bool
     */
    public function isCurrentUser()
    {
        return user() && user()->id === $this->id;
    }

    /**
     * Is this user admin of a given pack
     *
     * @param Pack $pack
     * @return bool
     */
    public function isPackAdmin(Pack $pack)
    {
        if ($this->isAdmin()) {
            return true;
        }

        if ($pack->user_id === $this->id) {
            return true;
        }

        return false;
    }

    /**
     * Is this user a moderator
     *
     * @return bool
     */
    public function isModerator()
    {
        return $this->hasRole(config('roles.roles.moderator'));
    }

    /**
     * Is this user an admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->hasRole(config('roles.roles.admin'));
    }

    /**
     * Does this user have a given role
     *
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        if ($role instanceof Role)
        {
            return $this->roles->contains('id', $role->id);
        }

        return false;
    }

    /**
     * Remove a role from this user
     *
     * @param $role
     */
    public function removeRole($role)
    {
        // he he =)
        $roleModel = is_string($role) ? Role::findByName($role) : $role;
        $this->roles()->detach($roleModel);
    }

    /**
     * Assign a role to this user
     *
     * @param $role
     */
    public function assignRole($role)
    {
        if (is_string($role) && in_array($role, config('roles.roles'))) {
            $role = Role::findByName($role);
        }

        $this->roles()->save($role);
    }

    /**
     * Assign the user role to this user
     */
    public function assignUserRole()
    {
        $this->assignRole(config('roles.roles.default'));
    }

    /**
     * Assign the moderator role to this user
     */
    public function assignModeratorRole()
    {
        $this->assignRole(config('roles.roles.moderator'));
    }

    /**
     * Assign the admin role to this user
     */
    public function assignAdminRole()
    {
        $this->assignRole(config('roles.roles.admin'));
    }
}
