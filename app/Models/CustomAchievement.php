<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

/**
 * Class CustomAchievement
 * @package App\Models
 */
class CustomAchievement extends Model
{
    use Mediable;

    protected $fillable = [
        'pack_id',
        'description',
        'name'
    ];

    /**
     * The pack this achievement belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pack()
    {
        return $this->belongsTo(Pack::class);
    }

    /**
     * The achieves for this achievement
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function achieves()
    {
        return $this->morphMany('App\Models\Achieve', 'achievable');
    }
}
