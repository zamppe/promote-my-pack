<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Wr
 * @package App\Models
 */
class Wr extends Model
{
    public $timestamps = false;

    protected $fillable = ['pack_id', 'time_id'];
}
