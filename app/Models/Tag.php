<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tag
 * @package App\Models
 */
class Tag extends Model
{
    protected $fillable = [
        'id',
        'name',
    ];

    /**
     * This tag can be used by many levels
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function levels()
    {
        return $this->morphedByMany(Level::class, 'taggable');
    }
}
