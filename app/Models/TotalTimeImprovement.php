<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TotalTimeImprovement extends Model
{
    public $timestamps = false;

    protected $fillable = ['pack_id', 'time_id', 'old_time_id', 'total_time'];
}
