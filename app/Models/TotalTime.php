<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TotalTime extends Model
{
    public $timestamps = false;

    protected $fillable = ['pack_id', 'kuski_id', 'total_time'];

    /**
     * The pack this total time belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pack()
    {
        return $this->belongsTo(Pack::class);
    }

    /**
     * The kuski this total time belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kuski()
    {
        return $this->belongsTo(Kuski::class);
    }
}
