<?php

namespace App\Models;

use App\Models\Traits\HasActivity;
use App\Utils\DateRange;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;
use Cache;

/**
 * Class Level
 * @package App\Models
 */
class Level extends Model
{
    use Mediable, HasActivity;

    protected $fillable = ['id', 'name', 'filename', 'killers', 'apples', 'flowers'];

    /*
     * Store a new level record from a row fetched from EOL api
     *
     * @param $id
     * @param $obj
     * @return Level
     */
    public static function createFromEolApiRow($id, $obj)
    {
        if ($level = self::find($id)) {
            return $level;
        }

        return Level::create([
            'id' => $id,
            'filename' => $obj->LevelName,
            'name' => $obj->LongName,
            'apples' => $obj->Apples,
            'killers' => $obj->Killers,
            'flowers' => $obj->Flowers,
        ]);
    }

    /**
     * All packs this level is in
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function packs()
    {
        return $this->belongsToMany(Pack::class);
    }

    /**
     * All times this level has
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function times()
    {
        return $this->hasMany(Time::class);
    }

    /**
     * World record of this level
     *
     * @return mixed
     */
    public function worldRecord()
    {
        return $this->hasOne(Time::class)
            ->select(['times.*'])
            ->leftJoin('wrs', 'wrs.time_id', '=', 'times.id')
            ->whereNotNull('wrs.time_id');
    }

    /**
     * World record of this level filtered by pack
     *
     * @return mixed
     */
    public function worldRecordForPack(Pack $pack)
    {
        return $this->worldRecord()
            ->where('wrs.pack_id', $pack->id)
            ->first();
    }

    /**
     * Get top n for this level, optionally filtered by pack (time for a level might be banned in one pack but not in another)
     *
     * @param int $n
     * @param array $with
     * @param Pack $pack
     * @return Collection
     */
    public function top($n = 0, $with = ['kuski'], Pack $pack = null)
    {
        $prs = $this->prs(false)
            ->with($with);

        if ($pack) {
            $prs->where('prs.pack_id', $pack->id);
        }

        if ($n) {
            $prs->limit($n);
        }

        return $prs->get();
    }

    /**
     * Get top n for a kuski for this level
     *
     * @param Kuski $kuski
     * @param Pack $pack
     * @param int $n
     * @param DateRange $dateRange
     * @param array $with
     * @return Collection
     */
    public function topForKuski(Kuski $kuski, Pack $pack = null, $n = 10, DateRange $dateRange = null, $with = ['kuski', 'level'])
    {
        $times = $this->times()
            ->where('times.kuski_id', $kuski->id)
            ->with($with);

        if ($pack) {
            $times->filterByPack($pack);
            $times->addSelect('times.*');
        }

        if ($dateRange) {
            $times->drivenDuring($dateRange);
        }

        if ($n) {
            $times->limit($n);
        }

        return $times->get();
    }

    /**
     * Get personal record for kuski for this level
     *
     * @param Kuski $kuski
     * @param Pack $pack
     * @param DateRange $dateRange
     * @return Time
     */
    public function personalRecordForKuski(Kuski $kuski, Pack $pack = null, DateRange $dateRange = null)
    {
        return $this->topForKuski($kuski, $pack, 1, $dateRange)->first();
    }

    /**
     * Get finish counts for this level, optionally filtered by pack (time for a level might be banned in one pack but not in another)
     *
     * @param int $n
     * @param array $with
     * @param Pack $pack
     * @param DateRange $dateRange
     * @param string $kuskiLike
     * @return mixed
     */
    public function finishCounts($n = 0, $with = ['kuski'], Pack $pack = null, DateRange $dateRange = null, $kuskiLike = '')
    {
        $finishCounts = $this->times()
            ->with($with)
            ->groupBy('times.kuski_id')
            ->orderBy('finish_count','desc')
            ->selectRaw('times.*, count(times.kuski_id) as finish_count')
            ->notGlobalBanned();

        if ($dateRange) {
            $finishCounts->drivenDuring($dateRange);
        }

        if ($pack) {
            $finishCounts->filterByPack($pack);
        }

        if ($kuskiLike) {
            $finishCounts->whereHas('kuski', function($q) use ($kuskiLike) {
                $q->where('name', 'like', "%{$kuskiLike}%");
            });
        }

        if ($n) {
            $finishCounts->limit($n);
        }

        return $finishCounts->get();
    }

    /**
     * Get recent activity for this level, optionally filtered by pack (time for a level might be banned in one pack but not in another)
     *
     * @param int $n
     * @param array $with
     * @param Pack $pack
     * @param DateRange $dateRange
     * @param string $kuskiLike
     * @return mixed
     * @todo get rid of pr improvements that are wr improvements
     */
    public function recentActivity($n = 0, $with = ['kuski'], Pack $pack = null, DateRange $dateRange = null, $kuskiLike = '')
    {
        $prImprovements = Time::where('times.level_id', $this->id)
            ->selectRaw('times.id as time_id, times.kuski_id, times.time, t.time as old_time, times.driven_at, \'pr\' as activity_type')
            ->leftJoin('pr_improvements', 'pr_improvements.time_id', '=', 'times.id')
            ->whereNotNull('pr_improvements.time_id')
            ->leftJoin('times as t', 'pr_improvements.old_time_id', '=', 't.id');

        if ($dateRange) {
            $prImprovements
                ->where('times.driven_at', '<', $dateRange->end())
                ->where('times.driven_at', '>', $dateRange->start());
        }

        if ($kuskiLike) {
            $prImprovements->whereHas('kuski', function($query) use ($kuskiLike) {
                $query->where('name', 'like', "%{$kuskiLike}%");
            });
        }

        if ($pack) {
            $prImprovements->where('pr_improvements.pack_id', $pack->id);
        }

        $wrImprovements = Time::where('times.level_id', $this->id)
            ->selectRaw('times.id as time_id, times.kuski_id, times.time, t2.time as old_time, times.driven_at, \'wr\' as activity_type')
            ->leftJoin('wr_improvements', 'wr_improvements.time_id', '=', 'times.id')
            ->whereNotNull('wr_improvements.time_id')
            ->leftJoin('times as t2', 'wr_improvements.old_time_id', '=', 't2.id');

        if ($dateRange) {
            $wrImprovements
                ->where('times.driven_at', '<', $dateRange->end())
                ->where('times.driven_at', '>', $dateRange->start());
        }

        if ($kuskiLike) {
            $wrImprovements->whereHas('kuski', function($q) use ($kuskiLike) {
                $q->where('name', 'like', "%{$kuskiLike}%");
            });
        }

        $wrImprovements->unionAll($prImprovements)
            ->withoutGlobalScopes()
            ->orderBy('time_id', 'desc')
            ->with($with);

        if ($pack) {
            $wrImprovements->where('wr_improvements.pack_id', $pack->id);
        }

        if ($n) {
            $wrImprovements->limit($n);
        }

        return $wrImprovements->get();
    }

    /**
     * Tags of this level
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Find the highest EOL time index of this level
     *
     * @param DateRange|null $dateRange
     * @return int
     */
    public function getHighestTimeIndex(DateRange $dateRange = null)
    {
        $times = $this->times();

        if ($dateRange) {
            $times->drivenDuring($dateRange);
        }

        $index = $times->max('times.id');

        return $index == null ? 1 : $index;
    }

    /**
     * Accessor for getting a Carbon datetime when times for this level were last updated
     *
     * @return \Carbon\Carbon
     */
    public function getTimesUpdatedAtAttribute()
    {
        return new \Carbon\Carbon($this->attributes['times_updated_at']);
    }

    /**
     * Accessor for getting the number of finished times in this level
     *
     * @return mixed
     */
    public function getFinishedTimesAttribute()
    {
        return $this->times()->withoutGlobalScope('order')->count();
    }

    /**
     * Accessor for getting all the kuskis that have at least one time in this level
     *
     * @return mixed
     * @todo return Kuski models
     */
    public function getKuskisAttribute()
    {
        return $this->times()->select('times.kuski_id')->distinct()->get();
    }

    /**
     * Accessor for getting all the kuskis whose data are still missing in this level
     *
     * @return mixed
     * @todo return Kuski models
     */
    public function getMissingKuskisAttribute()
    {
        return $this->times()
            ->select('times.kuski_id')
            ->join('kuskis', 'times.kuski_id', '=', 'kuskis.id')
            ->groupBy('kuski_id')
            ->whereNull('name')
            ->get();
    }

    /**
     * Get all the nationalities that are involved in this level
     *
     * @return mixed
     */
    public function getInvolvedNationalities()
    {
        return $this->times()
            ->select('nationality')
            ->join('kuskis', 'times.kuski_id', '=', 'kuskis.id')
            ->groupBy('nationality')
            ->pluck('nationality');
    }

    /**
     * Get all the teams that are involved in this level
     *
     * @return mixed
     */
    public function getInvolvedTeams()
    {
        return $this->times()
            ->select('times.team')
            ->join('kuskis', 'times.kuski_id', '=', 'kuskis.id')
            ->groupBy('team')
            ->pluck('team');
    }

    /**
     * Toggle updating status
     *
     * @deprecated
     */
    public function toggleUpdating()
    {
        $this->updating = !$this->updating;
        $this->save();
    }

    /**
     * Get the highest stored level id
     *
     * @return int
     */
    public static function getHighestLevelIndex()
    {
        $index = self::max('id');

        return $index == null ? 1 : $index;
    }

    /**
     * Checks if there is attached level media (.lev file) for this level yet
     *
     * @return bool
     */
    public function hasLevelData()
    {
        return ! empty($this->getMedia('level')->first());
    }

    /**
     * Accessor for getting the full name of the level including both filename and level name
     * @return type
     */
    public function getFullNameAttribute()
    {
        return collect([$this->attributes['filename'], $this->attributes['name']])->implode(', ');
    }
}
