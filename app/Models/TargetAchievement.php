<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Plank\Mediable\Mediable;

/**
 * Class TargetAchievement
 * @package App\Models
 */
class TargetAchievement extends Model
{
    use Mediable;

    protected $fillable = [
        'pack_id',
        'levels',
        'description',
        'apply_on',
        'comparator',
        'consider',
        'target_type',
        'target_value_type',
        'target_value',
        'name'
    ];

    const COMPARATOR_LT = 1;
    const COMPARATOR_EQ = 2;

    const CONSIDER_BEST = 1;
    const CONSIDER_ALL = 2;

    const TARGET_TYPE_TIME = 1;
    const TARGET_TYPE_TOTALTIME = 2;

    const TARGET_VALUE_TYPE_TIME = 1;
    const TARGET_VALUE_TYPE_POSITION = 2;

    const TARGET_VALUE_ANY_FINISH = 0;
    const APPLY_ON_ALL_LEVELS = 0;

    /* @todo remove */
    public static $comparisonTypeUnder = 1;
    public static $comparisonTypeExact = 2;

    public static $finishTypeBest = 1;
    public static $finishTypeAll = 2;

    public static $targetTypeTime = 1;
    public static $targetTypeTotalTime = 2;

    public static $targetValueTypeTime = 1;
    public static $targetValueTypePosition = 2;

    /**
     * This achievement belongs to a pack
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pack()
    {
        return $this->belongsTo(Pack::class);
    }

    /**
     * This achievement can have many achieves
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function achieves()
    {
        return $this->morphMany('App\Models\Achieve', 'achievable');
    }

    /**
     * Mutator for storing target value field
     *
     * @param type $value
     */
    public function setTargetValueAttribute($value)
    {
        if ($value != '0' && $this->isTargetValueTypeTime()) {
            $value = strtimetoms($value);
        }

        $this->attributes['target_value'] = $value;
    }

    /**
     * Mutator for storing levels field as a json
     *
     * @param $value
     */
    public function setLevelsAttribute($value)
    {
        if ($this->isTotalTimeType()) {
            $this->setLevelsFromPack($this->pack);

            return;
        }

        if ($value instanceof Collection) {
            $value = $value->toArray();
        }

        if (! is_array($value)) {
            return;
        }

        // empty array or [5326, 53426, 7453] format
        if(empty($value) || array_keys($value)[0] == 0) {
            $levelIds = json_encode($value);
        // [54326 => 1, 63267 => 0] format (checkbox input)
        } else {
            $levelIds = collect($value);
            $levelIds = $levelIds->filter(function($state, $levelId) {
                return $state == 1;
            })->keys()->toJson();
        }

        $this->attributes['levels'] = $levelIds;
    }

    /**
     * Accessor for getting levels field as an array
     *
     * @param $value
     * @return mixed
     */
    public function getLevelsAttribute($value)
    {
        if ($value == "") {
            $value = "[]";
        }

        return json_decode($value);
    }

    /**
     * Get levels as models
     *
     * @return mixed
     */
    public function getLevelModels()
    {
        if ($this->isAppliedToAll() && empty($this->levels)) {
            return $this->pack->levels;
        }

        return $this->pack->levels()->whereIn('id', $this->levels)->get();
    }

    /**
     * Accessor for getting the target value as a formatted string
     *
     * @return string
     */
    public function getTargetFormattedAttribute()
    {
        $value = $this->attributes['target_value'];
        if ($this->target_value != 0 && $this->isTargetValueTypeTime()) {
            return mstostrtime($value);
        }

        return $value;
    }

    /**
     * Accessor for getting the target value as a raw value
     *
     * @return mixed
     */
    public function getTargetTimeMsAttribute()
    {
        return $this->attributes['target_value'];
    }

    /**
     * Get a computed description of this achievement
     *
     * @return string
     */
    public function getComputedDescriptionAttribute()
    {
        if ($this->isTotalTimeType()) {
            if ($this->isTargetValueTypePosition()) {
                if ($this->isComparisonTypeExact()) {
                    return "Get position {$this->target_value} on totaltimes.";
                } else if ($this->isComparisonTypeUnder()) {
                    $top = $this->target_value - 1;
                    return "Get to top {$top} on totaltimes.";
                }
            } else if ($this->isTargetValueTypeTime()) {
                $time = mstostrtime($this->target_value);
                if ($this->isComparisonTypeExact()) {
                    return "Get {$time} total time";
                } else if ($this->isComparisonTypeUnder()) {
                    return "Get total time under {$time}";
                }
            }
        }

        else if ($this->isTimeType()) {
            $description = "";
            $levsString = "";
            $levs = $this->getLevelModels();
            if ($levs->count()) {
                $levsString = $levs->pluck('filename')->implode(', ');
            }

            if ($this->isFinishAnyTime()) {
                $description = $this->isAppliedToAll() ? "Finish every one of these levels:" : "Finish {$this->apply_on} of these levels:";

                return "{$description} {$levsString}";
            }
            else if ($this->isTargetValueTypePosition()) {
                $description .= "Get ";
                if ($this->isComparisonTypeExact()) {
                    $description .= "position {$this->target_value} ";
                } else if ($this->isComparisonTypeUnder()) {
                    $top = $this->target_value - 1;
                    $description .= "in top {$top} ";
                }
            } else if ($this->isTargetValueTypeTime()) {
                $description .= "Finish ";
                $time = mstostrtime($this->target_value);
                if ($this->isComparisonTypeExact()) {
                    $description .= "{$time} ";
                } else if ($this->isComparisonTypeUnder()) {
                    $description .= "under {$time} ";
                }
            }

            if ($this->apply_on == 0) {
                $description .= "on each one of these levels:";
            }

            else {
                $description .= "on at least {$this->apply_on} of these levels: ";
            }
        }

        return "{$description} {$levsString}";
    }

    /**
     * Populate levels from all pack levels
     *
     * @param Pack
     */
    public function setLevelsFromPack(Pack $pack)
    {
        $this->levels = $pack->levels()->pluck('id');
        $this->save();
    }

    /**
     * Is this achievement about total time?
     *
     * @todo this can probably be separated from this model ?
     */
    public function isTotalTimeType()
    {
        return $this->target_type == self::TARGET_TYPE_TOTALTIME;
    }

    /**
     * Is this achievement about finishes/times?
     */
    public function isTimeType()
    {
        return $this->target_type == self::TARGET_TYPE_TIME;
    }

    /**
     * Is the target value an elma time?
     */
    public function isTargetValueTypeTime()
    {
        return $this->target_value_type == self::TARGET_VALUE_TYPE_TIME;
    }

    /**
     * Is the target value a position?
     */
    public function isTargetValueTypePosition()
    {
        return $this->target_value_type == self::TARGET_VALUE_TYPE_POSITION;
    }

    /**
     * Must the best result be less than target value?
     */
    public function isComparisonTypeUnder()
    {
        return $this->comparator == self::COMPARATOR_LT;
    }

    /**
     * Must the best result be exactly the target value?
     */
    public function isComparisonTypeExact()
    {
        return $this->comparator == self::COMPARATOR_EQ;
    }

    /**
     * Is only best times considered? Best finishes per kuski, not all finishes
     */
    public function isConsiderBest()
    {
        return $this->consider == self::CONSIDER_BEST;
    }

    /**
     * Is all finishes considered?
     */
    public function isConsiderAll()
    {
        return $this->consider == self::CONSIDER_ALL;
    }

    /**
     *  Does this achievement require just any finish
     */
    public function isFinishAnyTime()
    {
        return $this->target_value == self::TARGET_VALUE_ANY_FINISH;
    }

    /**
     *  Is this achievement applied to all levels of the pack?
     */
    public function isAppliedToAll()
    {
        return $this->apply_on == self::APPLY_ON_ALL_LEVELS;
    }
}
