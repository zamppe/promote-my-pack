<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Support\Collect

/**
 * Class Sortable
 * @package App\Models\Traits
 */
trait Sortable
{
    /**
     * Get sortable items
     *
     * @param BelongsToMany|null $relation
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getItems(BelongsToMany $relation = null)
    {
        $positionColumn = config('models.position_column');

        if (! $relation) {
            $items = self::orderBy($positionColumn, 'asc')->get();
        } else {
            $relation->withPivot($positionColumn)->orderBy($positionColumn, 'asc');
            $items = $relation->get();
        }

        return $items;
    }

    /**
     * Find the index of an item inside a collection based on the item's id
     *
     * @param $collection
     * @param $findItem
     * @return int|string
     */
    private function findKeyById($collection, $findItem)
    {
        foreach($collection as $key => $item) {
            if ($findItem->id === $item->id) {
                return $key;
            }
        }
    }

    /**
     * Move an item up by one step
     *
     * @param BelongsToMany|null $relation
     * @param Model|null $item
     */
    public function moveUpByOne(BelongsToMany $relation = null, Model $item = null)
    {
        $positionColumn = config('models.position_column');

        if ($relation && $item) {
            $items = $this->getItems($relation);
            $key = $this->findKeyById($items, $item);
            $items = $items->splice($key - 1, 2);
            $relation->updateExistingPivot($items->first()->id, [$positionColumn => $items->last()->pivot->position]);
            $relation->updateExistingPivot($items->last()->id, [$positionColumn => $items->first()->pivot->position]);
        } else {
            // todo for model itself
        }
    }

    /**
     * Move an item down by one step
     *
     * @param BelongsToMany|null $relation
     * @param Model|null $item
     */
    public function moveDownByOne(BelongsToMany $relation = null, Model $item = null)
    {
        $positionColumn = config('models.position_column');

        if ($relation && $item) {
            $items = $this->getItems($relation);
            $key = $this->findKeyById($items, $item);
            $items = $items->splice($key, 2);
            $relation->updateExistingPivot($items->first()->id, [$positionColumn => $items->last()->pivot->position]);
            $relation->updateExistingPivot($items->last()->id, [$positionColumn => $items->first()->pivot->position]);
        } else {
            // todo for model itself
        }
    }

    /**
     * Sort items in alphabetical order
     *
     * @param BelongsToMany|null $relation
     * @param string $field
     * @param bool $asc
     */
    public function sortAlphabetical(BelongsToMany $relation, $field, $asc = true)
    {
        $positionColumn = config('models.position_column');
        $items = $this->getItems($relation);
        $items = $asc ? $items->sortBy($field) : $items->sortByDesc($field);
        $items->values()->each(function($item, $key) use ($relation, $positionColumn) {
            $relation->updateExistingPivot($item->id, ['position' => $key]);
        });
    }

    /**
     * Sort items in given array order
     * Items are sorted in same order as array for id matches that are found
     *
     * eg. if relation has items [id: 5, id: 10, id: 17] and $idArray contains [17, 5]
     * relation items are sorted in following order: [id: 17, id: 5, id: 10]
     *
     * @param BelongsToMany|null $relation
     * @param string $field
     * @param bool $asc
     */
    public function sortWithHintArray(BelongsToMany $relation, $idArray)
    {
        $positionColumn = config('models.position_column');

        // original items as [[id => id], [id => id]]
        $items = $this->getItems($relation)->map(function($item) {
            return $item->id;
        })->keyByValue();

        $wantedOrder = collect($idArray)
            ->keyByValue() // this is the wanted item order as [[id => id], [id => id]]
            ->union($items) // fill in missing id's from items (in case any missing from $idArray)
            ->values(); // convert into [[0 => id], [1 => id]]

        $wantedOrder->each(function($id, $order) use ($relation, $positionColumn) {
            $relation->updateExistingPivot($id, [$positionColumn => $order]);
        });
    }
}
