<?php

namespace App\Models\Traits;

use App\Models\Kuski;
use App\Models\Level;
use App\Models\Pack;
use App\Models\Time;
use App\Utils\DateRange;

/**
 * Class HasActivity
 * @package App\Models\Traits
 */
trait HasActivity
{
    protected $activityBuilders;
    /**
     * Prs
     *
     * @return mixed
     */
    public function prs($get = true, $applyClassConstraint = true)
    {
        $query = $this->getBaseQuery('prs', $applyClassConstraint);

        return $get ? $query->get() : $query;
    }

    /**
     * Wrs
     *
     * @return mixed
     */
    public function wrs($get = true, $applyClassConstraint = true)
    {
        $query = $this->getBaseQuery('wrs', $applyClassConstraint);

        return $get ? $query->get() : $query;
    }

    /**
     * Pr improvements. Including first finishes
     *
     * @return mixed
     */
    public function allPrImprovements($get = true, $applyClassConstraint = true)
    {
        $tableName = 'pr_improvements';

        $query = $this->getBaseQuery($tableName, $applyClassConstraint)
            ->addSelect('t.time as old_time')
            ->leftJoin('times as t', "{$tableName}.old_time_id", '=', 't.id');

        return $get ? $query->get() : $query;
    }

    /**
     * Pr improvements. Excluding first finishes
     *
     * @return mixed
     */
    public function prImprovements($get = true, $applyClassConstraint = true)
    {
        $tableName = 'pr_improvements';

        $query = $this->getBaseQuery($tableName, $applyClassConstraint)
            ->addSelect('t.time as old_time')
            ->selectRaw('\'pr\' as activity_type')
            ->join('times as t', "{$tableName}.old_time_id", '=', 't.id');

        return $get ? $query->get() : $query;
    }

    /**
     * First finishes
     *
     * @return mixed
     */
    public function firstFinishes($get = true, $applyClassConstraint = true)
    {
        $query = $this->allPrImprovements(false, $applyClassConstraint)
            ->selectRaw('\'ff\' as activity_type')
            ->whereNull('pr_improvements.old_time_id');

        return $get ? $query->get() : $query;
    }

    /**
     * Wr improvements
     *
     * @return mixed
     */
    public function wrImprovements($get = true, $applyClassConstraint = true)
    {
        $tableName = 'wr_improvements';

        $query = $this->getBaseQuery($tableName, $applyClassConstraint)
            ->addSelect('t.time as old_time')
            ->selectRaw('\'wr\' as activity_type')
            ->leftJoin('times as t', "{$tableName}.old_time_id", '=', 't.id');

        return $get ? $query->get() : $query;
    }

    /**
     * Total time improvements
     *
     * @return mixed
     */
    public function totalTimeImprovements($get = true, $applyClassConstraint = true)
    {
        $tableName = 'total_time_improvements';

        $query = $this->getBaseQuery($tableName, $applyClassConstraint)
            ->addSelect('t.time as old_time')
            ->addSelect("{$tableName}.total_time")
            ->leftJoin('times as t', "{$tableName}.old_time_id", '=', 't.id');

        return $get ? $query->get() : $query;
    }

    /**
     * Get the base query
     *
     * @param string $tableName
     * @return \Illuminate\Database\Eloquent\Builder &$query
     */
    private function getBaseQuery(string $tableName, $applyClassConstraint = true)
    {
        $query = Time::with(['kuski', 'level'])
            ->select(['times.*'])
            ->leftJoin($tableName, "{$tableName}.time_id", '=', 'times.id')
            ->whereNotNull("{$tableName}.time_id")
            ->setJoinTable($tableName);

        if ($applyClassConstraint) {
            $this->applyClassConstraint($query);
        }

        return $query;
    }

    /**
     * Apply constraint based on called class
     *
     * @param \Illuminate\Database\Eloquent\Builder &$query
     * @return void
     */
    private function applyClassConstraint(&$query)
    {
        $class = get_called_class();

        if ($class === Level::class) {
            $query->belongsToLevel($this);
        }

        if ($class === Pack::class) {
            $query->belongsToPack($this);

        }
    }
}
