<?php

namespace App\Models\Traits;

use Storage;
use Zipper;

/**
 * Class HandlesLevelsZips
 * @package App\Models\Traits
 */
trait HandlesLevelsZips
{
    /**
     * Delete any old levels zip files and generate a new one
     *
     * @return void
     */
    public function updateLevelsZip()
    {
        $this->trashOldZip();
        $this->generateLevelsZip();
    }

    /**
     * Get levels for this pack as a zip download response
     *
     * @return Illuminate\Http\Response
     */
    public function downloadLevels()
    {
        if (! Storage::disk('local')->exists($this->getLevelsZipFullStoragePath())) {
            $this->generateLevelsZip();
        }

        return response()->download($this->getLevelsZipFullPath());
    }

    /**
     * Generate a zip file that contains all levels
     *
     * @return void
     */
    protected function generateLevelsZip()
    {
        $fileNames = $this->levels->map(function($level) {
            return $level->firstMedia('level')->getAbsolutePath();
        })->toArray();

        $path = $this->getLevelsZipFullPath();

        Zipper::make($path)
            ->add($fileNames)
            ->close();
    }

    /**
     * Trash old levels zip
     *
     * @return type
     */
    protected function trashOldZip()
    {
        Storage::disk('local')->deleteDirectory($this->getLevelsZipStoragePath());
    }

    /**
     * Get storage path to levels zip
     *
     * @return string
     */
    protected function getLevelsZipStoragePath()
    {
        return join(DIRECTORY_SEPARATOR, [
            "packs",
            (string) $this->id,
            "levels",
        ]);
    }

    /**
     * Get full storage path to levels zip
     *
     * @return type
     */
    protected function getLevelsZipFullStoragePath()
    {
        return join(DIRECTORY_SEPARATOR, [
            $this->getLevelsZipStoragePath(),
            $this->getFileName()
        ]);
    }

    /**
     * Get filename of levels zip
     *
     * @return string
     */
    protected function getFileName()
    {
        return "{$this->name}.zip";
    }

    /**
     * Get full path to levels zip
     *
     * @return string
     */
    protected function getLevelsZipFullPath()
    {
        return storage_file_full_path($this->getLevelsZipFullStoragePath());
    }
}
