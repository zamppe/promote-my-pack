<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WrImprovement
 * @package App\Models
 */
class WrImprovement extends Model
{
    public $timestamps = false;

    protected $fillable = ['pack_id', 'time_id', 'old_time_id'];
}
