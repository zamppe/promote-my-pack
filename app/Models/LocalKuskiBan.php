<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LocalKuskiBan
 * @package App\Models
 */
class LocalKuskiBan extends Model
{
    protected $fillable = ['pack_id', 'kuski_id'];

    /**
     * The pack where kuski is locally banned
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pack() {
        return $this->belongsTo(Pack::class);
    }

    /**
     * The kuski that is locally banned
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kuski() {
        return $this->belongsTo(Kuski::class);
    }
}
