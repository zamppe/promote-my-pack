<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Achieve
 * @package App\Models
 */
class Achieve extends Model
{
    protected $fillable = [
        'id',
        'kuski_id',
        'time_id',
        'achieved_at',
    ];

    /**
     * The kuski this achieve belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kuski()
    {
        return $this->belongsTo(Kuski::class);
    }

    /**
     * The achievement this achieve is linked to
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function achievable()
    {
        return $this->morphTo();
    }
}
