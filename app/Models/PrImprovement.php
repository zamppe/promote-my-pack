<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PrImprovement
 * @package App\Models
 */
class PrImprovement extends Model
{
    public $timestamps = false;

    protected $fillable = ['pack_id', 'time_id', 'old_time_id'];

    /**
     * The pack this pr improvement belongs to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pack()
    {
        return $this->belongsTo(Pack::class);
    }

    /**
     * The time of this pr improvement
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function time()
    {
        return $this->belongsTo(Time::class);
    }
}
