<?php

namespace App\Models;

use App\Utils\DateRange;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Time
 * @package App\Models
 */
class Time extends Model
{
    protected $fillable = ['id', 'time', 'team', 'kuski_id', 'level_id', 'driven_at'];

    protected $dates = ['driven_at'];

    protected $extraData;

    public $timestamps = false;

    /**
     * Boot this model with order global scope
     * Order times query results by ascending time field and ascending id field by default
     */
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('times.time', 'asc')->orderBy('times.id', 'asc');
        });
    }

    /**
     * Store a new time record from a row fetched from EOL api
     *
     * @param type $obj
     * @param Level $level
     * @return Time
     */
    public static function createFromEolApiRow($obj, Level $level)
    {
        $drivenAt = Carbon::parse($obj->datetime, 'America/Vancouver');
        $drivenAt->setTimezone(config('app.timezone'));

        $time = Time::create([
            'id' => $obj->timeindex,
            'time' => strtimetoms($obj->time),
            'team' => $obj->team,
            'driven_at' => $drivenAt,
            'level_id' => $level->id,
            'kuski_id' => $obj->kuskiindex,
        ]);

        return $time;
    }

    /**
     * This time belongs to a level
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    /**
     * This time was driven by a kuski
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kuski()
    {
        return $this->belongsTo(Kuski::class);
    }

    /**
     * This time can be banned in many packs
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function localTimeBans()
    {
        return $this->hasMany(LocalTimeBan::class);
    }

    /**
     * This time can be globally banned
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function globalTimeBan()
    {
        return $this->hasOne(GlobalTimeBan::class);
    }

    /**
     * Scope for setting the join table
     *
     * @param $query
     * @param string $tableName
     */
    public function scopeSetJoinTable($query, string $tableName)
    {
        $query->joinTable = $tableName;
    }

    /**
     * Scope for constraining times to pack
     *
     * @param $query
     * @param Pack $pack
     */
    public function scopeBelongsToPack($query, Pack $pack)
    {
        if ($query->joinTable) {
            $query->where("{$this->joinTable}.pack_id", $pack->id);
        }
    }

    /**
     * Scope for constraining times to level
     *
     * @param $query
     * @param Level $level
     */
    public function scopeBelongsToLevel($query, Level $level)
    {
        $query->where('times.level_id', $level->id);
    }

    /**
     * Scope for constraining times to kuski
     *
     * @param $query
     * @param Kuski $kuski
     */
    public function scopeBelongsToKuski($query, Kuski $kuski)
    {
        $query->where('times.kuski_id', $kuski->id);
    }

    /**
     * Scope for checking is this time driven during the given date range
     *
     * @param $query
     * @param DateRange $range
     */
    public function scopeDrivenDuring($query, DateRange $range)
    {
        $query->where('times.driven_at', '>', $range->start());
        $query->where('times.driven_at', '<=', $range->end());
    }

    /**
     * Scope for checking is this time driven before the given date
     *
     * @param $query
     * @param Carbon $date
     */
    public function scopeDrivenBefore($query, Carbon $date)
    {
        $query->where('times.driven_at', '<=', $date);
    }

    /**
     * Scope for checking is this time driven after the given date
     *
     * @param $query
     * @param Carbon $date
     */
    public function scopeDrivenAfter($query, Carbon $date)
    {
        $query->where('times.driven_at', '>', $date);
    }

    /**
     * Scope for checking is this time globally banned
     *
     * @param $query
     */
    public function scopeNotGlobalBanned($query)
    {
        $query->leftJoin('global_time_bans', function($join) {
            $join->on('global_time_bans.time_id' , '=', 'times.id');
        });
        $query->whereNull('global_time_bans.time_id');
    }

    /**
     * Scope for considering only the best time per kuski
     * Turns the query into inner query, so it should be the last called scope to get expected behaviour
     *
     * @param $query
     * @todo doesn't work with "where in" so this can't be used with $pack->levels()->bestOnlyPerKuski, only $level->bestOnlyPerKuski
     */
    public function scopeBestOnlyPerKuski($query)
    {
        $query->selectRaw('min(times.time) as min_time, times.level_id, times.kuski_id')
            ->groupBy('times.kuski_id');
        $subQuery = $query->getQuery();

        $newQuery = self::select('times.*')
            ->from(\DB::raw('(' . $subQuery->toSql() . ') as t_inner'))
            ->mergeBindings($subQuery)
            ->join('times', function($join) {
                $join->on('t_inner.min_time', '=', 'times.time')
                     ->on('t_inner.level_id', '=', 'times.level_id')
                     ->on('t_inner.kuski_id', '=', 'times.kuski_id');
            })
            ->groupBy('times.kuski_id'); // removes shadow prs

        $query->setQuery($newQuery->getQuery());
    }

    /**
     * Scope for filtering query results by given pack
     * Include only the times that were driven before the given pack's deadline
     * Some times might be banned in the given pack
     *
     * @param $query
     * @param Pack $pack
     * @return mixed
     */
    public function scopeFilterByPack($query, Pack $pack)
    {
        if ($pack->deadline != null) {
            $query->drivenBefore($pack->deadline);
        }

        $query->leftJoin('local_time_bans', function($join) use ($pack) {
            $join->on('local_time_bans.time_id', '=', 'times.id')
                 ->where('local_time_bans.pack_id', '=', $pack->id);
        });
        $query->whereNull('local_time_bans.time_id');

        $query->leftJoin('local_kuski_bans', function($join) use ($pack) {
            $join->on('local_kuski_bans.kuski_id', '=', 'times.kuski_id')
                 ->where('local_kuski_bans.pack_id', '=', $pack->id);
        });
        $query->whereNull('local_kuski_bans.kuski_id');

        return $query;
    }

    /**
     * Scope for getting times driven before given pack's deadline
     *
     * @param $query
     * @param Pack $pack
     * @return mixed
     */
    public function scopeIsBeforeDeadline($query, Pack $pack)
    {
        return $query->where('driven_at', '<=', $pack->deadline);
    }

    /**
     * Is this time banned in given pack
     *
     * @param Pack $pack
     * @return bool
     */
    public function isBannedToPack(Pack $pack)
    {
        return $this->localTimeBans->filter(function($ban) use($pack) {
            return $ban->pack_id == $pack->id;
        })->count() > 0;
    }

    /**
     * Is this time globally banned
     *
     * @return bool
     */
    public function isGlobalBanned()
    {
        return $this->globalTimeBan != null;
    }

    /**
     * Accessor for getting the time field as a formatted string
     *
     * @param $value
     * @return string
     */
    public function getTimeAttribute($value)
    {
        if ($value) {
            return mstostrtime($value);
        }

        return $value;

    }

    /**
     * Accessor for getting the time field as a raw value
     *
     * @return mixed
     */
    public function getTimeMsAttribute()
    {
        if (! array_key_exists('time', $this->attributes)) {
            return config('promo.unfinished_time_penalty');
        }

        return $this->attributes['time'];
    }

    /**
     * Accessor for getting the old time field as a formatted string
     *
     * @param $value
     * @return string
     */
    public function getOldTimeAttribute($value)
    {
        return mstostrtime($value);
    }

    /**
     * Accessor for getting the old time field as raw
     *
     * @return string
     */
    public function getOldTimeMsAttribute()
    {
        return $this->attributes['old_time'];
    }

    /**
     * Set extra data for this time
     *
     * @param null $data
     * @todo look for alternative way to do this
     */
    public function setExtraData($data = null)
    {
        if (is_null($data)) {
            $data = collect();
        }


        $this->extraData = $data;
    }

    /**
     * Set extra data at key $key with value $value
     *
     * @param $key
     * @param $value
     */
    public function modifyExtraData($key, $value)
    {
        $this->extraData->put($key, $value);
    }

    /**
     * Get extra data for this time
     *
     * @param null $key
     * @return null
     * @todo look for alternative way to do this
     */
    public function getExtraData($key = null)
    {
        if (is_null($key)) {
            return $this->extraData;
        }

        if ($this->extraData->has($key)) {
            return $this->extraData->get($key);
        }

        return null;
    }
}
