<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @package App\Models
 */
class News extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'content',
        'newsable_type',
        'newsable_id',
    ];

    /**
     * Order news items by descending created_at field on default by using global scope
     */
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function newsable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Carbon\Carbon
     */
    public function getCreatedAtAttribute()
    {
        return new \Carbon\Carbon($this->attributes['created_at']);
    }
}
