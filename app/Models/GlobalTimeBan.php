<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class GlobalTimeBan
 * @package App\Models
 */
class GlobalTimeBan extends Model
{
    protected $fillable = ['time_id', 'kuski_id', 'level_id'];

    /**
     * The time that has been global banned
     *
     * @return mixed
     */
    public function time()
    {
        return $this->belongsTo(Time::class)->withoutGlobalScope('notBanned');
    }

    /**
     * The kuski that drove the globally banned time
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kuski()
    {
        return $this->belongsTo(Kuski::class);
    }

    /**
     * The level where the globally banned time was driven
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo(Level::class);
    }
}
