<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Kuski
 * @package App\Models
 */
class Kuski extends Model
{
    protected $fillable = ['id', 'name', 'team', 'nationality'];

    /**
     * All times of this kuski
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function times()
    {
        return $this->hasMany(Time::class);
    }

    /**
     * All achieves of this kuski
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function achieves()
    {
        return $this->hasMany(Achieve::class);
    }

    /**
     * All local bans of this kuski
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function localKuskiBans()
    {
        return $this->hasMany(LocalKuskiBan::class);
    }

    /**
     * All local time bans of this kuski
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function localTimeBans()
    {
        return $this->hasMany(LocalTimeBan::class);
    }

    /**
     * Query scope for filtering out results where kuski is banned to a pack
     *
     * @param $query
     * @param Pack $pack
     * @return mixed
     */
    public function scopeIsNotLocalBanned($query, Pack $pack)
    {
        return $query
            ->whereNotExists(function($q) use($pack) {
                $q->select(\DB::raw(1))
                ->from('local_kuski_bans')
                ->whereRaw('local_kuski_bans.kuski_id = kuskis.id AND local_kuski_bans.pack_id = '. $pack->id);
            });
    }

    /**
     * Get the best position to a pack level this kuski currently has
     * Use bestOnly = false if want to consider all times, not just prs
     * Use deadline to check position at certain date
     *
     * @param Pack $pack
     * @param Level $level
     * @param bool $bestOnly
     * @param Carbon $deadline
     * @param $limit
     * @return null
     */
    public function bestPositionToPackLevel(Pack $pack, Level $level, $bestOnly = true, Carbon $deadline = null, $limit = 0)
    {
        if ($bestOnly) {
            $query = $level->prImprovements(false);
        } else {
            $query = $level->times()
                           ->filterByPack($pack)
                           ->notGlobalBanned();
        }

        $query->select(['times.kuski_id']);

        if ($deadline) {
            $query->drivenBefore($deadline);
        }

        if ($limit) {
            $query->limit($limit);
        }

        $pos = 0;
        foreach($query->get() as $time) {
            $pos++;
            if ($time->kuski_id == $this->id) {
                $time->setExtraData(collect([
                    'best_result' => $pos,
                ]));

                return $time;
            }
        }

        return null;
    }

    /**
     * Get the best time this kuski has to a pack level
     * Use deadline to get best time at certain date
     *
     * @param Pack $pack
     * @param Level $level
     * @param Carbon $deadline
     * @return null
     */
    public function bestTimeToPackLevel(Pack $pack, Level $level, Carbon $deadline = null)
    {
        $query = $level->allPrImprovements(false)
            ->where('times.kuski_id', $this->id)
            ->filterByPack($pack)
            ->notGlobalBanned();

        if ($deadline) {
            $query->drivenBefore($deadline);
        }

        $bestTime = $query->first();

        if (is_null($bestTime)) {
            return null;
        }

        $bestTime->setExtraData(collect([
            'best_result' => $bestTime->timeMs,
        ]));

        return $bestTime;
    }

    /**
     * Check if kuski has a finish to a given level of given pack
     * @param Pack $pack
     * @param Level $level
     * @param Carbon|null $deadline
     * @return type
     */
    public function hasFinishToPackLevel(Pack $pack, Level $level, Carbon $deadline = null)
    {
        $query = $level->times()
            ->where('times.kuski_id', $this->id)
            ->filterByPack($pack)
            ->notGlobalBanned();

        if ($deadline) {
            $query->drivenBefore($deadline);
        }

        return $query->count() > 0;
    }

    /**
     * Check if kuski has given time to a given level of given pack
     *
     * @param Pack $pack
     * @param Level $level
     * @param string $timeMs
     * @param Carbon $deadline
     * @return bool
     */
    public function hasTimeToPackLevel(Pack $pack, Level $level, $timeMs, Carbon $deadline = null)
    {
        if ($timeMs == 0) {
            return $this->hasFinishToPackLevel($pack, $level, $deadline);
        }

        $query = $level->times()
            ->where('times.kuski_id', $this->id)
            ->where('times.time', $timeMs)
            ->filterByPack($pack)
            ->notGlobalBanned();

        if ($deadline) {
            $query->drivenBefore($deadline);
        }

        return $query->count() > 0;
    }

    /**
     * Check if kuski has given position to a given level of given pack at certain date
     *
     * @param Pack $pack
     * @param Level $level
     * @param string $timeMs
     * @param Carbon $deadline
     * @return type
     */
    public function hasPositionToPackLevel(Pack $pack, Level $level, $position, Carbon $deadline = null)
    {
        if ($position == 0) {
            return $this->hasFinishToPackLevel($pack, $level, $deadline);
        }

        $query = $level->times()
            ->select(['times.kuski_id'])
            ->filterByPack($pack)
            ->notGlobalBanned();

        if ($deadline) {
            $query->drivenBefore($deadline);
        }
        $query->limit($position);

        $rows = $query->get();

        // Case where lev doesn't have any finishes yet
        if (! $rows->count()) {
            return false;
        }

        return $rows->last()->kuski_id == $this->id && $rows->count() == $position;
    }

    /**
     * Get all the levels this kuski has at least one time in
     * Optionally use only levels of a pack for checking
     *
     * @param Pack|null $pack
     * @return static
     * @todo use collection of levels as parameter
     */
    public function levelsInvolvedIn(Pack $pack = null)
    {
        if ($pack) {
            $levels = $pack->levels;
        } else {
            $levels = Level::all();
        }

        return $levels->filter(function($level) {
            return $level->kuskis->pluck('kuski_id')->contains($this->id);
        });
    }

    /**
     * Accessor for getting the team of this kuski in formatted form
     *
     * @return type
     */
    public function getTeamFormattedAttribute()
    {
        $team = $this->attributes['team'];

        if ($team) {
            return '[' . $team . ']';
        }

        return '';
    }
}
