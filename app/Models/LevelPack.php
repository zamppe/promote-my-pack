<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LevelPack
 * @package App\Models
 * @todo Most likely redundant?
 */
class LevelPack extends Model
{
    protected $table = 'level_pack';
}
