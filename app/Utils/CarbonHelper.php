<?php

namespace App\Utils;

use Carbon\Carbon;

class CarbonHelper {
    /**
     * Create a Carbon instance from input
     *
     * @param string $input
     * @return Carbon
     */
    private static function carbonInput($input, $mode = 'min')
    {
        if ($input !== '' && $input !== null) {
            return Carbon::createFromFormat('Y-m-d', $input);
        }

        if ($mode === 'min') {
            return Carbon::minValue();
        }

        if ($mode === 'max') {
            return Carbon::maxValue();
        }
    }

    /**
     * Create a Carbon instance from input with default value as max
     *
     * @param string $input
     * @return Carbon
     */
    public static function maxCarbonInput($input)
    {
        return self::carbonInput($input, 'max');
    }

    /**
     * Create a Carbon instance from input with default value as min
     *
     * @param string $input
     * @return Carbon
     */
    public static function minCarbonInput($input)
    {
        return self::carbonInput($input, 'min');
    }
}
