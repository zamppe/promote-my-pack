<?php

namespace App\Utils;

use Carbon\Carbon;

class DateRange
{
    protected $start;
    protected $end;

    public function __construct($start = '', $end = '')
    {
        if (empty($start)) {
            $this->start = Carbon::minValue();
        } else if (is_string($start)) {
            $this->start = Carbon::create($start);
        } else if ($start instanceof Carbon) {
            $this->start = $start;
        } else {
            $this->start = Carbon::minValue();
        }

        if (empty($end)) {
            $this->end = Carbon::maxValue();
        } else if (is_string($end)) {
            $this->end = Carbon::create($end);
        } else if ($end instanceof Carbon) {
            $this->end = $end;
        } else {
            $this->end = Carbon::maxValue();
        }
    }

    public function useYearBounds()
    {
        $this->start->startOfYear();
        $this->end->endOfYear();
    }

    public function start()
    {
        return $this->start;
    }

    public function end()
    {
        return $this->end;
    }
}
