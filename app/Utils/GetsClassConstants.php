<?php

namespace App\Utils;

trait GetsClassConstants {
    public static function getAll()
    {
        $constants = (new \ReflectionClass(__CLASS__))->getConstants();

        return collect($constants);
    }
}
