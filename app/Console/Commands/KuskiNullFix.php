<?php

namespace App\Console\Commands;

use App\Services\KuskiService;
use Illuminate\Console\Command;

class KuskiNullFix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kuskis:nullfix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attempt to fix kuski records that are null';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        KuskiService::updateNullKuskiRecords();
    }
}
