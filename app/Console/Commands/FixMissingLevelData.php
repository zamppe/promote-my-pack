<?php

namespace App\Console\Commands;

use App\Services\LevelService;
use Illuminate\Console\Command;

class FixMissingLevelData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'levels:fix_missing_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attempt to fix missing level or image data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        LevelService::fixMissingData();
    }
}
