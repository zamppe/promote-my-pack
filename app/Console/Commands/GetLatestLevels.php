<?php

namespace App\Console\Commands;

use App\Services\LevelService;
use Illuminate\Console\Command;

class GetLatestLevels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'levels:get-latest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get latest levels from eol db and store into local db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        LevelService::getLatestLevels();
    }
}
