<?php

namespace App\Console\Commands;

use App\Models\Level;
use App\Models\Pack;
use App\Services\AchievementService;
use App\Services\LevelService;
use App\Services\PackService;
use Illuminate\Console\Command;

class RunUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update times, update improvement history, update total times, update achievements';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $levels = collect();
        $packs = Pack::all();

        foreach($packs as $pack) {
            PackService::updateAll($pack);
        }
    }
}
