<?php

namespace App\Console\Commands;

use App\Services\KuskiService;
use Illuminate\Console\Command;

class UpdateAllKuskiTeam extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kuskis:updateteams';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update every kuskis team to the most recent known team';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        KuskiService::UpdateAllKuskiTeam();
    }
}
