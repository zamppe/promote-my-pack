<?php

namespace App\Console\Commands;

use App\Models\Pack;
use Illuminate\Console\Command;
use Cache;

class CacheRefresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:refresh {pack?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh cache after automatic update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $packId = $this->argument('pack');
        if ($packId !== null && ! is_numeric($packId)) {
            exit;
        }        

        if ($packId) {
            $pack = Pack::find($packId);
            Cache::tags($pack->cache_tag)->flush();
            $packs = [];
        } else {
            Cache::flush();
            $packs = Pack::all();
        }
        
        $remember = config('cache.remember');

        foreach($packs as $pack) {
            foreach($pack->levels as $level) {
                foreach([3, 20] as $n) {
                    Cache::tags($pack->cache_tag)->remember("best_times{$level->id}{$n}", $remember, function() use ($pack, $level, $n) {
                        return $level->top($n, $with = ['kuski'], $pack);
                    });

                    Cache::tags($pack->cache_tag)->remember("finish_counts{$level->id}{$n}", $remember, function() use ($pack, $level, $n) {
                        return $level->finishCounts($n, $with = ['kuski'], $pack);
                    });

                    Cache::tags($pack->cache_tag)->remember("recent_activity{$level->id}{$n}", $remember, function() use ($pack, $level, $n) {
                        return $level->recentActivity($n, $with = ['kuski'], $pack);
                    });
                }
            }
        }
    }
}
