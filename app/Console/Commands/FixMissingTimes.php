<?php

namespace App\Console\Commands;

use App\Services\TimeUpdater;
use App\Services\PackService;
use Illuminate\Console\Command;

class FixMissingTimes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'times:fix_missing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attempt to add times that are missing from the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Handle the command
     *
     * @return void
     */
    public function handle()
    {
        $relatedPacks = TimeUpdater::fixMissingTimes();

        foreach ($relatedPacks as $pack) {
            PackService::updateHistoryAndStatistics($pack);
            cacheRefresh($pack->id);
        }
    }
}
