<?php

namespace App\Console;

use App\Console\Commands\CacheRefresh;
use App\Console\Commands\RunUpdate;
use App\Console\Commands\ScheduleList;
use App\Console\Commands\KuskiNullFix;
use App\Console\Commands\UpdateAllKuskiTeam;
use App\Console\Commands\FixMissingLevelData;
use App\Console\Commands\FixMissingTimes;
use App\Console\Commands\GetAllLevels;
use App\Console\Commands\GetLatestLevels;
use App\Models\Pack;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CacheRefresh::class,
        RunUpdate::class,
        ScheduleList::class,
        KuskiNullFix::class,
        UpdateAllKuskiTeam::class,
        FixMissingLevelData::class,
        FixMissingTimes::class,
        GetAllLevels::class,
        GetLatestLevels::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $deadlineUpdateAt = config('automatic_updates.deadline_update_at');
        $massUpdateAt = config('automatic_updates.mass_update_at');
        $dataFixAt = config('automatic_updates.data_fix_at');
        $cacheRefreshAt = config('automatic_updates.cache_refresh_at');
        $kuskiTeamUpdateAt = config('automatic_updates.kuski_team_update_at');
        $kuskiNullFixAt = config('automatic_updates.kuski_null_fix_at');
        $timezone = config('app.timezone');

        if (! \Schema::hasTable('packs')) {
            return;
        }

        // Update pack deadlines according to their update_frequency setting
        foreach(Pack::all() as $pack) {
            if ($pack->update_frequency == Pack::UPDATE_FREQUENCY_DAILY) {
                $schedule->call(function () use($pack, $deadlineUpdateAt) {
                    $pack->deadline = \Carbon\Carbon::now();
                    $pack->save();
                })->timezone($timezone)->dailyAt($deadlineUpdateAt);
            } else if ($pack->update_frequency == Pack::UPDATE_FREQUENCY_WEEKLY) {
                $schedule->call(function () use($pack, $deadlineUpdateAt) {
                    $pack->deadline = \Carbon\Carbon::now();
                    $pack->save();
                })->timezone($timezone)->weekly()->mondays()->at($deadlineUpdateAt);
            } else if ($pack->update_frequency == Pack::UPDATE_FREQUENCY_MONTHLY) {
                $schedule->call(function () use($pack, $deadlineUpdateAt) {
                    $pack->deadline = \Carbon\Carbon::now();
                    $pack->save();
                })->timezone($timezone)->monthlyOn(1, $deadlineUpdateAt);
            }
        }

        // Get latest levels every hour
        $schedule->command('levels:get-latest')
            ->timezone($timezone)
            ->hourly();

        // Do mass update every night at 4am
        $schedule->command('update:all')
            ->timezone($timezone)
            ->dailyAt($massUpdateAt);

        // Try to fix missing level data at 5:50
        $schedule->command('levels:fix_missing_data')
            ->timezone($timezone)
            ->dailyAt($dataFixAt);

        // Update kuski team to the most recent known one
        $schedule->command('kuskis:updateteam')
            ->timezone($timezone)
            ->dailyAt($kuskiTeamUpdateAt);

        // Check and update kuskis that are missing data for some reason
        $schedule->command('kuskis:nullfix')
            ->timezone($timezone)
            ->dailyAt($kuskiNullFixAt);

        // Refresh cache every night at 6.30am
        $schedule->command('cache:refresh')
            ->timezone($timezone)
            ->dailyAt($cacheRefreshAt);
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
