<?php

namespace App\Services;

use App\Jobs\GetKuskiJob;
use App\Models\Kuski;
use App\Models\Level;
use App\Models\Time;
use App\Utils\DateRange;
use Carbon\Carbon;

/**
 * Class TimeUpdater
 * @package App\Services
 */
class TimeUpdater {
    /**
     * Update a json row from eol times api
     *
     * @param stdClass $row
     * @param Level $level
     * @return Time
     */
    public static function updateRowFromEolApi($row, Level $level)
    {
        $kuski = Kuski::find($row->kuskiindex);

        if(!$kuski) {
            $kuski = Kuski::create([
                'id' => $row->kuskiindex,
            ]);
            dispatch(new GetKuskiJob($row->kuskiindex));
        }

        return Time::createFromEolApiRow($row, $level);
    }

    /**
     * Attempt to add times that are missing from database.
     *
     * Times become missing when a time is locked/hidden because it is a wr time and a times update is done.
     * The highest time index for a level becomes larger than the time index of the wr time and is from there on ignored
     * by the automatic times update.
     *
     * This issue is known to happen at least when 2 different persons beat an internal wr during a short timespan.
     *
     * @return void
     */
    public static function fixMissingTimes()
    {
        $internalLevels = Level::whereIn('id', config('promo.internal_levels'))->get();
        $someTimeAgo = Carbon::now()->subMonths(config('automatic_updates.missing_times_fix.some_time_ago'));

        $relatedPacks = collect();

        foreach ($internalLevels as $level) {
            $index = $level->getHighestTimeIndex(new DateRange(Carbon::minValue(), $someTimeAgo));
            $timesData = EolApiService::getTimes($level->id, $index);
            $timesData = collect_obj_key_by_attr($timesData, 'timeindex');

            /* get ids of all times that were driven since some time ago */
            /* todo this will not work properly if internal lev had over 25 000 (API number of rows restriction) finishes since some time ago but I guess that can never happen */
            $recentTimeIds = $level->times()->where('id', '>', $index)->pluck('times.id');

            /* get time ids that are in eol api but not in the database */
            $missingTimeIds = $timesData->map(function($timeData) {
                return (int) $timeData->timeindex;
            })->values()->diff($recentTimeIds->values());

            /* store each time that was missing */
            foreach ($missingTimeIds as $missingTimeId) {
                self::updateRowFromEolApi($timesData->get($missingTimeId), $level);
            }

            if ($missingTimeIds->count()) {
                $relatedPacks = $relatedPacks->merge($level->packs);
            }
        }

        return $relatedPacks->unique('id');
    }
}
