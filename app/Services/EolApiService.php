<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Level;

/**
 * Class EolApiService
 * @package App\Services
 */
class EolApiService {
    /**
     * Get level data for level index from eol api
     *
     * @param $levelIndex
     * @return mixed
     */
    public static function getLevel($levelIndex)
    {
        $client = new \GuzzleHttp\Client();
        $levelApiUrl = config('eol_api.level');
        $levelApiUrl .= $levelIndex;
        $response = $client->get($levelApiUrl);

        return json_decode($response->getBody()->getContents());
    }

    /**
     * Get levels data from eol api starting from $startId
     *
     * @param $startId
     * @return mixed
     */
    public static function getLevels($startId)
    {
        $client = new \GuzzleHttp\Client();
        $levelApiUrl = config('eol_api.levels');
        $levelApiUrl .= '?startindex=' . $startId;
        $response = $client->get($levelApiUrl);

        return json_decode($response->getBody()->getContents());
    }

    /**
     * Attempt to save new level
     *
     * @param stdClass $levelData
     * @return null|Level
     */
    public static function saveLevelData($levelData)
    {
        if (isset($levelData->error)) {
            // Error result
            return null;
        } else if (isset($levelData->LevelIndex)) {
            // OK result
            return Level::createFromEolApiRow($levelData->LevelIndex, $levelData);
        } else {
            // Empty result
            return null;
        }
    }

    /**
     * Get level data for level index from eol api and save it in db
     *
     * @param $levelIndex
     * @return mixed
     */
    public static function getAndSaveLevel($levelIndex)
    {
        // if the lev exists already, abort
        if ($level = Level::find($levelIndex)) {
            return $level;
        }

        $levelData = self::getLevel($levelIndex);
        return self::saveLevelData($levelData);
    }

    /**
     * Get times data for level index from eol api
     *
     *
     * @param $levelIndex
     * @param $timeIndex
     * @return mixed
     */
    public static function getTimes($levelIndex, $timeIndex)
    {
        $client = new \GuzzleHttp\Client();
        $baseUrl = config('eol_api.times');

        $timesApiUrl = $baseUrl . $levelIndex;
        $timesApiUrl .= '?bestall=all';
        $timesApiUrl .= '&startindex=' . $timeIndex;
        $timesApiUrl .= '&noorder=true';
        $response = $client->get($timesApiUrl);
        $responseData = json_decode($response->getBody()->getContents());

        return $responseData;
    }

    /**
     * Get kuski data for kuski index from eol api
     *
     * @param $kuskiIndex
     * @return mixed
     */
    public static function getKuski($kuskiIndex)
    {
        $client = new \GuzzleHttp\Client();
        $kuskiApiUrl = config('eol_api.kuski');
        $kuskiApiUrl .= $kuskiIndex;
        $response = $client->get($kuskiApiUrl);

        return json_decode($response->getBody()->getContents());
    }

    /**
     * Get pack levels data for pack name from eol api
     *
     * @param string $packName
     * @return mixed
     */
    public static function getPackLevels(string $packName)
    {
        $client = new \GuzzleHttp\Client();
        $packsApiUrl = config('eol_api.packs.by_name');
        $packsApiUrl .= $packName;
        $response = $client->get($packsApiUrl);

        return json_decode($response->getBody()->getContents());
    }
}
