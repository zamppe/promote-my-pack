<?php

namespace App\Services;

use App\Jobs\TargetAchievementJob;
use App\Models\Achieve;
use App\Models\Kuski;
use App\Models\Level;
use App\Models\Pack;
use App\Models\TargetAchievement;
use App\Models\Time;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class AchievementService
 * @package App\Services
 */
class AchievementService {
    /**
     * Process one achievement
     *
     * @param TargetAchievement $achievement
     */
    public static function processAchievement(TargetAchievement $achievement)
    {
        dispatch(new TargetAchievementJob($achievement));
    }

    /**
     * Process every achievement of a pack
     *
     * @param Pack $pack
     */
    public static function processAchievementsForPack(Pack $pack)
    {
        self::deleteAchievesForPack($pack);

        $achievements = $pack->targetAchievements()->with('pack')->get();

        foreach($achievements as $achievement) {
            self::processAchievement($achievement);
        }
    }

    /**
     * Delete achieves for a pack
     *
     * @param Pack $pack
     */
    private static function deleteAchievesForPack(Pack $pack)
    {
        $query = Achieve::join('target_achievements as t', function($join) {
            $join->on('achieves.achievable_id', '=', 't.id');
            $join->where('achieves.achievable_type', TargetAchievement::class);
        });
        $query->where('t.pack_id', $pack->id);
        $query->delete();
    }
}
