<?php

namespace App\Services;

use App\Jobs\GetLevelChunkJob;
use App\Jobs\UpdateTimesJob;
use App\Jobs\UpdateImprovementHistoryJob;
use App\Models\Level;
use App\Models\LevelPack;
use App\Models\Pack;
use App\Services\EolApiService;
use App\Services\KuskiService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\File;
use File as FileFacade;
use Queue;
use Storage;

/**
 * Class LevelService
 * @package App\Services
 */
class LevelService {
    /**
     * Get all levels from eol db and store into local db
     *
     * @return void
     */
    public static function getAllLevels()
    {
        $endId = self::findHighestEOLLevelIndex();

        dispatch(new GetLevelChunkJob(0, $endId));
    }

    /**
     * Get latest levels from eol db and store into local db
     *
     * @return void
     */
    public static function getLatestLevels()
    {
        $startId = Level::max('id');
        $endId = self::findHighestEOLLevelIndex();

        dispatch(new GetLevelChunkJob($startId, $endId));
    }

    /**
     * @param Level $level
     */
    public static function updateTimes(Level $level)
    {
        //update times with a job to avoid getting 'stuck' on request for a long time
        dispatch(new UpdateTimesJob($level));
    }

    /**
     * Find the highest EOL level index
     *
     * @return mixed
     */
    public static function findHighestEOLLevelIndex()
    {
        $index = \Storage::exists('eol_last_level_id.txt') ?
            \Storage::get('eol_last_level_id.txt') :
            config('eol_api.level_index_guess');

        $step = 2;
        $emptyFound = false;
        $add = true;
        while(true) {
            if ($step == 1) {
                \Storage::put('eol_last_level_id.txt', $index);

                return $index;
            }

            $levelData = EolApiService::getLevel($index);
            if (empty($levelData)) {
                if (! $emptyFound) {
                    $emptyFound = true;
                }
                $add = false;
            } else {
                $add = true;
            }

            $step = $emptyFound ? (int) ceil($step / 2) : $step * 2;
            $index = $add ? $index + $step : $index - $step;
            printf("step: %d, index: %d\n", $step, $index);
        }
    }

    /**
     * Get all levels from database that are missing data and attempt to fetch them
     *
     * @return void
     */
    public static function fixMissingData()
    {
        $levelIds = LevelPack::leftJoin('mediables', 'level_pack.level_id', '=', 'mediables.mediable_id')
            ->where(function($q) {
                $q->where('mediables.tag', 'level')
                  ->orWhereNull('mediables.tag');
            })
            ->where(function($q) {
                $q->where('mediables.mediable_type', Level::class)
                  ->orWhereNull('mediables.mediable_type');
            })
            ->whereNull('mediables.mediable_id')
            ->pluck('level_pack.level_id');

        $levelsMissingLevelData = Level::whereIn('id', $levelIds->toArray())->get();

        foreach($levelsMissingLevelData as $level) {
            self::downloadAndStoreLevel($level);
        }

        /* todo: change query to filter out levels that dont have level data */
        $levelIds = LevelPack::leftJoin('mediables', function($join) {
            $join->on('level_pack.level_id', '=', 'mediables.mediable_id')
                 ->where('mediables.tag', 'image')
                 ->orWhereNull('mediables.tag');
        })
            ->where(function($q) {
                $q->where('mediables.mediable_type', Level::class)
                  ->orWhereNull('mediables.mediable_type');
            })
            ->whereNull('mediables.mediable_id')
            ->pluck('level_pack.level_id');

        $levelsMissingImgData = Level::whereIn('id', $levelIds->toArray())->get();

        foreach($levelsMissingImgData as $level) {
            self::makeAndStoreImage($level);
        }
    }

    /**
     * Download .lev data for a level
     * Store the .lev into storage
     * Attach the level as Mediable media to level model
     *
     * @param Level $level
     */
    public static function downloadAndStoreLevel(Level $level)
    {
        $levelId = $level->id;
        $levelName = strtolower($level->filename);
        $levelDiskDirectory = join(DIRECTORY_SEPARATOR, [
            "levels",
            "{$levelId}"
        ]);
        $filename = "{$levelName}.lev";

        /* The location where .lev will be stored */
        $levelPath = storage_path(join(DIRECTORY_SEPARATOR, array("app", $levelDiskDirectory, $filename)));

        /* Create the folder where .lev will be stored */
        Storage::disk('local')->makeDirectory($levelDiskDirectory);

        /* If the level is an internal there is no need to download anything. Get file from local storage instead */
        if (collect(config('promo.internal_levels'))->contains($levelId)) {
            $internalStoragePath = storage_path(join(DIRECTORY_SEPARATOR, array("app", "internals", $filename)));
            FileFacade::copy($internalStoragePath, $levelPath);
        } else {
            $downloadBaseUrl = 'http://elmaonline.net/downloads/lev/';
            $tempPath = storage_path(join(DIRECTORY_SEPARATOR, array("app", "temp", "temp.lev")));

            $resource = new \GuzzleHttp\Psr7\LazyOpenStream($tempPath, 'w');
            $client = new \GuzzleHttp\Client([
                'base_uri' => $downloadBaseUrl,
            ]);

            $client->request('GET', (string) $levelId, ['sink' => $resource]);
            $resource->close();

            FileFacade::move($tempPath, $levelPath);
        }

        if (Storage::disk('local')->exists($levelPath = join(DIRECTORY_SEPARATOR, array($levelDiskDirectory, $filename)))) {
            $media = \MediaUploader::importPath('local', $levelPath);
            $level->attachMedia($media, 'level');
        }
    }

    /**
     * Create image from .lev data for a level by using DomiMapMaker
     * Store the image to storage
     * Attach the image as Mediable media to level model
     *
     * @param Level $level
     */
    public static function makeAndStoreImage(Level $level)
    {
        $levelId = $level->id;
        $levelName = strtolower($level->filename);
        $levelImagePath = join(DIRECTORY_SEPARATOR, array("app", "levelimages", "{$levelId}", "{$levelName}.png"));
        Storage::disk('local')->makeDirectory(join(DIRECTORY_SEPARATOR, array("levelimages", "{$levelId}")));

        \App\Services\DomiMapMaker::makeImage($level->firstMedia('level')->getAbsolutePath(), storage_path($levelImagePath), [
            'zoom' => 5,
        ]);

        if (Storage::disk('local')->exists($path = join(DIRECTORY_SEPARATOR, array("levelimages", "{$levelId}", "{$levelName}.png")))) {
            $media = \MediaUploader::importPath('local', $path);

            $level->attachMedia($media, 'image');
        }
    }

    /**
     * Rebuild the whole improvement history for level of a pack
     *
     * @param Pack $pack
     * @param Level $level
     */
    public static function rebuildImprovementHistory(Pack $pack, Level $level, $deleteOnly = false)
    {
        dispatch(new UpdateImprovementHistoryJob($level, $pack, $deleteOnly));
    }
}
