<?php

namespace App\Services;

use App\Models\Kuski;
use App\Jobs\GetKuskiJob;
use Illuminate\Http\Request;

/**
 * Class KuskiService
 * @package App\Services
 */
class KuskiService {
    /**
     * Update kuski records that are NULL
     */
    public static function updateNullKuskiRecords()
    {
        $kuskis = Kuski::whereNull('name')
            ->orWhere('team', '0')
            ->orWhere('nationality', '0')
            ->get(['id']);

        foreach($kuskis as $kuski) {
            dispatch(new GetKuskiJob($kuski->id));
        }
    }

    /**
     * Update every kuskis team to the most recent known team
     */
    public static function updateAllKuskiTeam()
    {
        $kuskis = Kuski::all();

        foreach($kuskis as $kuski) {
            $latestTime = $kuski->times()->orderBy('id', 'desc')->first();

            if ($latestTime !== null && $kuski->team !== $latestTime->team) {
                $kuski->update([
                    'team' => $latestTime->team,
                ]);
            }
        }
    }
}



