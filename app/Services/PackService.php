<?php

namespace App\Services;

use App\Jobs\UpdateTotalTimeHistoryJob;
use App\Models\Level;
use App\Models\Pack;

/**
 * Class PackService
 * @package App\Services
 */
class PackService {
    /**
     * Add a level to a pack based on eol level id
     *
     * @param Pack $pack
     * @param $levelId
     */
    public static function addLevelByEolId(Pack $pack, $levelId)
    {
        $level = Level::find($levelId);
        if ($level) {
            if(!$pack->levels->contains($level)) {
                $pack->addLevel($level);
                LevelService::updateTimes($level);
                LevelService::rebuildImprovementHistory($pack, $level);
                self::rebuildTotalTimeHistory($pack);
                cacheRefresh($pack->id);
                if (! $level->hasLevelData()) {
                    LevelService::downloadAndStoreLevel($level);
                    LevelService::makeAndStoreImage($level);
                }
            }
        } else {
            $levelData = EolApiService::getLevel($levelId);

            if(!isset($levelData->error)) {
                $newLevel = Level::create([
                    'id' => $levelData->LevelIndex,
                    'filename' => $levelData->LevelName,
                    'name' => $levelData->LongName,
                    'apples' => $levelData->Apples,
                    'killers' => $levelData->Killers,
                    'flowers' => $levelData->Flowers,
                ]);

                $pack->addLevel($newLevel);
                LevelService::updateTimes($newLevel);
                LevelService::rebuildImprovementHistory($pack, $newLevel);
                self::rebuildTotalTimeHistory($pack);
                cacheRefresh($pack->id);
                LevelService::downloadAndStoreLevel($newLevel);
                LevelService::makeAndStoreImage($newLevel);
            } else {
                // todo inform user that level is locked and cant be added
            }
        }
    }

    /**
     * Rebuild improvement history every level of the pack
     *
     * @param Pack $pack
     */
    public static function rebuildImprovementHistory(Pack $pack)
    {
        foreach($pack->levels as $level) {
            LevelService::rebuildImprovementHistory($pack, $level);
        }
    }

    /**
     * Rebuild total time history for the pack
     *
     * @param Pack $pack
     */
    public static function rebuildTotalTimeHistory(Pack $pack)
    {
        dispatch(new UpdateTotalTimeHistoryJob($pack->fresh()));
    }

    /**
     * Run mass update for this pack
     *
     * @param Pack $pack
     */
    public static function updateAll(Pack $pack)
    {
        foreach($pack->levels as $level) {
            LevelService::updateTimes($level);
            printf('Update times: %s, %s, id: %d\n', $level->name, $level->filename, $level->id);
        }
        TimeUpdater::fixMissingTimes();

        self::updateHistoryAndStatistics($pack);
        cacheRefresh($pack->id);
    }

    /**
     * Run history and statistics update
     *
     * @param Pack $pack
     */
    public static function updateHistoryAndStatistics(Pack $pack)
    {
        PackService::rebuildImprovementHistory($pack);
        printf('Rebuild improvement history: %s, id: %d\n', $pack->name, $pack->id);

        PackService::rebuildTotalTimeHistory($pack);
        printf('Rebuild total times history: %s, id: %d\n', $pack->name, $pack->id);

        AchievementService::processAchievementsForPack($pack);
        printf('Rebuild achievement history: %s, id: %d\n', $pack->name, $pack->id);
    }
}
