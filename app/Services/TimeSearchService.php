<?php

namespace App\Services;

use App\Models\Time;
use App\Models\Pack;
use App\Utils\CarbonHelper;
use App\Utils\DateRange;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class TimeSearchService
 * @package App\Services
 */
class TimeSearchService
{
    /**
     * TimeSearchService constructor.
     * @param Request|null $request
     */
    public function __construct(Request $request = null)
    {
        $this->query = Time::query()->with('kuski', 'globalTimeBan');
        $this->request = $request;
    }

    /**
     * Run filters
     *
     * @return $this
     */
    public function search()
    {
        if ($this->request->kuski) {
            $this->filterByKuski($this->request->kuski);
        }

        if ($this->request->nationality && $this->request->nationality != 'all') {
            $this->filterByNationality($this->request->nationality);
        }

        if ($this->request->team && $this->request->team != 'all') {
            $this->filterByTeam($this->request->team);
        }

        if ($this->request->level) {
            $this->filterByLevel($this->request->level);
        }

        if ($this->request->pack) {
            $this->filterByPack($this->request->pack);
        }

        if (! $this->request->include_banned) {
            $this->filterByGlobalBanned();
        }

        $this->filterByDateRange($this->request->after, $this->request->before);

        if ($this->request->best_only) {
            $this->filterByBestOnly();
        }

        return $this;
    }

    /**
     * Eager loads
     *
     * @param array $with
     * @return $this
     */
    public function with($with = [])
    {
        $this->query->with($with);

        return $this;
    }

    /**
     * Get query results as collection
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function get()
    {
        return $this->query->get();
    }

    /**
     * Get query results as paginator
     *
     * @param int $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getPaginated($perPage = 100)
    {
        // $renaults = $this->get();
        // $renaults->groupBy('kuski_id');
        // return new LengthAwarePaginator($renaults, $renaults->count(), $perPage);
        return $this->query->paginate($perPage, ['times.*']);
    }

    /**
     * Filter by level
     *
     * @param $level
     * @return $this
     */
    public function filterByLevel($level)
    {
        if (is_numeric($level)) {
            $this->query->where('times.level_id', $level);
        } else if (is_array($level)) {
            $this->query->whereIn('times.level_id', $level);
        } else if (is_string($level)) {
            $this->query->whereHas('level', function($query) use($level){
                $query->where('levels.filename', 'like', "%{$level}%")
                      ->orWhere('levels.name', 'like', "%{$level}%");
            });
        }

        return $this;
    }

    /**
     * Filter by pack
     *
     * @param $pack
     * @return $this
     */
    public function filterByPack($pack)
    {
        if (is_numeric($pack)) {
            $pack = Pack::find($pack);
        }

        if ($pack instanceof Pack && ! $this->request->include_banned) {
            $this->query->filterByPack($pack);
        }

        return $this;
    }

    /**
     * Filter by best only
     *
     * @return $this
     */
    public function filterByBestOnly()
    {
        $this->query->bestOnlyPerKuski();

        return $this;
    }

    /**
     * Filter by kuski
     *
     * @param $kuski
     */
    public function filterByKuski($kuski)
    {
        $this->query->whereHas('kuski', function($query) use($kuski) {
            $query->where('name', 'like', "%{$kuski}%");
        });

        return $this;
    }

    /**
     * Filter by nationality
     *
     * @param $nationality
     */
    public function filterByNationality($nationality)
    {
        $this->query->whereHas('kuski', function($query) use($nationality) {
            $query->where('nationality', $nationality);
        });

        return $this;
    }

    /**
     * Filter by team
     *
     * @param $team
     */
    public function filterByTeam($team)
    {
        $this->query->whereHas('kuski', function($query) use($team) {
            $query->where('team', $team);
        });

        return $this;
    }

    /**
     * Filter by date range
     *
     * @param string $after
     * @param string $before
     * @return void
     */
    public function filterByDateRange($after, $before)
    {
        $before = CarbonHelper::maxCarbonInput($before);
        $after = CarbonHelper::minCarbonInput($after);

        $dateRange = new DateRange($after, $before);

        $this->query->drivenDuring($dateRange);
    }

    /**
     * Filter by global banned
     *
     * @return $this
     */
    public function filterByGlobalBanned()
    {
        $this->query->notGlobalBanned();

        return $this;
    }
}
