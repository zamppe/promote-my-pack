<?php

namespace App\Services;

use App\Models\Kuski;
use App\Models\Level;
use App\Models\Pack;
use Illuminate\Support\Collection;

/**
 * Class RecentActivityBuilder
 * @package App\Services
 */
class RecentActivityBuilder
{
    protected $limit;
    protected $include;
    protected $dateRange;
    protected $pack;
    protected $level;
    protected $kuski;

    const PR_KEY = 'pr';
    const WR_KEY = 'wr';
    const FF_KEY = 'ff';

    public function __construct()
    {
        $this->dateRange = null;
        $this->pack = null;
        $this->level = null;
        $this->kuski = null;
        $this->limit = 0;
    }

    public function pr()
    {
        if (! isset($this->include)) {
            $this->include = collect();
        }

        if (! $this->include->flip()->has(self::PR_KEY)) {
            $this->include->push(self::PR_KEY);
        }

        return $this;
    }

    public function wr()
    {
        if (! isset($this->include)) {
            $this->include = collect();
        }

        if (! $this->include->flip()->has(self::WR_KEY)) {
            $this->include->push(self::WR_KEY);
        }

        return $this;
    }

    public function ff()
    {
        if (! isset($this->include)) {
            $this->include = collect();
        }

        if (! $this->include->flip()->has(self::FF_KEY)) {
            $this->include->push(self::FF_KEY);
        }

        return $this;
    }

    public function all()
    {
        $this->include = collect([
            self::PR_KEY,
            self::WR_KEY,
            self::FF_KEY,
        ]);

        return $this;
    }

    public function drivenDuring(DateRange $dateRange)
    {
        $this->dateRange = $dateRange;

        return $this;
    }

    public function limit($n = 0)
    {
        $this->limit = $n;

        return $this;
    }

    public function pack(Pack $pack)
    {
        $this->pack = $pack;

        return $this;
    }

    public function level(Level $level)
    {
        $this->level = $level;

        return $this;
    }

    public function kuski(Kuski $kuski)
    {
        $this->kuski = $kuski;

        return $this;
    }

    public function get()
    {
        $builders = collect();

        /* Todo: clean this use of Pack mess somehow */
        $pack = new Pack();

        if ($this->include->flip()->has(self::PR_KEY)) {
            $builders->push($pack->prImprovements(false, false));
        }

        if ($this->include->flip()->has(self::WR_KEY)) {
            $builders->push($pack->baseWrImprovements(false, false));
        }

        if ($this->include->flip()->has(self::FF_KEY)) {
            $builders->push($pack->firstFinishes(false, false));
        }

        $recentActivity = collect();

        foreach($builders as $builder) {
            if ($this->dateRange) {
                $builder->drivenDuring($this->dateRange);
            }

            if ($this->kuski) {
                $builder->belongsToKuski($this->kuski);
            }

            if ($this->level) {
                $builder->belongsToLevel($this->level);
            }

            if ($this->pack) {
                $builder->belongsToPack($this->pack);
            }
        }

        $lastBuilder = $builders->last();
        $builders->splice($builders->keys()->last());

        foreach($builders as $builder) {
            $lastBuilder->unionAll($builder);
        }

        $with = ['kuski', 'level'];

        $lastBuilder->withoutGlobalScopes()
            ->orderBy('id', 'desc')
            ->with($with);

        if ($this->limit) {
            $lastBuilder->limit($this->limit);
        }

        return $lastBuilder->get();
    }

    public static function start()
    {
        return new self();
    }
}
