<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

/**
 * Class PackContainsCustomAchievement
 * @package App\Http\Middleware
 */
class PackContainsCustomAchievement
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return redirect('/login');
        }

        $pack = request()->pack;
        $customAchievement = request()->customAchievement;

        abort_if(! is_null($pack) && ! is_null($customAchievement) && $customAchievement->pack_id != $pack->id, 403, "no rights");

        return $next($request);
    }
}
