<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

/**
 * Class IsPackAdminMiddleware
 * @package App\Http\Middleware
 */
class IsPackAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            return redirect('/login');
        }

        $pack = request()->pack;

        abort_if($pack && ! $request->user()->isPackAdmin($pack), 403, "no rights");

        return $next($request);
    }
}
