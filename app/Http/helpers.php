<?php

/**
 * @return \Illuminate\Contracts\Auth\Authenticatable|null
 */
function user()
{
    if(auth()->check()) {
        return auth()->user();
    }

    return null;
}

/**
 * convert 01:23,45 format to milliseconds
 * @param $strTime
 * @return int
 */
function strtimetoms($strTime)
{
    $minutes = $seconds = $tenths = 0;
    sscanf($strTime, "%d:%d,%d", $minutes, $seconds, $tenths);

    return $minutes * 60 * 1000 + $seconds * 1000 + $tenths * 10;
}

/**
 * convert milliseconds to 01:23:45,67 format or 01:23,45 format
 * @param $ms
 * @param bool $forceLongFormat
 * @return string
 */
function mstostrtime($ms, $forceLongFormat = false)
{
    if ($ms >= 1000 * 60 * 60 || $forceLongFormat) {
        return sprintf("%02d:%02d:%02d,%02d", $ms / 60 / 60 / 1000, $ms / 60 / 1000 % 60, $ms / 1000 % 60, $ms / 10 % 100);
    }

    return sprintf("%02d:%02d,%02d", $ms / 60 / 1000 % 60, $ms / 1000 % 60, $ms / 10 % 100);
}

/**
 * @param $class
 * @return bool|string
 */
function className($class)
{
    $classname = get_class($class);

    return (substr($classname, strrpos($classname, '\\') + 1));
}

/**
 * @param \Illuminate\Support\Collection $collection
 * @return \Illuminate\Support\Collection
 */
function keyByValue(\Illuminate\Support\Collection $collection)
{
    return collect($collection->reduce(function($carry, $item) {
        return $carry + [$item => $item];
    }, []));
}

/**
 * Clear cache with lowest priority queue job
 */
function cacheRefresh($packId)
{
    if (is_dev()) {
        $cacheRefreshCount = \DB::table('jobs')->where('queue', 'clear')->count();
    } else {
        $cacheRefreshCount = \Redis::connection()->llen('queues:clear');
    }

    if (! $cacheRefreshCount) {
        dispatch(new \App\Jobs\CacheRefreshJob($packId));
    }
}

function linkToPage($url, $title)
{
    return "<a href=\"$url\">$title</a>";
}

/**
 * Convert storage path to full path
 *
 * @param type $storagePath
 * @return type
 */
function storage_file_full_path($storagePath)
{
    $pathPrefix = \Storage::disk('local')
        ->getDriver()
        ->getAdapter()
        ->getPathPrefix();

    return $pathPrefix . $storagePath;
}

function collect_obj_key_by_attr($obj, string $attr)
{
    $collection = collect();

    foreach ($obj as $val) {
        if (isset($val->{$attr})) {
            $collection->put($val->{$attr}, $val);
        } else {
            $collection->push($val);
        }
    }

    return $collection;
}

function is_dev()
{
    return config('app.env') === 'local';
}
