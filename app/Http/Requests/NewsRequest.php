<?php

namespace App\Http\Requests;

/**
 * Class NewsRequest
 * @package App\Http\Requests
 */
class NewsRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100',
            'content' => 'required|max:1000',
        ];
    }
}
