<?php

namespace App\Http\Requests;

/**
 * Class ProfileRequest
 * @package App\Http\Requests
 */
class ProfileRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function all()
    {
        // Don't update empty password
        $input = parent::all();

        if (isset($input['password']) && $input['password'] == '') {
            unset($input['password']);
        }

        return $input;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email',
            'nickname' => '',
            'password' => 'confirmed|min:6'
        ];
    }
}
