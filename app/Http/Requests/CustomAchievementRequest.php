<?php

namespace App\Http\Requests;

/**
 * Class CustomAchievementRequest
 * @package App\Http\Requests
 */
class CustomAchievementRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|max:50',
            'description' => 'required|max:1000',
        ];

        return $rules;
    }
}
