<?php

namespace App\Http\Requests;

use App\Models\TargetAchievement;

/**
 * Class TargetAchievementRequest
 * @package App\Http\Requests
 */
class TargetAchievementRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function all($keys = null)
    {
        $inputs = parent::all();

        $levelIds = collect($inputs['levels'])->filter(function($state, $levelId) {
            return $state == 1;
        })->toArray();

        $inputs['levels'] = $levelIds;

        return $inputs;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $inputs = $this->all();

        $levelCount = collect($inputs['levels'])->filter(function($state, $levelId){
            return $state == 1;
        })->count();

        $rules = [
            'name' => 'required|max:50',
            'levels' => 'required_if:target_type,'.TargetAchievement::TARGET_TYPE_TIME.'|array',
            'description' => 'max:1000',
        ];

        $minTargetValue = $inputs['comparator'] == TargetAchievement::COMPARATOR_EQ ? 1 : 2;

        if($inputs['target_type'] != TargetAchievement::TARGET_TYPE_TOTALTIME) {
            $rules['apply_on'] = 'integer|between:0,' . $levelCount;
            if ($inputs['target_value'] != '0') {
                $rules['target_value'] = $inputs['target_value_type'] == TargetAchievement::TARGET_VALUE_TYPE_TIME? 'elma_time' : 'integer|between:' . $minTargetValue . ',1000';
            }
        } else {
            $rules['apply_on'] = 'integer';
            $rules['target_value'] = $inputs['target_value_type'] == TargetAchievement::TARGET_VALUE_TYPE_TIME? 'elma_time' : 'integer|between:' . $minTargetValue . ',1000';
        }

        return $rules;
    }

    /**
     * Messages
     *
     * @return type
     */
    public function messages()
    {
        return [
            'levels.required_if' => 'Selecting :attribute is required if :other is Time',
        ];
    }
}
