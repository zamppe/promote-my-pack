<?php

namespace App\Http\Controllers;

use App\Models\Achieve;
use App\Models\CustomAchievement;
use App\Models\Level;
use App\Models\Pack;
use App\Models\TargetAchievement;
use App\Models\Time;
use App\Statistics\PackStatisticsTypes;
use App\Utils\CarbonHelper;
use App\Utils\DateRange;
use Illuminate\Http\Request;

/**
 * Class PackStatisticsController
 * @package App\Http\Controllers
 */
class PackStatisticsController extends Controller
{
    const ROW_LIMIT = 100;

    public function show(Pack $pack, Request $request)
    {
        $levels = $pack->levels()->orderBy('position')->get();
        $levels->load('media');

        $levelsForSelect = $levels->mapWithKeys(function ($level) {
            return [
                $level->name => $level->id,
            ];
        })->flip();

        $statisticsTypes = PackStatisticsTypes::getAllWithLang()->toArray();

        $statisticsJson = [];

        if (!empty($request->all())) {
            $statisticsJson = collect($this->generateStatisticsJson($pack, $request))->toJson();
        }

        return view('packs.statistics.show', compact(
            'pack',
            'statisticsTypes',
            'levelsForSelect',
            'statisticsJson'
        ));
    }

    /**
     * Generate statistics for this pack based on request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Pack $pack, Request $request)
    {
        if ($request->wantsJson()) {
            return response()->json($this->generateStatisticsJson($pack, $request), 200);
        }
    }

    private function generateStatisticsJson(Pack $pack, Request $request)
    {
        $this->validate($request, [
            'after' => 'date_format:Y-m-d',
            'before' => 'date_format:Y-m-d',
        ]);

        if ($request->input('type') == PackStatisticsTypes::RECENT_ACTIVITY) {
            $table = $this->initRecentActivityTable($request, $pack);
        } else if ($request->input('type') == PackStatisticsTypes::FINISH_COUNTS) {
            $table = $this->initFinishCountsTable($request, $pack);
        } else if ($request->input('type') == PackStatisticsTypes::TOTALTIME) {
            $table = $this->initTotalTimesTable($request, $pack);
        }

        return [
            'columns' => $table->get('columns'),
            'rows' => $table->get('rows'),
        ];
    }

    /**
     * Parse request
     *
     * @param Request $request
     * @return Collection
     */
    private function parseRequest(Request $request)
    {
        $input = collect($request->only([
            'level_id',
            'kuski',
            'before',
            'after',
        ]));

        $requestCacheKey = md5($input->implode('|'));

        $modifiedInput = $input->only([
            'level_id',
            'kuski'
        ]);

        $before = CarbonHelper::maxCarbonInput($request->input('before'));
        $after = CarbonHelper::minCarbonInput($request->input('after'));

        $dateRange = new DateRange($after, $before);

        $modifiedInput->push($dateRange);

        $modifiedInput->push($requestCacheKey);

        return $modifiedInput;
    }

    /**
     * Get recent activity table for statistics as a collection of json ['rows' => json, 'columns' => json]
     *
     * @param Request $request
     * @param Pack $pack
     * @return Collection
     */
    private function initRecentActivityTable(Request $request, Pack $pack)
    {
        list($levelInput, $kuskiLike, $dateRange, $requestCacheKey) = $this->parseRequest($request)->values()->toArray();
        $level = Level::find($levelInput);
        $remember = config('cache.remember');

        if ($levelInput == 0) {
            $data = \Cache::remember("statistics_recent_activity{$pack->id}{$requestCacheKey}", $remember, function() use ($pack, $dateRange, $kuskiLike) {
                return $pack->recentActivity(self::ROW_LIMIT, $with = ['kuski'], $dateRange, $kuskiLike);
            });
        } else {
            $data = \Cache::remember("statistics_recent_activity{$pack->id}{$requestCacheKey}", $remember, function() use ($pack, $level, $dateRange, $kuskiLike) {
                return $level->recentActivity(self::ROW_LIMIT, $with = ['kuski'], $pack, $dateRange, $kuskiLike);
            });
        }

        return \Cache::remember("statistics_recent_activity_data{$pack->id}{$requestCacheKey}", $remember, function() use($levelInput, $data) {
            $data = $data->load('level')->map(function($time) use ($levelInput) {
                $data = [
                    'kuski' => view()->make('kuskis.partials.compactKuskiIdentifier', [
                        'kuski' => $time->kuski
                    ])->render(),
                    'old_time' => $time->old_time,
                    'time' => $time->time,
                    'activity' => view()->make('packs.partials.recentActivityTag', compact('time'))->render(),
                    'date' => view()->make('components.dateAgo', ['date' => $time->driven_at])->render(),
                ];

                if ($levelInput == 0) {
                    $data['level'] = $time->level->name;
                }

                return $data;
            });

            $columns = collect([
                [
                    'name' => 'kuski',
                    'title' => 'Kuski',
                ],
                [
                    'name' => 'activity',
                    'title' => 'Activity',
                    'breakpoints' => 'xs sm',
                ],
                [
                    'name' => 'old_time',
                    'title' => 'Old time',
                    'breakpoints' => 'xs',
                ],
                [
                    'name' => 'time',
                    'title' => 'New time',
                ],
            ]);

            if ($levelInput == 0) {
                $columns->push(collect([
                    'name' => 'level',
                    'title' => 'Level',
                ]));
            }

            $columns->push(collect([
                'name' => 'date',
                'title' => 'Date',
                'breakpoints' => 'xs sm',
            ]));

            return collect([
                'columns' => $columns->toJson(),
                'rows' => $data->values()->toJson(),
            ]);
        });
    }

    /**
     * Get finish counts table for statistics as a collection of json ['rows' => json, 'columns' => json]
     *
     * @param Request $request
     * @param Pack $pack
     * @return Collection
     */
    private function initFinishCountsTable(Request $request, Pack $pack)
    {
        list($levelInput, $kuskiLike, $dateRange, $requestCacheKey) = $this->parseRequest($request)->values()->toArray();
        $level = Level::find($levelInput);
        $remember = config('cache.remember');

        if ($levelInput == 0) {
            $data = \Cache::remember("statistics_finish_counts{$pack->id}{$requestCacheKey}", $remember, function() use ($pack, $dateRange, $kuskiLike) {
                $data = $pack->finishCounts(0, $with = ['kuski'], $dateRange, $kuskiLike);

                $data = $data->map(function($kuski) {
                    $data = [
                        'kuski' => view()->make('kuskis.partials.compactKuskiIdentifier', [
                            'kuskiName' => $kuski['kuski'],
                            'team' => $kuski['team'],
                            'nationality' => $kuski['nationality'],
                        ])->render(),
                        'finish_count' => $kuski['finish_count'],
                    ];

                    return $data;
                });

                return $data;
            });
        } else {
            $data = \Cache::remember("statistics_finish_counts{$pack->id}{$requestCacheKey}", $remember, function() use ($pack, $level, $dateRange, $kuskiLike) {
                $data = $level->finishCounts(self::ROW_LIMIT, $with = ['kuski'], $pack, $dateRange, $kuskiLike);

                $data = $data->map(function($time) use ($level) {
                    $data = [
                        'kuski' => view()->make('kuskis.partials.compactKuskiIdentifier', [
                            'kuski' => $time->kuski
                        ])->render(),
                        'finish_count' => $time->finish_count,
                    ];

                    return $data;
                });

                return $data;
            });
        }

        return \Cache::remember("statistics_finish_counts_data{$pack->id}{$requestCacheKey}", $remember, function() use ($data) {
            return collect([
                'columns' => collect([
                    [
                        'name' => 'kuski',
                        'title' => 'Kuski',
                    ],
                    [
                        'name' => 'finish_count',
                        'title' => 'Finish count',
                    ],
                ])->toJson(),
                'rows' => $data->values()->take(self::ROW_LIMIT)->toJson(),
            ]);
        });
    }

    /**
     * Get total times table for statistics as a collection of json ['rows' => json, 'columns' => json]
     *
     * @param Request $request
     * @param Pack $pack
     * @return Collection
     */
    private function initTotalTimesTable(Request $request, Pack $pack)
    {
        list($levelInput, $kuskiLike, $dateRange, $requestCacheKey) = $this->parseRequest($request)->values()->toArray();
        $remember = config('cache.remember');

        return \Cache::remember("statistics_total_times{$pack->id}{$requestCacheKey}", $remember, function() use ($pack, $dateRange, $kuskiLike) {
            if ($dateRange) {
                $data = $pack->getTotalTimes($kuskiId = 0, self::ROW_LIMIT, $dateRange, $kuskiLike)
                    ->map(function($kuski) {
                        return [
                            'kuski' => view()->make('kuskis.partials.compactKuskiIdentifier', [
                                'kuski' => $kuski
                            ])->render(),
                            'total_time' => mstostrtime($kuski->total_time),
                        ];
                    })
                    ->values();
            }

            else {
                $data = $pack->totalTimes()
                    ->with('kuski')
                    ->when($kuskiLike, function ($query) use ($kuskiLike) {
                        return $query->whereHas('kuski', function($query) use ($kuskiLike) {
                            $query->where('name', 'like', "%{$kuskiLike}%");
                        });
                    })
                    ->get()
                    ->sortBy('total_time')
                    ->take(self::ROW_LIMIT)
                    ->map(function($totalTime) {
                        return [
                            'kuski' => view()->make('kuskis.partials.compactKuskiIdentifier', [
                                'kuski' => $totalTime->kuski
                            ])->render(),
                            'total_time' => mstostrtime($totalTime->total_time),
                        ];
                    })
                    ->values();
            }

            return collect([
                'columns' => collect([
                    [
                        'name' => 'kuski',
                        'title' => 'Kuski',
                    ],
                    [
                        'name' => 'total_time',
                        'title' => 'Totaltime',
                    ],
                ])->toJson(),
                'rows' => $data->toJson(),
            ]);
        });
    }
}
