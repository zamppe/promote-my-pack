<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Level;
use App\Models\Pack;
use App\Models\Tag;
use App\Services\TimeSearchService;
use App\Services\TimeService;
use Response;

/**
 * Class PackLevelsController
 * @package App\Http\Controllers
 */
class PackLevelsController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param Pack $pack
     * @param $levelId
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Pack $pack, $levelId, Request $request)
    {
        $level = Level::find($levelId);
        $this->validate($request, [
            'after' => 'date_format:Y-m-d',
            'before' => 'date_format:Y-m-d'
        ]);
        $search = new TimeSearchService($request);
        $times = $search->search()->getPaginated();
        $nationalities = keyByValue($level->getInvolvedNationalities())->prepend('all', 'all');
        $teams = keyByValue($level->getInvolvedTeams())->prepend('all', 'all');
        $kuskiCount = $level->kuskis->count();
        $missingKuskiCount = $level->missingKuskis->count();
        $tags = $level->tags;
        $allTags = Tag::all();
        $input = collect($request->input());

        return view('packs.levels.show', compact(
            'pack',
            'level',
            'times',
            'kuskiCount',
            'missingKuskiCount',
            'nationalities',
            'teams',
            'tags',
            'allTags',
            'input'
        ));
    }

    /**
     * Show more or less rows about the pack
     *
     * @param Pack $pack
     * @param int $levelId
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function getExtraInfo(Pack $pack, $levelId, Request $request)
    {
        $level = Level::find($levelId);
        $n = $request->input('n', 3);
        if ($request->wantsJson()) {
            $response = [];
            $response['top_lists'] = view()->make('packs.partials.topLists', compact(
                'pack',
                'level',
                'n'
            ))->render();

            return Response::json($response);
        }
    }
}
