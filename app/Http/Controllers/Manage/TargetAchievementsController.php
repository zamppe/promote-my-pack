<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\TargetAchievement;
use App\Models\Pack;
use App\Http\Requests\TargetAchievementRequest;
use Illuminate\Http\Request;
use MediaUploader;

/**
 * Class TargetAchievementsController
 * @package App\Http\Controllers\Manage
 */
class TargetAchievementsController extends Controller
{
    /**
     * @param Pack $pack
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Pack $pack)
    {
        $levels = $pack->levels;

        return view('manage.packs.achievements.createTargetAchievement', compact('pack', 'levels'));
    }

    /**
     * @param Pack $pack
     * @param TargetAchievement $achievement
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Pack $pack, TargetAchievement $achievement)
    {
        $levels = $pack->levels;

        return view('manage.packs.achievements.editTargetAchievement', compact('pack', 'levels', 'achievement'));
    }

    /**
     * @param Pack $pack
     * @param TargetAchievementRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Pack $pack, TargetAchievementRequest $request)
    {
        $achievement = $pack->targetAchievements()->create($request->except('target_value', 'levels'));
        $achievement->target_value = $request->input('target_value');
        $achievement->levels = $request->input('levels');
        $achievement->save();

        if ($request->file('image')) {
            $media = MediaUploader::fromSource($request->file('image'))->upload();
            $achievement->attachMedia($media, 'achievement');
        }

        return redirect()->route('manage.packs.edit', compact('pack'));
    }

    /**
     * @param Pack $pack
     * @param TargetAchievement $achievement
     * @param TargetAchievementRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Pack $pack, TargetAchievement $achievement, TargetAchievementRequest $request)
    {
        $achievement->update($request->except('target_value', 'levels'));
        $achievement->target_value = $request->input('target_value');
        $achievement->levels = $request->input('levels');
        $achievement->save();

        if ($request->file('image')) {
            $media = $achievement->getMedia('achievement')->first();

            if ($media) {
                $media->delete();
            }

            $media = MediaUploader::fromSource($request->file('image'))->upload();
            $achievement->attachMedia($media, 'achievement');
        }

        return redirect()->route('manage.packs.targetAchievements.edit', compact('pack', 'achievement'));
    }

    /**
     * @param Pack $pack
     * @param TargetAchievement $achievement
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Pack $pack, TargetAchievement $achievement)
    {
        $user = user();

        if ($user && $user->isPackAdmin($pack)) {
            $achievement->achieves()->delete();
            $achievement->delete();
        }

        return redirect()->route('manage.packs.edit', compact('pack'));
    }
}
