<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Jobs\UpdateImprovementHistoryJob;
use App\Models\Kuski;
use App\Models\Level;
use App\Models\LocalKuskiBan;
use App\Models\Pack;
use App\Models\Time;
use App\Services\EolApiService;
use App\Services\KuskiService;
use App\Services\LevelService;
use App\Services\PackService;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class PackKuskisController
 * @package App\Http\Controllers\Manage
 */
class PackKuskisController extends Controller
{
    /**
     * @param Pack $pack
     * @param $kuskiId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function banKuski(Pack $pack, $kuskiId)
    {
        LocalKuskiBan::create([
            'pack_id' => $pack->id,
            'kuski_id' => $kuskiId,
        ]);

        $this->rebuildHistory($pack, $kuskiId);

        return redirect()->route('manage.packs.edit', $pack);
    }

    /**
     * @param Pack $pack
     * @param $kuskiId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unbanKuski(Pack $pack, $kuskiId)
    {
        $ban = LocalKuskiBan::where('pack_id', $pack->id)
            ->where('kuski_id', $kuskiId)
            ->first();

        if ($ban) {
            $ban->delete();
        }

        $this->rebuildHistory($pack, $kuskiId);

        return redirect()->route('manage.packs.edit', $pack);
    }

    private function rebuildHistory(Pack $pack, $kuskiId)
    {
        $kuski = Kuski::find($kuskiId);
        foreach($kuski->levelsInvolvedIn($pack) as $level) {
            LevelService::rebuildImprovementHistory($pack, $level);
        }

        PackService::rebuildTotalTimeHistory($pack);
        cacheRefresh($pack->id);
    }
}
