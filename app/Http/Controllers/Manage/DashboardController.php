<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\Pack;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Manage
 */
class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function Dashboard()
    {
        $user = user();
        if ($user->isAdmin()) {
            $packs = Pack::all();
        } else {
            $packs = $user->packs;
        }

        return view('manage.dashboard', compact('packs'));
    }
}
