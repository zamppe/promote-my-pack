<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\Level;
use App\Models\Tag;
use App\Services\LevelService;
use Illuminate\Http\Request;

/**
 * Class LevelController
 * @package App\Http\Controllers\Manage
 */
class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateTimes($id)
    {
        $level = Level::find($id);
        LevelService::updateTimes($level);

        return back();
    }

    /**
     * @param $levelId
     * @param $tagId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggleTag($levelId, $tagId)
    {
        $level = Level::find($levelId);
        $tag = Tag::find($tagId);
        if($level && $tag) {
            if ($level->tags()->pluck('id')->contains($tagId)) {
                $level->tags()->detach($tagId);
            } else {
                $level->tags()->attach($tagId);
            }
        }

        return back();
    }
}
