<?php

namespace App\Http\Controllers\Manage;

use App\Jobs\UpdateImprovementHistoryJob;
use App\Http\Controllers\Controller;
use App\Models\Level;
use App\Models\LocalTimeBan;
use App\Models\Pack;
use App\Models\Time;
use App\Services\EolApiService;
use App\Services\KuskiService;
use App\Services\LevelService;
use App\Services\PackService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * Class PackLevelsController
 * @package App\Http\Controllers\Manage
 */
class PackLevelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Pack $pack
     * @param $levelId
     * @return \Illuminate\Http\Response
     */
    public function show(Pack $pack, $levelId)
    {
        $level = Level::find($levelId);

        $times = $level->times()
                       ->with('kuski', 'localTimeBans')
                       ->paginate(100, ['times.*']);

        $kuskiCount = $level->kuskis->count();
        $missingKuskiCount = $level->missingKuskis->count();

        return view('manage.packs.levels.show', compact('pack', 'level', 'times', 'kuskiCount', 'missingKuskiCount'));
    }

    /**
     * @param Pack $pack
     * @param $levelId
     * @param $timeId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function banTime(Pack $pack, $levelId, $timeId)
    {
        $time = Time::where('times.id', $timeId)->first(['times.*']);
        LocalTimeBan::create([
            'pack_id' => $pack->id,
            'level_id' => $levelId,
            'time_id' => $timeId,
            'kuski_id' => $time->kuski_id
        ]);
        $level = Level::find($levelId);

        LevelService::rebuildImprovementHistory($pack, $level);
        PackService::rebuildTotalTimeHistory($pack);
        cacheRefresh($pack->id);

        return back();
        // return redirect()->route('manage.packs.levels.show', [$pack, $levelId]);
    }

    /**
     * @param Pack $pack
     * @param $levelId
     * @param $timeId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unbanTime(Pack $pack, $levelId, $timeId)
    {
        $ban = LocalTimeBan::where('pack_id', $pack->id)
            ->where('time_id', $timeId)
            ->first();
        $ban->delete();

        $level = Level::find($levelId);

        LevelService::rebuildImprovementHistory($pack, $level);
        PackService::rebuildTotalTimeHistory($pack);
        cacheRefresh($pack->id);

        return back();

        // return redirect()->route('manage.packs.levels.show', [$pack, $levelId]);
    }

    /**
     * @param Request $request
     * @param Pack $pack
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request, Pack $pack)
    {
        if($request->has('level_index')) {
            $this->validate($request, [
                'level_index' => 'required|numeric',
            ]);
            $levelIndex = $request->input('level_index');

            PackService::addLevelByEolId($pack, $levelIndex);
        }

        $pack = $pack->fresh(['targetAchievements', 'customAchievements']);

        if ($request->wantsJson()) {
            $response = [];
            $response['levels_table'] = $this->renderLevelsTable($pack);
            $response['bans_tab'] = $this->renderBansTab($pack);
            $response['achievements_tab'] = $this->renderAchievementsTab($pack);

            return \Response::json($response);
        }

        return redirect()->route('manage.packs.edit', [$pack]);
    }

    /**
     * @param Request $request
     * @param Pack $pack
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(Request $request, Pack $pack)
    {
        $packName = $request->input('pack_name');
        dispatch(new \App\Jobs\ImportPackJob($pack, $packName));

        return redirect()->route('manage.packs.edit', [$pack]);
    }

    /**
     * @param Pack $pack
     * @param $levelId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function detach(Pack $pack, $levelId, Request $request)
    {
        /* @todo remove level id from all achievements that have it */
        $level = Level::find($levelId);
        $pack->levels()->detach($level);

        if ($level->times->count()) {
            LevelService::rebuildImprovementHistory($pack, $level, $deleteOnly = true);
            PackService::rebuildTotalTimeHistory($pack);
            cacheRefresh($pack->id);
        }

        if ($request->wantsJson()) {
            $response = [];
            $response['levels_table'] = $this->renderLevelsTable($pack);
            $response['bans_tab'] = $this->renderBansTab($pack);
            $response['achievements_tab'] = $this->renderAchievementsTab($pack);

            return \Response::json($response);
        }

        return redirect()->route('manage.packs.edit', compact('pack'));
    }

    /**
     * Sort the levels in the pack in alphabetical ascending order
     *
     * @param Pack $pack
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sort(Pack $pack, Request $request)
    {
        $pack->sortAlphabetical($pack->levels(), 'filename', $asc = true);

        if ($request->wantsJson()) {
            $response = [];
            $response['levels_table'] = $this->renderLevelsTable($pack);

            return \Response::json($response);
        }

        return redirect()->route('manage.packs.edit', compact('pack'));
    }

    /**
     * Sort the levels based on sortable.js drag result
     *
     * @param Pack $pack
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sortDrag(Pack $pack, Request $request)
    {
        $pack->sortWithHintArray($pack->levels(), $request->input('array', []));

        if ($request->wantsJson()) {
            $response = [];

            return \Response::json($response);
        }

        return redirect()->route('manage.packs.edit', compact('pack'));
    }

    /**
     * @param Pack $pack
     * @param Request $request
     * @return mixed
     */
    public function search(Pack $pack, Request $request)
    {
        $keyword = $request->input('keyword', '');

        $levels = Level::where('name', 'like', "%{$keyword}%")
            ->orWhere('filename', 'like', "%{$keyword}%")
            ->limit(50)
            ->orderBy('filename')
            ->orderBy('name')
            ->get();

        if ($request->wantsJson()) {
            $response = view()->make('manage.packs.levels.partials.levelSearchTable', compact('levels', 'pack'))->render();

            return \Response::json($response);
        }
    }

    /**
     * @param Pack $pack
     * @return string
     */
    private function renderLevelsTable(Pack $pack)
    {
        $levels = $pack->levels()->orderBy('position')->get();

        return view()->make('manage.packs.levels.partials.levelTable', compact('pack', 'levels'))->render();
    }

    /**
     * @param Pack $pack
     * @return string
     */
    private function renderBansTab(Pack $pack)
    {
        $timeBans = $pack->localTimeBans()->with('kuski', 'time', 'level')->get();
        $kuskiBans = $pack->localKuskiBans()->with('kuski')->get();

        return view()->make('manage.packs.tabs.tabBans', compact('pack', 'timeBans', 'kuskiBans'))->render();
    }

    /**
     * @param Pack $pack
     * @return string
     */
    private function renderAchievementsTab(Pack $pack)
    {
        $targetAchievements = $pack->targetAchievements;
        $customAchievements = $pack->customAchievements;

        return view()->make('manage.packs.tabs.tabAchievements', compact('pack', 'targetAchievements', 'customAchievements'))->render();
    }
}
