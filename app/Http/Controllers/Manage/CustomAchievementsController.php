<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\CustomAchievement;
use App\Models\Kuski;
use App\Models\Pack;
use App\Http\Requests\CustomAchievementRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use MediaUploader;
use Response;

/**
 * Class CustomAchievementsController
 * @package App\Http\Controllers\Manage
 */
class CustomAchievementsController extends Controller
{
    /**
     * CustomAchievementsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('packContainsCustomAchievement')->except('create');
    }

    /**
     * @param Pack $pack
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Pack $pack)
    {
        $levels = $pack->levels;

        return view('manage.packs.achievements.createCustomAchievement', compact('pack', 'levels'));
    }

    /**
     * @param Pack $pack
     * @param CustomAchievement $achievement
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Pack $pack, CustomAchievement $achievement, Request $request)
    {
        if ($request->wantsJson()) {
            $data = [];
            $data['kuskis'] = $this->renderKuskiSection($pack, $achievement, $request);

            return Response::json($data);
        }

        $levels = $pack->levels;
        $kuskis = $this->getKuskis();
        $achieves = $achievement->achieves;
        $kuskis = $kuskis->reject(function($kuski) use($achieves) {
            return $achieves->pluck('kuski_id')->contains($kuski->id);
        });

        return view('manage.packs.achievements.editCustomAchievement', compact('pack', 'levels', 'achievement', 'kuskis', 'achieves'));
    }

    /**
     * @param Pack $pack
     * @param CustomAchievementRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Pack $pack, CustomAchievementRequest $request)
    {
        $achievementData = $request->all();
        $achievementData['pack_id'] = $pack->id;
        $achievement = CustomAchievement::create($achievementData);

        //todo validate image
        if ($request->file('image')) {
            $media = MediaUploader::fromSource($request->file('image'))->upload();
            $achievement->attachMedia($media, 'achievement');
        }

        return redirect()->route('manage.packs.edit', compact('pack'));
    }

    /**
     * @param Pack $pack
     * @param CustomAchievement $achievement
     * @param CustomAchievementRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Pack $pack, CustomAchievement $achievement, CustomAchievementRequest $request)
    {
        /* @todo check that ach belongs to pack */
        $achievementData = $request->all();
        $achievement->update($achievementData);

        //todo validate image
        if ($request->file('image')) {
            $media = $achievement->getMedia('achievement')->first();

            if ($media) {
                $media->delete();
            }

            $media = MediaUploader::fromSource($request->file('image'))->upload();
            $achievement->attachMedia($media, 'achievement');
        }

        return redirect()->route('manage.packs.customAchievements.edit', compact('pack', 'achievement'));
    }

    /**
     * @param Pack $pack
     * @param CustomAchievement $achievement
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Pack $pack, CustomAchievement $achievement)
    {
        $user = user();

        if ($user && $user->isPackAdmin($pack)) {
            $achievement->achieves()->delete();
            $achievement->delete();
        }

        return redirect()->route('manage.packs.edit', compact('pack'));
    }

    /**
     * @param Pack $pack
     * @param CustomAchievement $achievement
     * @param Request $request
     * @return mixed
     */
    public function grantToKuski(Pack $pack, CustomAchievement $achievement, Request $request)
    {
        if(is_null($achievement->achieves()->where('kuski_id', $request->k_id)->first())) {
            $achievement->achieves()->create([
                'kuski_id' => $request->k_id,
                'achieved_at' => Carbon::now(),
            ]);
        }

        if ($request->wantsJson()) {
            $data = [];
            $data['kuskis'] = $this->renderKuskiSection($pack, $achievement, $request);

            return Response::json($data);
        }
    }

    /**
     * @param Pack $pack
     * @param CustomAchievement $achievement
     * @param Request $request
     * @return mixed
     */
    public function removeFromKuski(Pack $pack, CustomAchievement $achievement, Request $request)
    {
        $achieve = $achievement->achieves()->where('id', $request->achieve_id);
        $achieve->delete();

        if ($request->wantsJson()) {
            $data = [];
            $data['kuskis'] = $this->renderKuskiSection($pack, $achievement, $request);

            return Response::json($data);
        }
    }

    /**
     * @param Pack $pack
     * @param CustomAchievement $achievement
     * @param Request $request
     * @return string
     */
    protected function renderKuskiSection(Pack $pack, CustomAchievement $achievement, Request $request)
    {
        $whereLike = $request->kuski;
        $kuskis = $this->getKuskis($whereLike);
        // $kuskis = $pack->getKuskis($request->kuski, 50)->get();
        $achieves = $achievement->achieves;
        $kuskis = $kuskis->reject(function($kuski) use($achieves) {
            return $achieves->pluck('kuski_id')->contains($kuski->id);
        });

        return view()->make('manage.packs.achievements.partials.customAchievementKuskis', compact('pack', 'achievement', 'kuskis', 'achieves'))->render();
    }

    /**
     * Get all kuskis filtered by a string
     *
     * @param string $whereLike
     * @return \Illuminate\Support\Collection
     */
    private function getKuskis($whereLike = '')
    {
        return Kuski::where('name', 'like', "%{$whereLike}%")
            ->orWhereNull('name')
            ->orderBy('name', 'asc')
            ->limit(50)
            ->get();
    }
}
