<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;
use App\Models\News;
use App\Models\Pack;

/**
 * Class PacknewsController
 * @package App\Http\Controllers\Manage
 */
class PacknewsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Pack $pack
     * @param NewsRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Pack $pack, NewsRequest $request)
    {
        if($user = user()) {
            $user->news()->create([
                'title' => $request->title,
                'content' => $request->content,
                'newsable_id' => $pack->id,
                'newsable_type' => Pack::class
            ]);
        }

        return redirect()->route('manage.packs.edit', [$pack]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Pack $pack
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Pack $pack, $id)
    {
        $newsItem = News::find($id);

        return view('manage.packs.news.edit', compact('pack', 'newsItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Pack $pack
     * @param  int $id
     * @param NewsRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Pack $pack, $id, NewsRequest $request)
    {
        $newsItem = News::find($id);
        $newsItem->update($request->all());

        return redirect()->route('manage.packs.edit', [$pack]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Pack $pack
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pack $pack, $id)
    {
        $newsItem = News::find($id);
        $newsItem->delete();

        return redirect()->route('manage.packs.edit', [$pack]);
    }
}
