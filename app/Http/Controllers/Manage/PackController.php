<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Http\Requests\PackRequest;
use App\Models\Level;
use App\Models\Pack;
use App\Models\Time;
use App\Services\AchievementService;
use App\Services\LevelService;
use App\Services\PackService;
use DOMDocument;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class PackController
 * @package App\Http\Controllers\Manage
 */
class PackController extends Controller
{
    /**
     * PackController constructor.
     */
    public function __construct()
    {
        $this->middleware('isPackAdmin')->only('destroy');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PackRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PackRequest $request)
    {
        $user = user();
        if ($user) {
            Pack::create([
               'name' => $request->input('name'),
               'user_id' => $user->id,
               'deadline' => \Carbon\Carbon::now(),
            ]);
        }

        return redirect()->route('manage.dashboard');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Pack $pack
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Pack $pack, Request $request)
    {
        $pack->load([
            'localTimeBans.kuski',
            'localTimeBans.time',
            'localTimeBans.level',
            'localKuskiBans.kuski',
            'targetAchievements.achieves'
        ]);

        error_reporting(E_ERROR);
        $EOLPacks = keyByValue(self::getEOLPacks());

        $kuskiSearchResults = $pack->getKuskis($request->input('kuski'), 50)->get();

        return view('manage.packs.edit', compact('pack', 'kuskiSearchResults', 'EOLPacks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PackRequest|Request $request
     * @param Pack $pack
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(PackRequest $request, Pack $pack)
    {
        $pack->update($request->all());

        return redirect()->route('manage.packs.edit', [$pack]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Pack $pack
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Pack $pack)
    {
        $pack->delete();

        return back();
    }

    /**
     * @param Pack $pack
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateTimes(Pack $pack)
    {
        foreach($pack->levels as $level) {
            LevelService::updateTimes($level);
        }

        $pack->deadline = \Carbon\Carbon::now();

        return redirect()->route('manage.packs.edit', [$pack]);
    }

    /**
     * @param Pack $pack
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rebuildHistory(Pack $pack)
    {
        PackService::rebuildImprovementHistory($pack);
        PackService::rebuildTotalTimeHistory($pack);
        cacheRefresh($pack->id);

        return redirect()->route('manage.packs.edit', [$pack]);
    }

    /**
     * @param Pack $pack
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteTimes(Pack $pack)
    {
        foreach($pack->levels as $level) {
            foreach($level->times()->get(['times.*']) as $time) {
                $time->delete();
            }
        }

        return redirect()->route('manage.packs.edit', [$pack]);
    }

    /**
     * @param Pack $pack
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processAchievements(Pack $pack)
    {
        AchievementService::processAchievementsForPack($pack);

        return redirect()->route('manage.packs.edit', [$pack]);
    }

    /**
     * Get a collection of strings of EOL level packs
     *
     * @return Collection
     */
    private static function getEOLPacks()
    {
        $packs = collect();
        $xml = new DOMDocument();
        $url = "http://elmaonline.net/statistics/levelpack";
        $xml->loadHTMLFile($url);

        if (! $xml) {
            \Log::error("Load packs: failed to loadHTMLFile {$url}");

            return $packs;
        }

        $table = $xml->getElementsByTagName('table')->item(0);
        $rows = $table->getElementsByTagName('tr');

        foreach($rows as $row) {
            $cells = $row->getElementsByTagName('td');
            if ($cells->length) {
                $packName = $cells->item(0)->getElementsByTagName('a')->item(0)->textContent;
                $packs->push($packName);
            }
        }

        return $packs;
    }
}
