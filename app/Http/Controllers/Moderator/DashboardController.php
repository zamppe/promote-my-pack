<?php

namespace App\Http\Controllers\Moderator;

use App\Http\Controllers\Controller;
use App\Models\GlobalTimeBan;
use App\Models\Tag;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Moderator
 */
class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function Dashboard()
    {
        $tags = Tag::all();
        $timeBans = GlobalTimeBan::with(['level', 'time', 'kuski'])->get();

        return view('moderator.dashboard', compact('timeBans', 'tags'));
    }
}
