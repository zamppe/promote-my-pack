<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\Models\News;
use App\Models\Pack;
use Illuminate\Http\Request;


/**
 * Class NewsController
 * @package App\Http\Controllers
 */
class NewsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param NewsRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsRequest $request)
    {
        if(user() && (user()->isModerator()) || user()->isAdmin()) {
            user()->news()->create([
                'title' => $request->title,
                'content' => $request->content,
                'newsable_type' => null,
                'newsable_id' => 0
            ]);
        }

        return redirect()->route('frontpage');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort_if(!(user() && (user()->isModerator() || user()->isAdmin())), 403, "no rights");
        $newsItem = News::find($id);

        if ($newsItem->newsable instanceof Pack) {
            return redirect()->route('manage.packs.news.edit', [$newsItem->newsable, $newsItem]);
        }

        return view('news.edit', compact('newsItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewsRequest|Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsRequest $request, $id)
    {
        abort_if(!(user() && (user()->isModerator() || user()->isAdmin())), 403, "no rights");
        $newsItem = News::find($id);
        $newsItem->update($request->all());

        return redirect()->route('frontpage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort_if(!(user() && (user()->isModerator() || user()->isAdmin())), 403, "no rights");
        $newsItem = News::find($id);
        $news->delete();

        return redirect()->route('frontpage');
    }
}
