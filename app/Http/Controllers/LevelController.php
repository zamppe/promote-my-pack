<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\UpdateImprovementHistoryJob;
use App\Models\GlobalTimeBan;
use App\Models\Level;
use App\Models\Time;

/**
 * Class LevelController
 * @package App\Http\Controllers
 */
class LevelController extends Controller
{

    /**
     * LevelController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:'.config('roles.roles.moderator'))->only('banTime', 'unbanTime');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $level = Level::find($id);
        $times = $level->times()->with('kuski')->orderBy('time', 'asc')->orderBy('id', 'asc')->paginate(1000);

        $kuskiCount = $level->kuskis->count();
        $missingKuskiCount = $level->missingKuskis->count();

        return view('levels.show', compact('level', 'times', 'kuskiCount', 'missingKuskiCount'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param $levelId
     * @param $timeId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function banTime($levelId, $timeId)
    {
        if(!(user() && (user()->isModerator()) || user()->isAdmin())) {
            return back();
        }

        $time = Time::find($timeId);

        GlobalTimeBan::create([
            'level_id' => $levelId,
            'time_id' => $timeId,
            'kuski_id' => $time->kuski->id
        ]);

        $level = Level::find($levelId);

        // when time is global banned, improvement history is updated for every pack the level is in
        foreach($level->packs as $pack) {
            dispatch(new UpdateImprovementHistoryJob($level, $pack));
        }

        return back();
    }

    /**
     * @param $levelId
     * @param $timeId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unbanTime($levelId, $timeId)
    {
        if(!(user() && (user()->isModerator()) || user()->isAdmin())) {
            return back();
        }

        $ban = GlobalTimeBan::where('time_id', $timeId)->first();
        $ban->delete();

        $level = Level::find($levelId);

        // when global time ban is removed, improvement history is updated for every pack the level is in
        foreach($level->packs as $pack) {
            dispatch(new UpdateImprovementHistoryJob($level, $pack));
        }

        return back();
    }
}
