<?php

namespace App\Http\Controllers;

use App\Statistics\StatisticsFormRenderer;
use App\Statistics\StatisticsCollection;
use Illuminate\Http\Request;

/**
 * Class PackStatisticsController
 * @package App\Http\Controllers
 */
class StatisticsController extends Controller
{
    /**
     * Show the statistics form
     *
     * @param Request $request
     * @return type
     */
    public function show(Request $request, StatisticsFormRenderer $renderer)
    {
        $form = $renderer->handle($request);

        if ($request->wantsJson()) {
            return response()->json(['view' => $form], 200);
        }

        if (! count($request->all())) {
            $tables = "";
        } else {
            $tables = (new StatisticsCollection($request))->getTablesJson();
        }

        return view('statistics.show', compact('form', 'tables'));
    }

    /**
     * Generate statistics based on request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {
        $request->validate([
            'before' => 'date_format:Y-m-d',
            'after' => 'date_format:Y-m-d',
        ]);

        $results = new StatisticsCollection($request);

        if ($request->wantsJson()) {
            return response()->json($results->getTablesCollection(), 200);
        }
    }
}
