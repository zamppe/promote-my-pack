<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Pack;
use Illuminate\Http\Request;

/**
 * Class FrontPageController
 * @package App\Http\Controllers
 */
class FrontPageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function frontPage()
    {
        $packs = Pack::all();

        return view('packs.index', compact('packs'));
    }
}
