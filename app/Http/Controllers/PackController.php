<?php

namespace App\Http\Controllers;

use App\Models\Achieve;
use App\Models\CustomAchievement;
use App\Models\Level;
use App\Models\Pack;
use App\Models\TargetAchievement;
use App\Models\Time;
use App\Statistics\StatisticsTypes;
use Illuminate\Http\Request;
use Cache;

/**
 * Class PackController
 * @package App\Http\Controllers
 */
class PackController extends Controller
{
    /**
     * Show landing page of pack
     *
     * @param Pack $pack
     * @return \Illuminate\Http\Response
     */
    public function landing(Pack $pack)
    {
        $remember = config('cache.remember');

        if (is_dev()) {
            $recentActivity = $pack->recentActivity(50);
        } else {
            $recentActivity = Cache::tags($pack->cache_tag)->remember('landing_recent_activity', $remember, function() use ($pack) {
                return $pack->recentActivity(50); 
            });
        }

        return view('packs.landing', compact(
            'pack',
            'recentActivity'
        ));
    }

    /**
     * Show achievements of pack
     *
     * @param Pack $pack
     * @return \Illuminate\Http\Response
     */
    public function achievements(Pack $pack)
    {
        $targetAchievements = $pack->targetAchievements()->with('media')->get();
        $customAchievements = $pack->customAchievements()->with('media')->get();

        $recentAchieves = $pack->achieves()->sortByDesc('created_at');

        return view('packs.achievements', compact(
            'pack',
            'targetAchievements',
            'customAchievements',
            'recentAchieves'
        ));
    }

    /**
     * Show records of pack
     *
     * @param Pack $pack
     * @return \Illuminate\Http\Response
     */
    public function records(Pack $pack)
    {
        $levels = $pack->levels()->orderBy('position')->paginate(100);
        $levels->load('media');

        return view('packs.records', compact(
            'pack',
            'levels'
        ));
    }

    /**
     * Download levels
     *
     * @param Pack $pack
     * @return Illuminate\Http\Response
     */
    public function download(Pack $pack)
    {
        return $pack->downloadLevels();
    }
}
