<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * Class UsersController
 * @package App\Http\Controllers\Admin
 */
class UsersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($userId)
    {
        $user = User::find($userId);
        $roles = collect(config('roles.roles'))->reduce(function ($carry, $item) {
            return $carry + [$item => $item];
        }, []);

        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * @param $userId
     * @param ProfileRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($userId, ProfileRequest $request)
    {
        $user = User::find($userId);

        $input = $request->all();
        if (isset($input['password'])) {
            $input['password'] = bcrypt($input['password']);
        }

        $user->update($input);

        return redirect()->route('admin.users.edit', compact('user'));
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($userId)
    {
        $user = User::find($userId);

        if ($user && ! $user->isCurrentUser()) {
            $user->delete();
        } else {
            return redirect()->route('admin.users.edit', compact('user'));
        }

        return redirect()->route('admin.users.index');
    }

    /**
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function grantRole($userId, Request $request)
    {
        $user = User::find($userId);
        $role = $request->input('role');

        if ($user && ! $user->isCurrentUser()) {
            $user->assignRole($role);
        }

        return redirect()->route('admin.users.edit', compact('user'));
    }

    /**
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeRole($userId, Request $request)
    {
        $user = User::find($userId);
        $role = $request->input('role');
        if ($user && ! $user->isCurrentUser()) {
            $user->removeRole($role);
        }

        return redirect()->route('admin.users.edit', compact('user'));
    }
}
