<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pack;
use Illuminate\Http\Request;

/**
 * Class PackController
 * @package App\Http\Controllers\Admin
 */
class PackController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param Pack $pack
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Pack $pack)
    {
        $pack->delete();

        return back();
    }
}
