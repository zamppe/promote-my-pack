<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\LevelService;
use Illuminate\Http\Request;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Admin
 */
class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function Dashboard()
    {
        return view('admin.dashboard');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getAllLevels()
    {
        LevelService::getAllLevels();

        return redirect()->route('admin.dashboard');
    }
}
