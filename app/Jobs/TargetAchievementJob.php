<?php

namespace App\Jobs;

use App\Models\TargetAchievement;
use App\Models\Level;
use App\Models\Time;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

/**
 * Class TargetAchievementPerKuskiJob
 * @package App\Jobs
 */
class TargetAchievementJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $achievement;

    /**
     * TargetAchievementPerKuskiJob constructor.
     *
     * @param TargetAchievement $achievement
     */
    public function __construct(TargetAchievement $achievement)
    {
        $this->achievement = $achievement;
        $this->onQueue('tertiary');
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        $achievement = $this->achievement;

        if ($achievement->isTotalTimeType()) {
            $levels = $achievement->pack->levels;
        } else {
            $levels = Level::whereIn('id', $achievement->levels)->get();
        }
        $times = Time::select(['times.*']);

        // Most of the time it's enough to process through only pr improvements
        // But sometimes it's required to go through all times, for example when
        // the achievement is about getting position 17 in lev x among all finishes.
        // So this is just small "optimization"
        if (!($achievement->isConsiderAll() && $achievement->isTimeType() && $achievement->isComparisonTypeExact())) {
            $times->leftJoin('pr_improvements', 'pr_improvements.time_id', '=', 'times.id')
                ->whereNotNull('pr_improvements.time_id');
        }

        if ($achievement->isTimeType() && $achievement->isTargetValueTypeTime()) {
            if ($achievement->target_value != 0) {
                if ($achievement->isComparisonTypeUnder()) {
                    $operator = '<';
                } else if ($achievement->isComparisonTypeExact()) {
                    $operator = '=';
                }
                $times->where('times.time', $operator, $achievement->target_value);
            }
        }

        $times->withoutGlobalScope('order')
            ->orderBy('times.id', 'asc')
            ->whereIn('level_id', $levels->pluck('id'))
            ->chunk(500, function($times) use($achievement) {
                dispatch(new TargetAchievementChunkJob($achievement, $times->pluck('id')));
            });
    }

}
