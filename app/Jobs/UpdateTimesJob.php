<?php

namespace App\Jobs;

use App\Models\Level;
use App\Services\EolApiService;
use App\Services\TimeUpdater;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class UpdateTimesJob
 * @package App\Jobs
 */
class UpdateTimesJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $level;

    /**
     * UpdateTimesJob constructor.
     * @param Level $level
     */
    public function __construct(Level $level)
    {
        $this->level = $level;
        $this->onQueue('primary');
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        $level = $this->level;

        if ($level == null) {
            return;
        }

        $timesData = EolApiService::getTimes($level->id, $level->getHighestTimeIndex());

        if ($timesData) {
            foreach($timesData as $timeData) {
                TimeUpdater::updateRowFromEolApi($timeData, $level);
            }
        }

        $level->times_updated_at = Carbon::now();
        $level->save();
    }
}
