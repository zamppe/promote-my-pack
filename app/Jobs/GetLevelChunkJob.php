<?php

namespace App\Jobs;

use App\Models\Level;
use App\Services\EolApiService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class GetLevelJob
 * @package App\Jobs
 */
class GetLevelChunkJob extends Job implements ShouldQueue
{
    use InteractsWithQueue;

    protected $startId;
    protected $endId;

    /**
     * GetLevelJob constructor.
     * @param $levelId
     */
    public function __construct($startId, $endId)
    {
        $this->startId = $startId;
        $this->endId = $endId;
        $this->onQueue('tertiary');
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        $levelsData = EolApiService::getLevels($this->startId);

        if (! count($levelsData)) {
            return;
        }

        foreach($levelsData as $levelData) {
            if (! Level::find($levelData->LevelIndex)) {
                EolApiService::saveLevelData($levelData);
            }
        }

        $lastId = array_last($levelsData)->LevelIndex;

        if ($lastId < $this->endId) {
            dispatch(new self($lastId++, $this->endId));
        }
    }
}
