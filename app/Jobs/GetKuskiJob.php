<?php

namespace App\Jobs;

use App\Models\Kuski;
use App\Services\EolApiService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class GetKuskiJob
 * @package App\Jobs
 */
class GetKuskiJob extends Job implements ShouldQueue
{
    use InteractsWithQueue;

    protected $kuskiIndex;

    /**
     * GetKuskiJob constructor.
     * @param $kuskiIndex
     */
    public function __construct($kuskiIndex)
    {
        $this->kuskiIndex = $kuskiIndex;
        $this->onQueue('tertiary');
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        $kuskiData = EolApiService::getKuski($this->kuskiIndex);

        $kuski = Kuski::find($this->kuskiIndex);

        $kuski->update([
            'name' => $kuskiData->Nick,
            'team' => $kuskiData->Team,
            'nationality' => $kuskiData->Nation,
        ]);
    }
}
