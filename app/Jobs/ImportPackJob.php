<?php

namespace App\Jobs;

use App\Models\Pack;
use App\Models\Level;
use App\Services\LevelService;
use App\Services\PackService;
use App\Services\EolApiService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use DOMDocument;
use Storage;
use ZipArchive;

/**
 * Class ImportPackJob
 *
 * Import level pack from elma online site.
 * Logic is following:
 *
 * Get level filenames from the pack's page download zip archive (there are no better indicators on pack page)
 * Get best time for each level and attach to level filename
 * Make a search with each level filename
 * Pick id of the level that has equal best time and push to collection of level ids (there is no 100% guaranteed way to pick the correct one)
 * For each level id get level from EOL api and attach it to destination pack
 *
 * @package App\Jobs
 */
class ImportPackJob extends Job implements ShouldQueue
{
    use InteractsWithQueue;

    protected $pack;
    protected $packName;

    /**
     * ImportPackJob constructor.
     * @param Pack $pack
     * @param $packName
     */
    public function __construct(Pack $pack, $packName)
    {
        $this->packName = $packName;
        $this->pack = $pack;
        $this->onQueue('primary');
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        error_reporting(E_ERROR);

        $levelsData = EolApiService::getPackLevels($this->packName);
        $addedAny = false;

        foreach($levelsData as $id => $levelData) {
            $level = Level::createFromEolApiRow($id, $levelData);

            $addSucceeded = $this->pack->addLevel($level);

            if ($addSucceeded) {
                if (! $addedAny) {
                    $addedAny = true;
                }

                LevelService::updateTimes($level);
                LevelService::rebuildImprovementHistory($this->pack, $level);
                if (! $level->hasLevelData()) {
                    LevelService::downloadAndStoreLevel($level);
                    LevelService::makeAndStoreImage($level);
                }
            }
        }

        if ($addedAny) {
            PackService::rebuildTotalTimeHistory($this->pack);
            cacheRefresh($this->pack->id);
        }
    }
}
