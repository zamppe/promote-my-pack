<?php

namespace App\Jobs;

use App\Models\Level;
use App\Models\Time;
use App\Models\Pack;
use App\Models\Pr;
use App\Models\PrImprovement;
use App\Models\Wr;
use App\Models\WrImprovement;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class UpdateImprovementHistoryJob
 * @package App\Jobs
 */
class UpdateImprovementHistoryJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $level;
    protected $pack;
    protected $deleteOnly;

    /**
     * UpdateImprovementHistoryJob constructor.
     * @param Level $level
     * @param Pack $pack
     * @param bool $deleteOnly
     */
    public function __construct(Level $level, Pack $pack, $deleteOnly = false)
    {
        $this->level = $level;
        $this->pack = $pack;
        $this->deleteOnly = $deleteOnly;
        $this->onQueue('secondary');
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        $this->deleteHistory();
        if (! $this->deleteOnly) {
            $this->buildHistory();
        }
    }

    /**
     * Delete the history for this level in this pack
     */
    private function deleteHistory()
    {
        WrImprovement::join('times as t', 't.id', '=', 'wr_improvements.time_id')
            ->where('wr_improvements.pack_id', $this->pack->id)
            ->where('t.level_id', $this->level->id)
            ->delete();

        PrImprovement::join('times as t', 't.id', '=', 'pr_improvements.time_id')
            ->where('pr_improvements.pack_id', $this->pack->id)
            ->where('t.level_id', $this->level->id)
            ->delete();

        Wr::join('times as t', 't.id', '=', 'wrs.time_id')
            ->where('wrs.pack_id', $this->pack->id)
            ->where('t.level_id', $this->level->id)
            ->delete();

        Pr::join('times as t', 't.id', '=', 'prs.time_id')
            ->where('prs.pack_id', $this->pack->id)
            ->where('t.level_id', $this->level->id)
            ->delete();
    }

    /**
     * Rebuild the history for this level in this pack
     */
    private function buildHistory()
    {
        $levelTimes = $this->level->times()
            ->filterByPack($this->pack)
            ->notGlobalBanned()
            ->orderBy('id', 'asc')
            ->get(['times.*']);

        $prs = collect();
        $wr = null;

        foreach($levelTimes as $time) {
            $pr = $prs->get($time->kuski_id);
            if (!$pr || $time->time < $pr->time) {
                $prs->put($time->kuski_id, $time);
                $this->addPrImprovement($time, $pr);
            }

            if (! $wr || $time->time < $wr->time) {
                $this->addWrImprovement($time, $wr);
                $wr = $time;
            }
        }

        foreach($prs->sortBy('id') as $pr) {
            $this->addPr($pr);
        }

        if ($wr) {
            $this->addWr($wr);
        }
    }

    /**
     * Add pr record
     *
     * @param Time $time
     */
    private function addPr(Time $time)
    {
        Pr::create([
            'time_id' => $time->id,
            'pack_id' => $this->pack->id,
        ]);
    }

    /**
     * Add wr record
     *
     * @param Time $time
     */
    private function addWr(Time $time)
    {
        Wr::create([
            'time_id' => $time->id,
            'pack_id' => $this->pack->id,
        ]);
    }

    /**
     * Add pr improvement record
     *
     * @param Time $time
     * @param Time|null $pr
     */
    private function addPrImprovement(Time $time, Time $pr = null)
    {
        PrImprovement::create([
            'time_id' => $time->id,
            'old_time_id' => $pr ? $pr->id : null,
            'pack_id' => $this->pack->id,
        ]);
    }

    /**
     * Add wr improvement record
     *
     * @param Time $time
     * @param Time|null $wr
     */
    private function addWrImprovement(Time $time, Time $wr = null)
    {
        WrImprovement::create([
            'time_id' => $time->id,
            'old_time_id' => $wr ? $wr->id : null,
            'pack_id' => $this->pack->id,
        ]);
    }
}
