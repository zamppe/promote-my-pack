<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Artisan;

/**
 * Class CacheClearJob
 * @package App\Jobs
 */
class CacheRefreshJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $packId;
    /**
     * CacheClearJob constructor.
     */
    public function __construct($packId)
    {        
        $this->onQueue('clear');
        $this->packId = $packId;
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        Artisan::call("cache:refresh", [$this->packId]);
    }
}
