<?php

namespace App\Jobs;

use App\Models\Pack;
use App\Models\Time;
use App\Models\TotalTime;
use App\Models\TotalTimeImprovement;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Class UpdateTotalTimeHistoryJob
 * @package App\Jobs
 */
class UpdateTotalTimeHistoryJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $pack;

    /**
     * UpdateTotalTimeHistoryJob constructor.
     * @param Pack $pack
     */
    public function __construct(Pack $pack)
    {
        $this->pack = $pack;
        $this->onQueue('tertiary');
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        $this->deleteHistory();
        $this->buildHistory();
    }

    /**
     * Delete the history for this level in this pack
     */
    private function deleteHistory()
    {
        TotalTimeImprovement::where('pack_id', $this->pack->id)
            ->delete();

        TotalTime::where('pack_id', $this->pack->id)
            ->delete();
    }

    /**
     * Rebuild the history for this level in this pack
     */
    private function buildHistory()
    {
        $unfinishedTimePenalty = config('promo.unfinished_time_penalty');

        // Get all pr improvements for the pack in ascending chronological order
        $packPrImprovements = Time::leftJoin('pr_improvements', 'pr_improvements.time_id', '=', 'times.id')
            ->whereNotNull('pr_improvements.time_id')
            ->where('pr_improvements.pack_id', $this->pack->id)
            ->orderBy('times.id', 'asc')
            ->get(['times.*']);

        $kuskiPrsPerLevel = $this->kuskiPrsPerLevel();

        foreach($packPrImprovements as $prImprovement) {
            $levelId = $prImprovement->level_id;
            $kuskiId = $prImprovement->kuski_id;

            $kuskiPrs = $kuskiPrsPerLevel->get($kuskiId);
            $oldPr = $kuskiPrs->get($levelId);
            $kuskiPrs->put($levelId, $prImprovement);

            $kuskiTotalTime = $kuskiPrs->sum(function($prImprovement) {
                return $prImprovement ? $prImprovement->time_ms : null;
            });

            $finishedLevels = $kuskiPrs->reject(function($finish) {
                return $finish === null;
            })->count();

            $kuskiTotalTime += ($this->pack->levels->count() - $finishedLevels) * $unfinishedTimePenalty;

            TotalTimeImprovement::create([
                'time_id' => $prImprovement->id,
                'old_time_id' => ($oldPr instanceof Time)? $oldPr->id : null,
                'pack_id' => $this->pack->id,
                'total_time' => $kuskiTotalTime
            ]);

            $kuskiPrsPerLevel->put($kuskiId, $kuskiPrs);
        }

        foreach($kuskiPrsPerLevel as $kuskiId => $kuskiPrs) {
            $finishesOnly = $kuskiPrs->reject(function($pr) {
                return $pr === null;
            });

            if ($finishesOnly->count()) {
                $kuskiTotalTime = $kuskiPrs->sum(function($time) use($unfinishedTimePenalty) {
                    return $time ? $time->time_ms : $unfinishedTimePenalty;
                });

                TotalTime::create([
                    'kuski_id' => $kuskiId,
                    'pack_id' => $this->pack->id,
                    'total_time' => $kuskiTotalTime
                ]);
            }
        }
    }

    /**
     * Initialize a collection of unfinished times per level for each kuski
     *
     * @return Collection
     */
    private function kuskiPrsPerLevel()
    {
        $totalTimes = $this->pack->getKuskis()->get()->keyBy('id');

        return $totalTimes->map(function($time) {
            return $this->pack->levels->keyBy('id')->map(function($level) {
                return null;
            });
        });
    }
}
