<?php

namespace App\Jobs;

use App\Models\Level;
use App\Services\EolApiService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class GetLevelJob
 * @package App\Jobs
 */
class GetLevelJob extends Job implements ShouldQueue
{
    use InteractsWithQueue;

    protected $levelId;

    protected $getNext;

    /**
     * GetLevelJob constructor.
     * @param $levelId
     */
    public function __construct($levelId)
    {
        $this->levelId = $levelId;
        $this->onQueue('tertiary');
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        EolApiService::getAndSaveLevel($this->levelId);
    }
}
