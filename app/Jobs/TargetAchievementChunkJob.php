<?php

namespace App\Jobs;

use App\Models\TargetAchievement;
use App\Models\Kuski;
use App\Models\Level;
use App\Models\Time;
use App\Utils\DateRange;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

/**
 * Class TargetAchievementPerKuskiJob
 * @package App\Jobs
 */
class TargetAchievementChunkJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $achievement;
    protected $timeIds;
    protected $timeId;
    protected $kuski;
    protected $deadline;

    /**
     * TargetAchievementPerKuskiJob constructor.
     *
     * @param TargetAchievement $achievement
     * @param Level $level
     * @param Collection $times
     */
    public function __construct(TargetAchievement $achievement, Collection $timeIds)
    {
        $this->achievement = $achievement;
        $this->timeIds = $timeIds;
        $this->onQueue('tertiary');
    }

    /**
     * Handle the job
     */
    public function handle()
    {
        $times = Time::whereIn('times.id', $this->timeIds)
            ->select(['times.id', 'times.kuski_id', 'times.driven_at'])
            ->withoutGlobalScope('order')
            ->orderBy('times.id', 'asc')
            ->with('kuski')
            ->get();

        foreach($times as $time) {
            $this->kuski = $time->kuski;
            $this->deadline = $time->driven_at;
            $this->timeId = $time->id;
            $this->handleAchievementForKuski();
        }
    }

    /**
     * Handle the job per kuski
     */
    private function handleAchievementForKuski()
    {
        $kuskiAlreadyAchievedThis = $this->kuski->achieves()
                ->where('achievable_type', TargetAchievement::class)
                ->where('achievable_id', $this->achievement->id)
                ->get()->first() != null;

        if($kuskiAlreadyAchievedThis) {
            return;
        }

        if ($this->achievement->isTotalTimeType()) {
            $this->handleTotalTime();

        } else if ($this->achievement->isTimeType()) {
            $this->handleTime();
        }
    }

    /**
     * Find the position in total times for this kuski
     *
     * @param Collection $totalTimes
     * @return int
     */
    private function getTotalTimePosition(Collection $totalTimes)
    {
        $i = 1;
        foreach($totalTimes as $totalTime) {
            if ($totalTime->kuski_id == $this->kuski->id) {
                return $i;
            }

            $i++;
        }

        return 0;
    }

    /**
     * Handle processing of total time type targetAchievement
     */
    private function handleTotalTime()
    {
        if ($this->achievement->isTargetValueTypeTime()) {
            $kuski = $this->achievement->pack->getTotalTimes($this->kuski->id, $limit = 0, new DateRange(null, $this->deadline))->first();
            $totalTimeResult = $kuski ? $kuski->total_time : 0;
        } else if ($this->achievement->isTargetValueTypePosition()) {
            $totalTimes = $this->achievement->pack->getTotalTimes($kuskiId = 0, $limit = 0, new DateRange(null, $this->deadline));
            $totalTimeResult = $this->getTotalTimePosition($totalTimes);
        }

        if ($this->achievement->isComparisonTypeUnder()) {
            if ($totalTimeResult && $totalTimeResult < $this->achievement->target_value) {
                $this->grant();
            }

        } else if ($this->achievement->isComparisonTypeExact()) {
            if ($totalTimeResult && $totalTimeResult == $this->achievement->target_value) {
                $this->grant();
            }
        }
    }

    /**
     * Handle processing of time type targetAchievement
     */
    private function handleTime()
    {
        $pack = $this->achievement->pack;
        $levels = Level::whereIn('id', $this->achievement->levels)->get();
        $applyOn = $this->achievement->apply_on == 0? $levels->count() : $this->achievement->apply_on;
        $applies = 0;

        foreach($levels as $level) {
            if ($this->achievement->isTargetValueTypeTime()) {
                if ($this->achievement->isComparisonTypeExact()) {
                    if ($this->kuski->hasTimeToPackLevel($pack, $level, $this->achievement->target_value, $this->deadline)) {
                        $applies++;
                    }
                } else {
                    $bestResult = $this->kuski->bestTimeToPackLevel($pack, $level, $this->deadline);

                    if ($this->achievement->target_value == 0 && $bestResult != null) {
                        $applies++;
                    } else {
                        if ($bestResult != null && $bestResult->getExtraData('best_result') < $this->achievement->target_value) {
                            $applies++;
                        }
                    }
                }

            } else if ($this->achievement->isTargetValueTypePosition()) {
                if ($this->achievement->isComparisonTypeExact()) {
                    if ($this->kuski->hasPositionToPackLevel($pack, $level, $this->achievement->target_value, $this->deadline)) {
                        $applies++;
                    }
                } else {
                    $bestOnly = ($this->achievement->isConsiderBest() && $this->achievement->isComparisonTypeExact()) ? true : false;
                    $bestResult = $this->kuski->bestPositionToPackLevel($pack, $level, $bestOnly, $this->deadline, $this->achievement->target_value);
                    if ($this->achievement->target_value == 0 && $bestResult != null) {
                        $applies++;
                    } else {
                        if ($bestResult != null && $bestResult->getExtraData('best_result') < $this->achievement->target_value) {
                            $applies++;
                        }
                    }
                }
            }
        }

        if ($applies >= $applyOn) {
            $this->grant();
        }
    }

    /**
     * Grant the achievement to this kuski
     */
    private function grant()
    {
        $this->achievement->achieves()->create([
            'kuski_id' => $this->kuski->id,
            'time_id' => $this->timeId,
            'achieved_at' => $this->deadline,
        ]);
    }
}
