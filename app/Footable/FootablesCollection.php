<?php

namespace App\Footable;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

/**
 * Class FootablesCollection
 * @package App\Http\Controllers
 */
abstract class FootablesCollection
{
    protected $tables;
    protected $request;
    protected $required;

    public function __construct(Request $request, array $required)
    {
        $this->request = $request;
        $this->required = $required;
        $this->tables = collect();
    }

    public function getTables()
    {
        return $this->tables;
    }

    public function getTablesCollection()
    {
        return $this->tables->map(function($table) {
            return $table->getCollection();
        });
    }

    public function getTablesJson()
    {
        return $this->getTablesCollection()->toJson();
    }

    public function pushTable(FooTable $table)
    {
        $this->tables->push($table);
    }

    protected function hasRequired()
    {
        if (! $this->request->has('type')) {
            return false;
        }

        if (! array_key_exists($this->request->input('type'), $this->required)) {
            return false;
        }

        return $this->request->has($this->required[$this->request->input('type')]);
    }

    public static function generateDefaultTableCollection(Request $request)
    {
        $collection = new DefaultCollection($request, []);
        $table = new DefaultTable();
        $table->hideHeader();
        $table->disableFiltering();
        $table->setCols([['name' => 'placeholder']]);
        $collection->pushTable($table);

        return $collection;
    }
}
