<?php

namespace App\Footable;

use Illuminate\Support\Collection;

/**
 * Class FooTable
 * @package App\Http\Controllers
 */
abstract class Footable
{
    protected $keyvals;
    const JSON_KEY = 'statistics_json';
    const ROWS_KEY = 'rows';
    const COLS_KEY = 'columns';
    const FILTERING_KEY = 'filtering';
    const PAGING_KEY = 'paging';
    const SORTING_KEY = 'sorting';
    const TITLE_KEY = 'title';
    const HEADER_KEY = 'showHeader';

    public function __construct()
    {
        $this->keyvals = collect();
        $this->title = '';
        $this->showHeader();

        /* Default footable options */
        $this->setFiltering([
            'enabled' => true,
            'delay' => 200,
        ]);

        $this->setSorting([
            'enabled' => true,
        ]);

        $this->setPaging([
            'enabled' => true,
            'size' => 50,
        ]);
    }

    public function setKeyVal($key, $val)
    {
        if (is_array($val)) {
            $val = collect($val);
        }

        $this->keyvals->put($key, $val);
    }

    public function getKeyVal($key)
    {
        return $this->keyvals->get($key);
    }

    public function pushVal($key, $val)
    {
        if (! $this->keyvals->has($key)) {
            $this->keyvals->put($key, collect());
        }

        $this->keyvals->get($key)->push($val);
    }

    public function getJson()
    {
        return $this->keyvals->toJson();
    }

    public function getCollection()
    {
        return $this->keyvals;
    }

    public function setRows($val)
    {
        $this->setKeyVal(self::ROWS_KEY, $val);

        return $this;
    }

    public function pushRow($val)
    {
        $this->pushVal(self::ROWS_KEY, $val);

        return $this;
    }

    public function setCols($val)
    {
        $this->setKeyVal(self::COLS_KEY, $val);

        return $this;
    }

    public function setFiltering($val)
    {
        $this->setKeyVal(self::FILTERING_KEY, $val);

        return $this;
    }

    public function setPaging($val)
    {
        $this->setKeyVal(self::PAGING_KEY, $val);

        return $this;
    }

    public function setSorting($val)
    {
        $this->setKeyVal(self::SORTING_KEY, $val);

        return $this;
    }

    public function setTitle($title)
    {
        $this->setKeyVal(self::TITLE_KEY, $title);

        return $this;
    }

    public function hideHeader()
    {
        $this->setKeyVal(self::HEADER_KEY, false);

        return $this;
    }

    public function showHeader()
    {
        $this->setKeyVal(self::HEADER_KEY, true);

        return $this;
    }

    public function disableFiltering()
    {
        $filtering = $this->getKeyVal(self::FILTERING_KEY);
        $filtering->put('enabled', false);

        $this->setFiltering($filtering);

        return $this;
    }

    public function perPage($n)
    {
        $filtering = $this->getKeyVal(self::PAGING_KEY);
        $filtering->put('size', $n);

        $this->setPaging($filtering);

        return $this;
    }
}

