<?php

namespace App\Footable;

use Illuminate\Http\Request;

/**
 * Class DefaultCollection
 * @package App\Statistics
 */
class DefaultCollection extends FootablesCollection
{
    /**
     * Constructor
     *
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct($request, []);
    }
}
