<?php

namespace App\Providers;

use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Validator::extend('elma_time', function($attribute, $value, $parameters)
        {
            return preg_match('#^[0-5][0-9](:[0-5][0-9])(,[0-9][0-9])?$#', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Collection::macro('keyByValue', function() {
            return $this->reduce(function($carry, $item) {
                $carry->put($item, $item);

                return $carry;
            }, collect());
        });

        Collection::macro('hardIncrement', function($key, $amount) {
            return $this->put($key, $this->get($key) + $amount);
        });

        Collection::macro('cumulativeCount', function($resultKey, $countable) {
            $cumulativeCounts = collect();

            foreach($this->items as $outerItem) {
                foreach($outerItem as $innerItem) {
                    $key = $innerItem->get($resultKey);
                    $innerCount = $innerItem->get($countable);

                    if ($cumulativeCounts->has($key)) {
                        $innerCollection = $cumulativeCounts->get($key);
                        $innerCollection->hardIncrement($countable, $innerCount);
                        $cumulativeCounts->put($key, $innerCollection);
                    } else {
                        $cumulativeCounts->put($key, $innerItem);
                    }
                }
            }

            return $cumulativeCounts;
        });

        /* https://gist.github.com/brunogaspar/154fb2f99a7f83003ef35fd4b5655935 */
        /* Convert an array of arrays to a collection of collections */
        Collection::macro('recursive', function () {
            return $this->map(function ($value) {
                if (is_array($value) || is_object($value)) {
                    return collect($value)->recursive();
                }

                return $value;
            });
        });

        /* Return only the items before first found key or value */
        Collection::macro('before', function($keyOrVal) {
            $items = collect();

            foreach ($this->items as $key => $item) {
                if ($item == $keyOrVal || $key == $keyOrVal) {
                    return $items;
                }

                $items->put($key, $item);
            }

            return $items;
        });
    }
}
