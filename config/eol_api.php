<?php

return [
    'levels' => 'http://elmaonline.net/API/levels/',
    'level' => 'http://elmaonline.net/API/level/',
    'kuski' => 'http://elmaonline.net/API/player/',
    'times' => 'http://elmaonline.net/API/times/',
    'packs' => [
        'by_name' => 'http://elmaonline.net/API/levelpacklevels/byName/',
        'by_id' => 'http://elmaonline.net/API/levelpacklevels/byId/',
    ],
    'level_index_guess' => 400000,
];
