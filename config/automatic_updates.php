<?php

return [
    'deadline_update_at' => '3:55',
    'mass_update_at' => '4:00',
    'data_fix_at' => '5:50',
    'kuski_team_update_at' => '6:00',
    'kuski_null_fix_at' => '6:10',
    'cache_refresh_at' => '6:30',
    'missing_times_fix' => [
        'some_time_ago' => 3, // months
    ],
];
