<?php
    return [
        'roles' => [
            'default' => 'user',
            'moderator' => 'moderator',
            'admin' => 'admin',
        ],
        'models' => [
            'role' => App\Models\Role::class,
        ],
        'table_names' => [
            'users' => 'users',
            'roles' => 'roles',
            'user_has_roles' => 'role_user',
        ],

    ];
