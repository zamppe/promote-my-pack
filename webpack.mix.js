let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
.postCss('resources/assets/postcss/app.css', 'public/css', [
  require('postcss-import')(),
  require('postcss-cssnext')()
])
.copy('node_modules/flag-icon-css/flags', 'public/flags')
.copy('node_modules/font-awesome/fonts', 'public/fonts')
.copy('node_modules/lightgallery/dist/fonts', 'public/fonts')
.copy('node_modules/lightgallery/dist/img/loading.gif', 'public/img')
.copy('node_modules/lightgallery/dist/css/lightgallery.min.css', 'public/css')
.copy('resources/assets/js/vendor/ion-tabs/preloader-flat.gif', 'public/img/')
.copy('node_modules/tinymce/', 'public/js/tinymce')
.combine([
	/* npm vendor packages */
	'node_modules/jquery/dist/jquery.min.js',
	'node_modules/sortablejs/Sortable.min.js',
	'node_modules/lightgallery/dist/js/lightgallery.min.js',
	'node_modules/lg-fullscreen/dist/lg-fullscreen.min.js',
  'node_modules/lg-zoom/dist/lg-zoom.min.js',
  'node_modules/flatpickr/dist/flatpickr.min.js',
	'node_modules/select2/dist/js/select2.full.min.js',
	/* Manually installed packages included in repository (require manual updating) */
	'resources/assets/js/vendor/ion-tabs/ion.tabs.js',
	'resources/assets/js/vendor/footable-standalone.latest/footable.min.js',
	/* App scripts */
	'resources/assets/js/app.js'
], 'public/js/app.js')
.options({
	processCssUrls: false
});

if (mix.inProduction()) {
    mix.version();
}
